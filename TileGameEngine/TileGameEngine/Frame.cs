﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using BlueRavenUtility;

using Newtonsoft.Json;

namespace TileGameEngine
{
    public struct Frame
    {
        public int timeMax;
        public readonly int animationIndex;

        public readonly int frameIndex;

        public Rectangle sourceRect;

        [JsonIgnore]
        public readonly bool usable;

        public Frame(int animationIndex, int index, Rectangle sourceRect, int timeMax)
        {
            this.animationIndex = animationIndex;
            this.frameIndex = index;

            this.sourceRect = sourceRect;

            this.timeMax = timeMax;

            usable = true;
        }

        public int GetByteSize()
        {
            return 4 + 4 + 4 +  //timemax, animationindex, frameindex
                4 + 4 + 4 + 4;  //sourcerect xywh
        }

        #region static
        public static Frame[] CreateFrames(int animIndex, int startFrameIndex, int startX, int constY, int buffer, int frameWidth, int constHeight, int num, int time)
        {
            List<Frame> frames = new List<Frame>();
            for (int i = 0; i < num; i++)
            {
                frames.Add(new Frame(animIndex, startFrameIndex, new Rectangle(startX + (frameWidth * i) + (buffer * i), constY, frameWidth, constHeight), time));
            }

            return frames.ToArray();
        }

        public static Frame[] CreateFrames(int animIndex, int startFrameIndex, int startX, int constY, int buffer, int[] frameWidths, int constHeight, int num, int time)
        {
            List<Frame> frames = new List<Frame>();
            for (int i = 0; i < num; i++)
            {
                frames.Add(new Frame(animIndex, startFrameIndex, new Rectangle(startX + (SumPrevWidths(frameWidths, i)) + (buffer * i), constY, frameWidths[i], constHeight), time));
            }

            return frames.ToArray();
        }

        private static int SumPrevWidths(int[] frameWidths, int index)
        {
            int sum = 0;
            for (int i = 0; i < index; i++ )
            {
                sum += frameWidths[i];
            }
            return sum;
        }
        #endregion
    }
}
