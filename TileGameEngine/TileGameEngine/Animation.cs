﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace TileGameEngine
{
    public struct Animation
    {
        public int index { get; private set; }

        public AnimationType animationType { get; private set; }
        public int returnIndex { get; private set; }

        public List<Frame> frames { get; private set; }

        [JsonIgnore]
        public readonly bool usable;

        public Animation(int index, AnimationType animationType, int returnIndex = 0, params Frame[] frames)
        {
            this.index = index;
            this.animationType = animationType;
            this.frames = frames.ToList();
            this.returnIndex = returnIndex;

            usable = true;
        }

        public int GetByteSize()
        {
            int framesSize = 0;
            frames.ForEach(x => framesSize += x.GetByteSize());
            int frameCount = frames.Count * 4;
            return 4 + 4 + 4 +  //index, animationType, returnIndex
                framesSize + frameCount;
        }
    }
}
