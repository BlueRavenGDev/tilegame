﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.IO;

using TileGameEngine;

using BlueRavenUtility;

using QuakeConsole;

using Myra;
using Myra.Graphics2D.UI;
using System.Collections.Generic;
using System;
using TileGameEngine.Components.Renderers;
using TileGameEngine.Components.Colliders;
using TileGameEngine.Assets;
using TileGameEngine.Components;

using TileGameEngine.Editor;
using TileGameEngine.Components.Scripts;
using TileGameEngine.Editor.UI;

namespace TileGameEngine
{
    /// <summary>
    /// Handle the main state of the game and related global things
    /// </summary>
    public abstract class GameMain : Game
    {
        public static int WIDTH, HEIGHT;

        GraphicsDeviceManager graphics;
        SpriteBatch batch;

        public GameWorld world;

        public static GlobalAssets assets;

        public static GameKeyboard keyboard;
        public static GameMouse mouse;

        public static ConsoleComponent console;

        //private Desktop _host;

        public static bool pause;

        public static Dictionary<string, Preset> presets;
        public static Dictionary<string, Func<GameObject>> entityPresets;
        public static Dictionary<string, Func<GameObject>> particlePresets;
        private bool presetsRegistered;

        public static Random rand;

        public static Func<string, object> createComponent;

        public static EditPanelWorld editPanel;
        public static TextureSelector textureSelector;

        private static bool _editing;
        public static bool editing { get { return _editing; } set { editingChanged?.Invoke(value); _editing = value; } }

        public static Action<bool> editingChanged;

        private string _errorTextureKey;
        protected string errorTextureKey
        {
            get
            {
                if (_errorTextureKey == default(string))
                    throw new Exception("Default error texture not set!");
                return _errorTextureKey;
            }
            set { _errorTextureKey = value; }
        }

        public GameMain()
        {
            base.IsMouseVisible = true;
            rand = new Random();

            pause = false;

            presets = new Dictionary<string, Preset>();
            entityPresets = new Dictionary<string, Func<GameObject>>();
            particlePresets = new Dictionary<string, Func<GameObject>>();

            console = new ConsoleComponent(this);

            var interpreter = new ManualInterpreter();
            console.Interpreter = interpreter;

            RegisterConsoleCommands(interpreter);

            Components.Add(console);

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            createComponent = CreateComponent;

            WIDTH = graphics.PreferredBackBufferWidth;
            HEIGHT = graphics.PreferredBackBufferHeight;
            CreateWorld();

            editPanel = new EditPanelWorld(this);
            textureSelector = new TextureSelector(this);
        }

        protected override void Initialize()
        {
            keyboard = new GameKeyboard();
            mouse = new GameMouse();

            Texture2D whitePixel = new Texture2D(GraphicsDevice, 1, 1);
            whitePixel.SetData(new Color[] { Color.White });
            Utility.Setup(new Camera(GraphicsDevice.Viewport), whitePixel);

            assets = new GlobalAssets(errorTextureKey, "monro12");
            base.Initialize();

            world.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            batch = new SpriteBatch(GraphicsDevice);

            assets.Load(Content, GraphicsDevice);

            world.LoadContent(Content, Directory.GetCurrentDirectory(), GraphicsDevice);
        }

        /// <summary>
        /// Initialize the game's initial world object.
        /// </summary>
        protected abstract void CreateWorld();

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            mouse.hasFocus = IsActive;
            if (!presetsRegistered)
            {
                RegisterPresets();
                presetsRegistered = true;
            }

            keyboard.PreUpdate();
            mouse.PreUpdate();

            keyboard.Update();
            Camera.camera.Update();

            if (keyboard.KeyPressed(Keys.P))
                pause = !pause;

            if (keyboard.KeyPressed(Keys.OemTilde))
                console.ToggleOpenClose();

            if (keyboard.KeyPressed(Keys.I))
            {
                editing = !editing;
                textureSelector.Visible = !textureSelector.Visible;
                editPanel.Visible = !editPanel.Visible;

                world.OnEditingModeChanged(editing);
            }

            if (!pause || keyboard.KeyPressed(Keys.O))
            {
                mouse.Update();
                world.Update();

                mouse.PostUpdate();
            }
            keyboard.PostUpdate();
            Camera.camera.PostUpdate(rand);

            base.Update(gameTime);
        }

        public virtual void RegisterConsoleCommands(ManualInterpreter interpreter)
        {
            interpreter.RegisterCommand("timescale", new Action<string[]>(p =>
            {
                try
                {
                    float timescale = float.Parse(p[0]);
                    GameWorld.timescale = timescale;
                }
                catch
                {
                    console.Output.Append("Incorrect input format.");
                }
            }));

            interpreter.RegisterCommand("debug_drawgrid", strings =>
            {
                try
                {
                    GameWorld.DEBUGgrid = bool.Parse(strings[0]);
                }
                catch
                {
                    console.Output.Append("Incorrect input format. parameter 1 requires a boolean value - 'true' or 'false'");
                }
            });

            interpreter.RegisterCommand("debug_drawPCT", strings =>     //pct = potential collidable tiles
            {
                try
                {
                    RendererPanel.DEBUGdrawCollidableTiles = bool.Parse(strings[0]);
                }
                catch
                {
                    console.Output.Append("Incorrect input format. parameter 1 requires a boolean value - 'true' or 'false'");
                }
            });

            interpreter.RegisterCommand("debug_drawCollision", strings =>
            {   //debug_drawCollision true
                try
                {
                    ColliderTile.DEBUGdrawCollision = bool.Parse(strings[0]);
                    ColliderTileSlope.DEBUGdrawSlopeCollision = bool.Parse(strings[0]);
                }
                catch
                {
                    console.Output.Append("Incorrect input format. parameter 1 requires a boolean value - 'true' or 'false'");
                }
            });
        }

        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.CornflowerBlue);

            batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointWrap, DepthStencilState.Default, RasterizerState.CullNone, null, Camera.camera.GetViewMatrix());
            world.Draw(batch);
            batch.End();

            batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointWrap, DepthStencilState.Default, RasterizerState.CullNone);
            Camera.camera.Draw(batch);
            batch.End();

            base.Draw(gameTime);
        }

        public static GameObject CloneFromPreset(string name, Vector2 position, bool useTexSelector, bool posIsTileSpace = false)
        {
            try
            {
                Preset p = presets[name.ToLower()];
                GameObject f = p.value?.Invoke();

                Vector2 fpos = position;

                if (posIsTileSpace && f.tile)   //if the given position is in tile space already, we need to turn it into world space as SetPositionFromWorldSpace
                    fpos *= GameWorld.TileSize; //Would try to convert a vlue already in tile space into tile space.

                f.transform.SetPositionFromWorldSpace(fpos);
                f.transform.UpdateStartPosition();

                f.tags.Add(name);

                if (p.paintable && useTexSelector && textureSelector.setTexture && ((f.renderer != null && f.renderer.texture != null) || (f.GetComponent<ScriptEntitySpawner>() != null)))
                {
                    ScriptEntitySpawner esT = f.GetComponent<ScriptEntitySpawner>();
                    if (f.GetComponent<ScriptEntitySpawner>() != null)
                    {
                        esT.spawnTexture = new TextureInfo(textureSelector.texturename, textureSelector.GetSourceRect());
                        esT.spawnTexture.scale = textureSelector.GetScale();
                    }
                    else
                    {
                        f.renderer.texture = new TextureInfo(textureSelector.texturename, textureSelector.GetSourceRect());
                        f.transform.scale = textureSelector.GetScale();
                    }
                }

                return f;
            }
            catch
            {
                console.Output.Append("[Error] Preset '" + name + "' tried to clone but was not found.");
                return null;
            }
        }

        public static GameObject CloneFromPreset(string name, Vector2 position, TextureInfo forceTexture, bool posIsTileSpace = false)
        {
            try
            {
                Preset p = presets[name.ToLower()];
                GameObject f = p.value?.Invoke();

                Vector2 fpos = position;

                if (posIsTileSpace && f.tile)   //if the given position is in tile space already, we need to turn it into world space as SetPositionFromWorldSpace
                    fpos *= GameWorld.TileSize; //Would try to convert a vlue already in tile space into tile space.

                f.transform.SetPositionFromWorldSpace(fpos);
                f.transform.UpdateStartPosition();

                f.tags.Add(name);

                if (p.paintable && forceTexture != null && f.renderer != null && f.renderer.texture != null)
                {
                    f.renderer.texture = new TextureInfo(forceTexture.name, forceTexture.sourceRectangle);
                    f.transform.scale = forceTexture.scale;
                }

                return f;
            }
            catch
            {
                console.Output.Append("[Error] Preset '" + name + "' tried to clone but was not found.");
                return null;
            }
        }

        public virtual object CreateComponent(string className)
        {
            return Activator.CreateInstance(Type.GetType(className));
        }

        public virtual void RegisterPresets()
        {
            presets.Add("env_worldwrap", new Preset("env_worldwrap", delegate ()
            {
                GameObject envObj = new GameObject(world);
                envObj.transform = new Transform(envObj);
                envObj.AddScript(new ScriptWorldWrap(envObj, new Rectangle(128, 0, 256, 1024)));
                envObj.renderer = new RendererNOOP(envObj, 0);
                envObj.forceActive = true;
                return envObj;
            }));

            presets.Add("func_tilesloped", new Preset("func_tilesloped", delegate ()
            {
                GameObject obj = new GameObject(world);
                obj.renderer = new RendererTile(obj, new TextureInfo("error"));
                obj.transform = new Transform(obj, scale: new Vector2(4));
                obj.collider = new ColliderTileSlope(obj, 0, GameWorld.TileSize / 2);
                obj.tile = true;
                return obj;
            }, true));

            presets.Add("func_tilenocollide", new Preset("func_tilenocollide", delegate ()
            {
                GameObject obj = new GameObject(world);
                obj.renderer = new RendererTile(obj, new TextureInfo("error"));
                obj.transform = new Transform(obj, scale: new Vector2(4));
                obj.tile = true;
                return obj;
            }, true));

            presets.Add("func_tilebreakable", new Preset("func_tilebreakable", delegate ()
            {
                GameObject spawnerObj = new GameObject(world);
                spawnerObj.transform = new Transform(spawnerObj, new Vector2(128, 0));
                //no renderer, is a spawner
                spawnerObj.AddScript(new ScriptEntitySpawner(spawnerObj, "internal_func_tilebreakable"));

                return spawnerObj;
            }, true));

            presets.Add("internal_func_tilebreakable", new Preset("internal_func_tilebreakable", delegate ()
            {
                GameObject tileObj = new GameObject(world);
                tileObj.renderer = new RendererTile(tileObj, new TextureInfo("error"));
                tileObj.transform = new Transform(tileObj, scale: new Vector2(4));
                tileObj.collider = new ColliderTile(tileObj, ColliderTile.TileCollideType.All);
                tileObj.AddScript(new ScriptDamagable(tileObj, GameWorld.TileSize + 4, 1, 0, "player"));
                tileObj.tile = true;
                return tileObj;
            }, true));

            //env
            presets.Add("env_background", new Preset("env_background", delegate ()
            {
                GameObject obj = new GameObject(world);
                obj.renderer = new RendererBackground(obj, new TextureInfo("error"), new Vector2(.25f, 0), new Vector2(0, 0));
                obj.transform = new Transform(obj);
                obj.forceActive = true;
                return obj;
            }, false));

            presets.Add("env_autoscroll", new Preset("env_autoscroll", delegate ()
            {
                GameObject envObj = new GameObject(world);
                envObj.transform = new Transform(envObj);
                envObj.AddScript(new ScriptAutoScroller(envObj, new Vector2(0, 0), new Vector2(1024, 0), new Vector2(1, 0)));
                envObj.forceActive = true;
                return envObj;
            }));
        }
    }
}
