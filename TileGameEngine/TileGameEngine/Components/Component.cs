﻿using BlueRavenUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Editor;

namespace TileGameEngine.Components
{
    public abstract class Component : ICloneable, IEditable
    {
        public GameObject owner;

        private bool _dead;
        public bool dead { get { return _dead; } set { bool prevded = _dead; _dead = value; if(!prevded) OnDeath(); } }

        public Dictionary<string, EditableValue> values { get; set; }

        public bool serializable = true;
        
        protected const int maxBytes = 128;

        protected int byteSize;

        /// <summary>
        /// DO NOT USE. FOR SERIALIZATION ONLY.
        /// </summary>
        public Component()
        {
            values = new Dictionary<string, EditableValue>();
        }

        public Component(GameObject owner)
        {
            this.owner = owner;

            values = new Dictionary<string, EditableValue>();
        }

        public virtual T GetAs<T>(Component obj) where T : Component
        {
            return obj as T;
        }

        public abstract void OnDeath();

        /// <summary>
        /// NOTE: when overriding, make sure to NOT call this base function.
        /// This function should only be used when there are NO dynamic bytes.
        /// </summary>
        /// <param name="writer"></param>
        public virtual void GetDynamicByteSize(ref int count)
        {
            
        }

        public int ReadDynamicByteSize(BinaryReader reader)
        {
            return reader.ReadInt32();
        }


        public abstract int GetByteSize();

        public abstract void Serialize(BinaryWriter writer);

        public abstract void Deserialize(BinaryReader reader);

        public abstract object Clone();

        public virtual void AddEditorValues()
        {
            values.Clear();
        }

        public virtual void OnRotated(Enums.DirectionClock direction)
        {
            //NOOP
        }

        public virtual void OnFlipped(Enums.DirectionMirror direction)
        {
            //NOOP
        }

        public virtual bool PreDestroy()
        {   //no objection by default
            return false;
        }
    }
}
