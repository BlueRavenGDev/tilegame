﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.CollidersR
{
    public class ColliderHandlerR
    {
        public ColliderHandlerR()
        {

        }

        public void Update(GameWorld world)
        {
            foreach (ColliderDynamic dynamic in world.dynamics.ToList())
            {
                dynamic.UpdatePosition();
                int xround = (int)(dynamic.owner.transform.position.X / GameWorld.TileSize);
                int yround = (int)(dynamic.owner.transform.position.Y / GameWorld.TileSize);

                for (int x = xround - 1; x <= xround + 1; x++)
                {
                    for (int y = yround - 1; y <= yround + 1; y++)
                    {
                        if (world.IsValidTile(x, y, dynamic.owner.transform.layer))
                        {
                            ColliderStaticTile st = world.tiles[dynamic.owner.transform.layer][x, y].colliderStatic;

                            if (st != null)
                            {   //extra safety
                                if (st.owner.active)
                                {
                                    dynamic.ResolveCollision(st);
                                }
                            }
                        }
                    }
                }

                foreach (ColliderStaticTile st in world.hybrids.ToList())
                {
                    if (st.owner.active)
                    {
                        dynamic.ResolveCollision(st);
                    }
                }

                dynamic.PostAllResolve();
            }
        }
    }
}
