﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.CollidersR
{
    public class ColliderDynamic : Component
    {
        public Vector2 hsLeft { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-10, 4); } }
        public Vector2 hsRight { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(10, 4); } }

        public Vector2 fsLeftT { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-9, 0); } }
        public Vector2 fsLeftB { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-9, 36); } }
        public Vector2 fsRightT { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(9, 0); } }
        public Vector2 fsRightB { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(9, 36); } }

        public Vector2 csLeftT { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-9, -36); } }
        public Vector2 csLeftB { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-9, 0); } }
        public Vector2 csRightT { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-9, -36); } }
        public Vector2 csRightB { get { return owner.transform.GetPositionInWorldSpace(true) + new Vector2(-9, 0); } }

        public bool falling, onGround;
        public ColliderStaticTile ground;

        public Vector2 velocity;
        private float _gspeed;
        public float gspeed
        {
            get { return _gspeed; }
            set
            {
                if (onGround && !falling)
                {
                    _gspeed = value; if (ground != null)
                    {
                        float angleRad = MathHelper.ToRadians(ground.angle);
                        velocity.X = _gspeed * (float)Math.Cos(angleRad);
                        velocity.Y = _gspeed * (float)Math.Sin(angleRad);
                    }
                }
            }
        }

        public bool groundMovedThisFrame;
        public float acceleration;
        public float deceleration;
        public float friction;
        public float gravity;

        private int fallSensorCount;
        public ColliderDynamic() : base()
        {
            onGround = false;
            falling = true;
        }

        public ColliderDynamic(GameObject owner) : base(owner)
        {
            onGround = false;
            falling = true;

            acceleration = 0.046875f;
            deceleration = 0.5f;
            friction = 0.046875f;
        }

        public void UpdatePosition()
        {
            groundMovedThisFrame = false;

            if (GameMain.keyboard.KeyHeld(Keys.A))
            {
                velocity.X -= 0.046875f;    //if we're on the ground, this value gets instantly overwritten
                gspeed -= 0.046875f;
                groundMovedThisFrame = true;
            }
            if (GameMain.keyboard.KeyHeld(Keys.D))
            {
                velocity.X += 0.046875f;    //if we're on the ground, this value gets instantly overwritten
                gspeed += 0.046875f;
                groundMovedThisFrame = true;
            }

            if (GameMain.keyboard.KeyPressed(Keys.Space))
            {
                Jump(new Vector2(0, -6.5f));
            }

            if (!groundMovedThisFrame)
            {
                gspeed -= MathHelper.Min(Math.Abs(gspeed), friction) * Math.Sign(gspeed);
            }

            if (!onGround && falling)
                velocity.Y += 0.21875f;

            owner.transform.SetPositionFromWorldSpace(velocity, true);
        }

        public void Jump(Vector2 jumpVector)
        {
            onGround = false;
            falling = true;
            ground = null;

            velocity += jumpVector;
        }

        public bool ResolveCollision(ColliderStaticTile collider)
        {
            return collider.ResolveCollision(this);
        }

        public bool ResolveWallSensor(ColliderStaticTile collider)
        {
            bool resolved = false;

            bool inY = collider.InVertical(hsLeft.Y, collider.top, collider.bottom);
            bool leftIn = collider.InHorizontal(collider.left, hsLeft.X, hsRight.X); //collider.InHorizontal(hsLeft.X, collider.left, collider.right);
            bool rightIn = collider.InHorizontal(collider.right, hsLeft.X, hsRight.X); //collider.InHorizontal(hsRight.X, collider.left, collider.right);
            if (!collider.leftCollide || !inY) leftIn = false;
            if (!collider.rightCollide || !inY) rightIn = false;

            //if any of these two points, or the space between them is inside a tile

            if (leftIn && !rightIn)
            {   //if only the left is in the tile, we can move the player to the left side + width + 1.
                //ignore y, as this is only a horizontal sensor.
                owner.transform.SetPositionFromWorldSpace(new Vector2(collider.left - 11, owner.transform.GetPositionInWorldSpace(true).Y));
                resolved = true;
                velocity.X = 0;
                gspeed = 0;
            }
            if (rightIn && !leftIn)
            {   //same thing, but for the right.
                owner.transform.SetPositionFromWorldSpace(new Vector2(collider.right + 11, owner.transform.GetPositionInWorldSpace(true).Y));
                resolved = true;
                velocity.X = 0;
                gspeed = 0;
            }
            //Case where both is in? probably not necessary, limit width to <= tilesize?

            return resolved;
        }

        public bool ResolveFloorSensor(ColliderStaticTile collider)
        {
            //since we only have to check one side with this sensor, we can get away with returning without any logic.
            if (!collider.topCollide)
                return false;

            bool resolved = false;

            if (collider.InHorizontal(fsLeftB.X, collider.left, collider.right) || collider.InHorizontal(fsRightB.X, collider.left, collider.right))
            {
                float topLAV = collider.GetTopY(fsLeftB.X);
                float topRAV = collider.GetTopY(fsRightB.X);
                float topL = collider.top + collider.GetTopY(fsLeftB.X);
                float topR = collider.top + collider.GetTopY(fsRightB.X);
                //doesn't matter if we use T or B to calculate; they both have the exact same X position

                bool botInL = collider.InVertical(collider.bottom, fsLeftT.Y, fsLeftB.Y) && topLAV > -1;
                bool topInL = collider.InVertical(topL, fsLeftT.Y, fsLeftB.Y) && topLAV > -1;   //NOTE: top of the TILE, not of the dynamic!
                bool justtopL = topInL && !botInL;

                bool botInR = collider.InVertical(collider.bottom, fsRightT.Y, fsRightB.Y) && topLAV > -1;
                bool topInR = collider.InVertical(topR, fsRightT.Y, fsRightB.Y) && topLAV > -1;
                bool justtopR = topInR && !botInR;

                if ((topInL && botInL) ||       //both the bottom and the top
                    justtopL)                   //OR just the top
                {   //both the bottom and the top, OR just the top, are in the sensor.
                    //if the bottom alone was allowed to be in the sensor, we'd be popping up the side of a tile randomly!
                    //NOTE: this may not be necessary, as our horizontal sensors should prevent this from happening, since they're wider and resolve first

                    if ((onGround && !falling) || (!onGround && falling && owner.transform.GetPositionInWorldSpace(true).Y > topL - 20 && velocity.Y >= 0))
                    {   //if the player is ON THE GROUND, 'suck' them to the floor
                        //otherwise, if the player is less than what we would resolve to - (top - 20) - we resolve
                        owner.transform.SetPositionFromWorldSpace(new Vector2(owner.transform.GetPositionInWorldSpace(true).X, topL - 20));
                        fallSensorCount++;
                        resolved = true;
                        velocity.Y = 0;
                        ground = collider;
                    }
                    //we don't change X position - only y
                }

                if ((topInR && botInR) ||       //both the bottom and the top
                    justtopR)                   //OR just the top
                {   //exact same thing here, but for the right sensor
                    if ((onGround && !falling) || (!onGround && falling && owner.transform.GetPositionInWorldSpace(true).Y > topR - 20 && velocity.Y >= 0))
                    {
                        owner.transform.SetPositionFromWorldSpace(new Vector2(owner.transform.GetPositionInWorldSpace(true).X, topR - 20));
                        fallSensorCount++;
                        resolved = true;
                        velocity.Y = 0;
                        ground = collider;
                    }
                }
            }

            return resolved;
        }

        public bool ResolveCeilSensor(ColliderStaticTile collider)
        {   //this sensor is pretty much identical to the floor one, including logic. Pretty much copy pasted.
            bool resolved = false;

            //ceiling is only relevant while in the air.
            if (onGround && !falling)
                return false;

            if (collider.InHorizontal(csLeftB.X, collider.left, collider.right) || collider.InHorizontal(csRightB.X, collider.left, collider.right))
            {
                float botL = collider.bottom + collider.GetTopY(csLeftB.X);
                float botR = collider.bottom + collider.GetTopY(csRightB.X);
                //doesn't matter if we use T or B to calculate; they both have the exact same X position

                bool botInL = collider.InVertical(collider.bottom, csLeftT.Y, csLeftB.Y);
                bool topInL = collider.InVertical(collider.top, csLeftT.Y, csLeftB.Y);   //NOTE: top of the TILE, not of the dynamic!

                bool botInR = collider.InVertical(collider.bottom, csRightT.Y, csRightB.Y);
                bool topInR = collider.InVertical(collider.top, csRightT.Y, csRightB.Y);
                bool justtopR = topInR && !botInR;

                if (topInL || botInL)
                {   //both the bottom and the top, OR just the top, are in the sensor.
                    //if the bottom alone was allowed to be in the sensor, we'd be popping up the side of a tile randomly!
                    //NOTE: this may not be necessary, as our horizontal sensors should prevent this from happening, since they're wider and resolve first

                    if ((onGround && !falling) || (!onGround && falling && owner.transform.GetPositionInWorldSpace(true).Y > collider.bottom - 20 && velocity.Y <= 0))
                    {   //if the player is ON THE GROUND, 'suck' them to the floor
                        //otherwise, if the player is less than what we would resolve to - (top - 20) - we resolve
                        owner.transform.SetPositionFromWorldSpace(new Vector2(owner.transform.GetPositionInWorldSpace(true).X, collider.bottom - 20));
                        resolved = true;
                        velocity.Y = 0;
                    }
                    //we don't change X position - only y
                }

                if ((topInR && botInR) ||       //both the bottom and the top
                    justtopR)                   //OR just the top
                {   //exact same thing here, but for the right sensor
                    if ((onGround && !falling) || (!onGround && falling && owner.transform.GetPositionInWorldSpace(true).Y > collider.bottom - 20 && velocity.Y <= 0))
                    {
                        owner.transform.SetPositionFromWorldSpace(new Vector2(owner.transform.GetPositionInWorldSpace(true).X, collider.bottom - 20));
                        resolved = true;
                        velocity.Y = 0;
                    }
                }
            }

            return resolved;
        }

        public void PostAllResolve()
        {
            if (fallSensorCount == 0)
            {   //we're not touching the ground at all
                falling = true;
                ground = null;
                onGround = false;
            }
            else if (fallSensorCount == 1)
            {   //we're only touching one tile
                //ground is set in ResolveSolidTileVertical
                falling = false;
                onGround = true;
            }
            else if (fallSensorCount >= 2)
            {   //shouldn't ever be more than 2, but just to be safe
                //ground is set in ResolveSolidTileVertical
                falling = false;
                onGround = true;
            }

            fallSensorCount = 0;
        }

        public void DrawDebug(SpriteBatch batch)
        {
            batch.DrawLine(hsLeft, hsRight, Color.Black, 4);

            batch.DrawLine(fsLeftT, fsLeftB, Color.Black, 4);
            batch.DrawLine(fsRightT, fsRightB, Color.Black, 4);
        }

        public override void OnDeath()
        {
            throw new NotImplementedException();
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public override int GetByteSize()
        {
            throw new NotImplementedException();
        }

        public override void Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public override void Deserialize(BinaryReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
