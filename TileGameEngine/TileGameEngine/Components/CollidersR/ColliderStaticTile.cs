﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BlueRavenUtility;

namespace TileGameEngine.Components.CollidersR
{
    public class ColliderStaticTile : Component
    {
        public float left { get { return owner.transform.GetPositionInWorldSpace(true).X; } }
        public float right { get { return owner.transform.GetPositionInWorldSpace(true).X + GameWorld.TileSize; } }
        public float top {get { return owner.transform.GetPositionInWorldSpace(true).Y; } }
        public float bottom { get { return owner.transform.GetPositionInWorldSpace(true).Y + GameWorld.TileSize; } }

        public bool leftCollide = true, rightCollide = true, topCollide = true, bottomCollide = true;

        private float _angle;
        public float angle { get { return _angle; } private set { _angle = EngineMathHelper.Mod(value, 360); } }

        public bool hybrid { get { return !owner.tile; } }  //we're a hybrid if the owner is not a tile - i.e. we exist in world space, not tile space

        public bool flipX, flipY;

        public ColliderStaticTile() : base()
        {

        }

        public ColliderStaticTile(GameObject owner) : base(owner)
        {
        }

        public bool ResolveCollision(ColliderDynamic collider)
        {
            bool resolved = false;

            Vector2 pos = owner.transform.GetPositionInWorldSpace(true);    //our position
            Vector2 otherPos = collider.owner.transform.GetPositionInWorldSpace(true);  //dynamic's position

            float left = pos.X;
            float right = pos.X + GameWorld.TileSize;
            float top = pos.Y;
            float bot = pos.Y + GameWorld.TileSize;

            //horizontal sensor
            if (collider.ResolveWallSensor(this))
                resolved = true;

            //falling sensors (2, vertical)
            if (collider.ResolveFloorSensor(this))
                resolved = true;

            if (collider.ResolveCeilSensor(this))
                resolved = true;

            return resolved;
        }
        
        /// <summary>
        /// Gets the height to resolve upon.
        /// Note: relative to this tile - i.e. 0 = <see cref="top"/>
        /// </summary>
        /// <param name="xpoint">the x point used to calculate the height.</param>
        /// <returns>The height to resolve to (relative to tile)</returns>
        public virtual float GetTopY(float xpoint)
        {
            return 0;
        }

        private bool InBounds(Vector2 point, float left, float right, float top, float bot)
        {
            bool inx = InHorizontal(point.X, left, right);
            bool iny = InVertical(point.Y, top, bot);
            return inx && iny;
        }

        public bool InHorizontal(float xpoint, float left, float right)
        {
            return xpoint > left && xpoint < right;
        }

        public bool InVertical(float ypoint, float top, float bot)
        {
            return ypoint >= top && ypoint < bot;
        }

        public int[] GetHeightMap(int index, bool flipX = false, bool flipY = false)
        {
            int[] fhm = new int[16];
            if (index == 0)
            {
                fhm = new int[] { 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 };
            }
            else if (index == 1)
            {
                fhm = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3 };
            }
            else if (index == 2)
            {
                fhm = new int[] { 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9 };
            }
            else if (index == 3)
            {
                fhm = new int[] { 10, 10, 11, 12, 12, 13, 14, 14, 15, 16, 16, 16, 16, 16, 16, 16 };
            }

            if (flipX)
            {
                Array.Reverse(fhm);
            }
            if (flipY)
            {
                int[] temp = fhm.ToArray();

                for (int i = 0; i < temp.Length; i++)
                {
                    temp[i] = Math.Abs(fhm[i] - fhm.Length);
                }
            }
            if (fhm.Length != 16)
                throw new Exception("Heightmap value not 16 wide");
            return fhm;
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public override int GetByteSize()
        {
            throw new NotImplementedException();
        }

        public override void Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public override void Deserialize(BinaryReader reader)
        {
            throw new NotImplementedException();
        }

        public override void OnDeath()
        {
            throw new NotImplementedException();
        }
    }
}
