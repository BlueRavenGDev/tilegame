﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;
using BlueRavenUtility;
using TileGameEngine.Components.Scripts;
using TileGameEngine.Editor;

namespace TileGameEngine.Components
{
    public class Transform : Component
    {
        public Vector2 startPosition { get; private set; }
        public Vector2 prevPosition;
        public Vector2 moveDistance { get { return GetPositionInWorldSpace() - prevPosition; } }
        public Vector2 position; //{ get { return _position; } set { prevPosition = _position; _position = value;} }

        public Vector2 prevScale;
        public Vector2 scale; //{ get { return _scale; } set { _scale = value; } }

        public float prevRotation;
        private float _rotation;
        public float rotation { get { return _rotation; } set { _rotation = value; } }

        public int prevLayer;
        private int _layer;
        public int layer { get { return _layer; } set { _layer = value; } }

        public bool isParentRelative { get; private set; }

        public Vector2 up
        {
            get
            {
                return Vector2.Normalize(Vector2.Transform(new Vector2(0, -1), Matrix.CreateRotationZ(MathHelper.ToRadians(rotation))));
            }
        }

        public Vector2 left
        {
            get
            {
                return Vector2.Normalize(Vector2.Transform(new Vector2(-1, 0), Matrix.CreateRotationZ(MathHelper.ToRadians(rotation))));
            }
        }

        public Vector2 right
        {
            get
            {
                return Vector2.Normalize(Vector2.Transform(new Vector2(1, 0), Matrix.CreateRotationZ(MathHelper.ToRadians(rotation))));
            }
        }

        public Vector2 down
        {
            get
            {
                return Vector2.Normalize(Vector2.Transform(new Vector2(0, 1), Matrix.CreateRotationZ(MathHelper.ToRadians(rotation))));
            }
        }

        public Vector2 worldUp
        {
            get
            {
                return new Vector2(0, -1);
            }
        }

        public Vector2 worldLeft
        {
            get
            {
                return new Vector2(-1, 0);
            }
        }

        public Vector2 worldDown
        {
            get
            {
                return new Vector2(0, 1);
            }
        }

        public Vector2 worldRight
        {
            get
            {
                return new Vector2(1, 0);
            }
        }

        public Transform()
        {

        }

        public Transform(GameObject owner, Vector2? position = null, Vector2? scale = null, float rotation = 0) : base(owner)
        {
            startPosition = position == null ? Vector2.Zero : position.Value;
            this.position = startPosition;
            this.scale = scale == null ? Vector2.One : scale.Value;
            this.rotation = rotation;
        }

        /// <summary>
        /// Gets the position in world space.
        /// Multiplies position by <see cref="GameWorld.TileSize"/> if owner is a tile.
        /// Otherwise returns position.
        /// </summary>
        public Vector2 GetPositionInWorldSpace(bool worldspace = false)
        {
            if (owner.tile && owner.parent != null)
                GameMain.console.Output.Append("[Warn] Tile is parented to another object! This should not happen and may result in funky stuff.");
            if (owner.tile)
                return position * GameWorld.TileSize ;
            else return position;
        }

        /// <summary>
        /// Updates the start position to be the current position.
        /// </summary>
        public void UpdateStartPosition()
        {
            startPosition = position;
        }

        /// <summary>
        /// Sets the position in world space.
        /// Divides given position by <see cref="GameWorld.TileSize"/> if owner is a tile.
        /// Otherwise sets position to given position.
        /// </summary>
        /// <param name="newpos">the vector (in world space, NOT tile space) to set the position to</param>
        public void SetPositionFromWorldSpace(Vector2 newpos, bool relative = false)
        {
            prevPosition = position;
            prevScale = scale;
            prevRotation = rotation;
            prevLayer = layer;

            if (owner.tile)
            {
                if (relative)
                    position += newpos / GameWorld.TileSize;
                else position = newpos / GameWorld.TileSize;

                //position.X = (int)position.X.RoundDown(GameWorld.TileSize);
                //position.Y = (int)position.Y.RoundDown(GameWorld.TileSize);
            }
            else if (relative)
                position += newpos;
            else position = newpos;

            if (float.IsNaN(position.X))
                position.X = prevPosition.X;
            if (float.IsNaN(position.Y))
                position.Y = prevPosition.Y;

            if (owner.children.Count > 0)
            {
                foreach (GameObject child in owner.children)
                {
                    child.transform.SetPositionFromWorldSpace(moveDistance, true);
                }
            }

            foreach (Script script in owner.scripts)
            {
                script.OnTransformChanged(prevPosition, prevScale, prevRotation, prevLayer);
            }
        }

        /// <summary>
        /// Translates the position to be relative to the parent's transform.
        /// NOTE: not applicable to tiles. Will not do anything to them. However, will work if the parent is a tile.
        /// </summary>
        public void TranslatePostionParentRelative(Transform ownerParent, bool untranslate = false)
        {
            if (!untranslate)
            {
                if (!owner.tile)
                {
                    Vector2 offset = (ownerParent.owner.tile ? new Vector2(GameWorld.TileSize) : Vector2.Zero);
                    position = (ownerParent.GetPositionInWorldSpace() + offset) - position;
                    isParentRelative = true;
                }
            }
            else
            {
                position += ownerParent.position;
                isParentRelative = false;
            }
        }
        
        public override object Clone()
        {
            return new Transform(owner, position, scale, rotation);
        }

        public override int GetByteSize()
        {
            return 4 + 4 +  //pos xy
                4 + 4 +     //startpos xy
                4 + 4 +     //scale xy
                4 +         //rot
                4;          //layer
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(position.X);
            writer.Write(position.Y);

            writer.Write(startPosition.X);
            writer.Write(startPosition.Y);

            writer.Write(scale.X);
            writer.Write(scale.Y);

            writer.Write(rotation);

            writer.Write(layer);
        }

        public override void Deserialize(BinaryReader reader)
        {
            position.X = reader.ReadSingle();
            position.Y = reader.ReadSingle();

            startPosition = new Vector2(reader.ReadSingle(), reader.ReadSingle());

            scale.X = reader.ReadSingle();
            scale.Y = reader.ReadSingle();

            rotation = reader.ReadSingle();

            layer = reader.ReadInt32();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("position", new EditableValue(x => {
                if (owner.tile)
                {
                    owner.parentWorld.DeleteTile(owner.transform.layer, owner.transform.position.X, owner.transform.position.Y);

                    if (owner.parentWorld.tiles[layer][(int)((Vector2)x).X, (int)((Vector2)x).Y] != null)
                        owner.parentWorld.tiles[layer][(int)((Vector2)x).X, (int)((Vector2)x).Y].Destroy(true);

                    owner.parentWorld.tiles[layer][(int)((Vector2)x).X, (int)((Vector2)x).Y] = owner;
                }
                startPosition = (Vector2)x;
                position = (Vector2)x; }, delegate () { return position; }));

            values.Add("scale", new EditableValue(x => { scale = (Vector2)x; }, delegate () { return scale; }));
            values.Add("rotation", new EditableValue(x => { rotation = (float)x; }, delegate () { return rotation; }));

            values.Add("layer", new EditableValue(x => { layer = (int)x; }, delegate () { return layer; }));
        }

        public override string ToString()
        {
            return position.ToString() + " sX:" + scale.X + " sY:" + scale.Y + " R:" + rotation + " L:" + layer;
        }

        public override void OnDeath()
        {
            owner.transform = null;
        }
    }
}
