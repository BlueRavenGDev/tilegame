﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlueRavenUtility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileGameEngine.Components.Renderers
{
    public class RendererText : Renderer
    {
        private string fontname;
        private SpriteFont font;
        private string text;
        private Color color;

        public RendererText()
        {
        }

        public RendererText(GameObject owner, string fontname, string text, Color color, Enums.Alignment alignment, int renderPriority, RelativeTo relative) : base(owner, alignment, renderPriority)
        {
            this.fontname = fontname;
            font = GameWorld.assets.fonts.GetAsset(fontname);
            this.text = text;
            this.color = color;
            this.relative = relative;
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public override void Render(SpriteBatch batch, BasicEffect basicEffect)
        {
            if (relative == RelativeTo.Camera)
            {
                Vector2 pos = owner.transform.GetPositionInWorldSpace(true);
                pos += TextHelper.GetAlignmentOffset(font, text, new Rectangle(-4, -4, 8, 8), alignment);
                pos = VectorHelper.WorldToScreenCoords(pos);
                batch.DrawString(font, text, pos, color);
            }
            else
            {   //relative to world
                Vector2 pos = owner.transform.GetPositionInWorldSpace(true);
                pos += TextHelper.GetAlignmentOffset(font, text, new Rectangle(-4, -4, 8, 8), alignment);
                batch.DrawString(font, text, pos, color);
            }
        }

        public override void GetDynamicByteSize(ref int count)
        {
            count += fontname.Length * sizeof(char);
            count += text.Length * sizeof(char);
        }

        public override int GetByteSize()
        {
            return base.GetByteSize() + 4;  //color rgba = 4byte
        }

        public override void Serialize(BinaryWriter writer)
        {
            base.Serialize(writer);

            writer.Write(fontname);
            writer.Write(text);

            writer.Write(color.R);
            writer.Write(color.G);
            writer.Write(color.B);
            writer.Write(color.A);
        }

        public override void Deserialize(BinaryReader reader)
        {
            base.Deserialize(reader);

            fontname = reader.ReadString();
            font = GameWorld.assets.fonts.GetAsset(fontname);
            text = reader.ReadString();

            color.R = reader.ReadByte();
            color.G = reader.ReadByte();
            color.B = reader.ReadByte();
            color.A = reader.ReadByte();
        }
    }
}
