﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlueRavenUtility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileGameEngine.Components.Renderers
{
    public class RendererNOOP : Renderer
    {
        public RendererNOOP()
        {
        }

        public RendererNOOP(GameObject owner, int renderPriority) : base(owner, Vector2.Zero, renderPriority)
        {
        }

        public override object Clone()
        {
            return new RendererNOOP(owner, renderPriority);    
        }

        public override void Render(SpriteBatch batch, BasicEffect basicEffect)
        {
            return; //NOOP
        }
    }
}
