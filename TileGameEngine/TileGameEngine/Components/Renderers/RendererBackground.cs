﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BlueRavenUtility;
using System.IO;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Renderers
{
    public class RendererBackground : Renderer
    {
        public Vector2 scroll;

        public RendererBackground() : base()
        {

        }

        public RendererBackground(GameObject owner, TextureInfo texture, Vector2 scroll, Vector2 offset = default(Vector2)) : base(owner, offset, 0)
        {
            this.scroll = scroll;
            this.texture = texture;
        }

        public override object Clone()
        {
            return new RendererBackground(owner, texture, scroll, offset);
        }

        public override void Render(SpriteBatch batch, BasicEffect basicEffect)
        {
            Vector2 pos = new Vector2(Camera.camera.Position.X, Camera.camera.Position.Y) - new Vector2(4);
            //RectangleF rect = new RectangleF((Camera.camera.Position.X - 4), (Camera.camera.Position.Y - 4), texture.width, texture.height);
            RectangleF rectS = new RectangleF(offset.X + pos.X * scroll.X, offset.Y + pos.Y * scroll.Y, GameMain.WIDTH + offset.X + 8, GameMain.HEIGHT + offset.Y + 8);
            batch.Draw(texture.baseTexture, pos, rectS.ToRectangle(), Color.White, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0);
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("scroll", new EditableValue(x => { scroll = (Vector2)x; }, delegate () { return scroll; }));
        }

        public override int GetByteSize()
        {
            return base.GetByteSize() + 4 + 4;  //scroll xy
        }

        public override void Serialize(BinaryWriter writer)
        {
            base.Serialize(writer);

            writer.Write(scroll.X);
            writer.Write(scroll.Y);
        }

        public override void Deserialize(BinaryReader reader)
        {
            base.Deserialize(reader);

            scroll.X = reader.ReadSingle();
            scroll.Y = reader.ReadSingle();
        }
    }
}
