﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;
using BlueRavenUtility;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Renderers
{
    public enum RelativeTo
    {
        World,
        Camera
    }

    public abstract class Renderer : Component, ICloneable
    {
        public TextureInfo texture;
        public TextureInfo[] altTextures;

        public Vector2 offset;
        public Enums.Alignment alignment;

        public int renderPriority;

        protected RelativeTo relative;

        public Renderer() : base()
        {
            altTextures = new TextureInfo[4];
            values = new Dictionary<string, EditableValue>();
            renderPriority = 0;
        }

        public Renderer(GameObject owner, Enums.Alignment alignment, int renderPriority) : base(owner)
        {
            this.alignment = alignment;

            altTextures = new TextureInfo[4];

            this.renderPriority = renderPriority;
        }

        public Renderer(GameObject owner, Vector2? offset = null, int renderPriority = 1) : base(owner)
        {
			this.offset = offset == null ? Vector2.Zero : offset.Value;

            altTextures = new TextureInfo[4];

            this.renderPriority = renderPriority;
        }
		
        public abstract void Render(SpriteBatch batch, BasicEffect basicEffect);

        public virtual void RenderDebug(SpriteBatch batch)
        {
            if (GameMain.editing)
            {
                Vector2 pos = owner.transform.position * (owner.tile ? GameWorld.TileSize : 1);

                batch.DrawRectangle(new RectangleF(pos.X - 8, pos.Y - 8, 16, 16), Color.Orange);
                if (owner.selected)
                    batch.DrawHollowRectangle(new RectangleF(pos.X - 10, pos.Y - 10, 20, 20), 2, Color.Black);
            }
        }

        public override void OnFlipped(Enums.DirectionMirror direction)
        {
            base.OnFlipped(direction);

            if (texture != null)
            {
                if (direction == Enums.DirectionMirror.Horizontal)
                {
                    if (texture.flip.Has(SpriteEffects.FlipHorizontally))
                    {
                        texture.flip = texture.flip.Remove(SpriteEffects.FlipHorizontally);
                    }
                    else texture.flip = texture.flip.Add(SpriteEffects.FlipHorizontally);
                }
                if (direction == Enums.DirectionMirror.Vertical)
                {
                    if (texture.flip.Has(SpriteEffects.FlipVertically))
                    {
                        texture.flip = texture.flip.Remove(SpriteEffects.FlipVertically);
                    }
                    else texture.flip = texture.flip.Add(SpriteEffects.FlipVertically);
                }
            }
        }

        public override abstract object Clone();

        public override int GetByteSize()
        {
            return 1 +  //texture exists check 
                4 + 4;  //offset xy
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(texture != null);

            writer.Write(offset.X);
            writer.Write(offset.Y);
            /*AddBit(texture != null);
            CompleteBits();
            AddBytes(BitConverter.GetBytes(offset.X));
            AddBytes(BitConverter.GetBytes(offset.Y));*/

            texture?.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader)
        {
            /*byte[] chunk = reader.ReadBytes(maxBytes);
            int o = 0;
            bool hasTex = GetBools(chunk, ref o)[0];

            offset.X = GetFloat(chunk, ref o);
            offset.Y = GetFloat(chunk, ref o);*/

            bool hasTex = reader.ReadBoolean();

            offset.X = reader.ReadSingle();
            offset.Y = reader.ReadSingle();

            if (hasTex)
            {
                texture = new TextureInfo();
                texture.Deserialize(reader);
            }
        }

        protected Vector2 GetOffsetFromAlignment()
        {
            if (alignment != Enums.Alignment.Center)
            {
                Rectangle sourceRect = texture.GetCurrentSourceRectangle();
                switch (alignment)
                {
                    case Enums.Alignment.Bottom:
                            return new Vector2(0, (sourceRect.Height / 2));

                    case Enums.Alignment.BottomLeft:
                            return new Vector2(-(sourceRect.Width / 2), (sourceRect.Height / 2));

                    case Enums.Alignment.BottomRight:
                            return new Vector2((sourceRect.Width / 2), (sourceRect.Height / 2));

                    case Enums.Alignment.Left:
                            return new Vector2(-(sourceRect.Width / 2), 0);

                    case Enums.Alignment.Right:
                            return new Vector2((sourceRect.Width / 2), 0);

                    case Enums.Alignment.Top:
                        return new Vector2(0, -(sourceRect.Height / 2));

                    case Enums.Alignment.TopLeft:
                        return new Vector2(-(sourceRect.Width / 2), -(sourceRect.Height / 2));

                    case Enums.Alignment.TopRight:
                        return new Vector2((sourceRect.Width / 2), -(sourceRect.Height / 2));
                }
            }

            return Vector2.Zero;
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("offset", new EditableValue(x => { offset = (Vector2)x; }, delegate () { return offset; }));

            if (texture != null)
            {
                texture.AddEditorValues();

                foreach (KeyValuePair<string, EditableValue> p in texture.values)
                    values.Add(p.Key, p.Value);
            }
        }

        public override void OnDeath()
        {
            owner.renderer = null;    
        }
    }
}