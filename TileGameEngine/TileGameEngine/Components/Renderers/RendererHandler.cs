﻿using BlueRavenUtility;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Renderers
{
    public class RendererHandler
    {
        private bool hasSorted;

        public void Render(GameWorld world, SpriteBatch batch)
        {
            if (!hasSorted)
            {
                world.renderers = world.renderers.OrderBy(x => 
                {
                    return x.renderPriority + 1 * x.owner.transform.layer;
                }).ToList(); 
                hasSorted = true;
            }

            world.renderers.ForEach(x => 
            {
                bool canR = false;
                if (GameMain.editPanel.drawLayerOnly)
                {
                    if (x.owner.transform.layer == GameMain.editPanel.currentEditingLayer)
                        canR = true;
                }
                else canR = true;

                if (x.owner.active && canR)
                {
                    x.Render(batch, GameMain.assets.basicEffect);
                    x.owner.scripts.ForEach(x1 => x1.OnDraw(batch));

                    x.owner.colliderDynamic?.DrawDebug(batch);
                }
            });

            if (GameMain.editing)
            {
                world.renderers.ForEach(x =>
                {
                    bool canRD = false;
                    if (x.owner.tile && GameMain.editPanel.drawTileMarkers)
                        canRD = true;
                    if (!x.owner.tile && GameMain.editPanel.drawOtherMarkers)
                        canRD = true;

                    if (x.owner.active && canRD)
                    {
                        x.RenderDebug(batch);
                        x.owner.scripts.ForEach(x1 => x1.OnDrawDebug(batch));
                    }
                });
            }

            world.renderers.CheckAndDelete(x => x.dead);
        }

        public void AddRenderer(GameWorld world, Renderer renderer, bool forceSort = true)
        {
            world.renderers.Add(renderer);
            if (forceSort)
                hasSorted = false;
        }
    }
}
