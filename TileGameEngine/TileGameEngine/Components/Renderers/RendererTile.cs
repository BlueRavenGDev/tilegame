﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using BlueRavenUtility;
namespace TileGameEngine.Components.Renderers
{
    public class RendererTile : Renderer
    {
        public RendererTile()
        {

        }

        public RendererTile(GameObject owner, TextureInfo topTexture, Vector2? offset = null) : base(owner, offset, 64)
        {
            texture = topTexture;
        }

        public override void Render(SpriteBatch batch, BasicEffect basicEffect)
        {
            float width = texture.GetCurrentSourceRectangle().Width;
            float height = texture.GetCurrentSourceRectangle().Height;
            Vector2 newOffset = (offset / new Vector2(width, height));
            if (float.IsInfinity(newOffset.X))
                newOffset.X = offset.X;
            if (float.IsInfinity(newOffset.Y))
                newOffset.Y = offset.Y;

            Vector2 origin = texture.center + -newOffset;

            if (owner.selected)
            {
                batch.Draw(texture.baseTexture, owner.transform.position * GameWorld.TileSize - new Vector2(1, 0), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), Vector2.Zero, owner.transform.scale, SpriteEffects.None, 0);
                batch.Draw(texture.baseTexture, owner.transform.position * GameWorld.TileSize - new Vector2(0, 1), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), Vector2.Zero, owner.transform.scale, SpriteEffects.None, 0);
                batch.Draw(texture.baseTexture, owner.transform.position * GameWorld.TileSize - new Vector2(-1, 0), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), Vector2.Zero, owner.transform.scale, SpriteEffects.None, 0);
                batch.Draw(texture.baseTexture, owner.transform.position * GameWorld.TileSize - new Vector2(0, -1), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), Vector2.Zero, owner.transform.scale, SpriteEffects.None, 0);
            }

            Vector2 scale = new Vector2(GameWorld.TileSize) / new Vector2(texture.GetCurrentSourceRectangle().Width, texture.GetCurrentSourceRectangle().Height);
            batch.Draw(texture.baseTexture, owner.transform.GetPositionInWorldSpace(true), texture.GetCurrentSourceRectangle(), 
                Color.White, MathHelper.ToRadians(owner.transform.rotation), Vector2.Zero, 
                scale, texture.flip, 0);

            if (owner.collider is Colliders.ColliderTileSlope)
            {
                Colliders.ColliderTileSlope c = (Colliders.ColliderTileSlope)owner.collider;

                c.DrawDebug(batch);
            }

            if (owner.collider is Colliders.ColliderTile)
            {
                Colliders.ColliderTile c = (Colliders.ColliderTile)owner.collider;

                c.DrawDebug(batch);
            }

            if (owner.collider is Colliders.ColliderRectangle)
            {
                Colliders.ColliderRectangle c = (Colliders.ColliderRectangle)owner.collider;
                batch.DrawHollowCircle(owner.transform.position, c.height, Color.Red, 2, 32);
            }

            if (owner.collider is Colliders.ColliderTileCheckNear)
            {
                Colliders.ColliderTileCheckNear c = (Colliders.ColliderTileCheckNear)owner.collider;
                c.DrawDebug(batch);
            }

            if (owner.children.Count > 0)
            {
                foreach (GameObject child in owner.children)
                {
                    batch.DrawLine(owner.transform.GetPositionInWorldSpace(true), child.transform.GetPositionInWorldSpace(true), Color.Black);
                }
            }
        }

        public override object Clone()
        {
            return new RendererTile(owner, (TextureInfo)texture.Clone(), offset);
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();
        }
    }
}
