﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using BlueRavenUtility;
using System.IO;
using TileGameEngine.Components.Scripts;

namespace TileGameEngine.Components.Renderers
{
    public class RendererPanel : Renderer
    {
        public static bool DEBUGdrawCollidableTiles;

        public RendererPanel() : base()
        {

        }

        public RendererPanel(GameObject owner, TextureInfo texture, Enums.Alignment alignment) : base(owner, alignment, 128)
        {
            base.texture = texture;
        }

        public RendererPanel(GameObject owner, TextureInfo texture, Vector2? offset = null) : base(owner, offset, 128)
        {
            base.texture = texture;
        }

        public override void Render(SpriteBatch batch, BasicEffect basicEffect)
        {
            ScriptDamagable damagescript = owner.GetComponent<ScriptDamagable>();

            bool isnew = false;
            Texture2D useTexture = texture.baseTexture;
            if (damagescript != null)
            {
                if (damagescript.invulnTime > 0 && damagescript.invulnTime % 2 == 0)
                {
                    return; //I guess we'll just flash on and off because xna sucks
                }
            }

            float width = texture.GetCurrentSourceRectangle().Width;
            float height = texture.GetCurrentSourceRectangle().Height;
            Vector2 newOffset = (offset / new Vector2(width, height));
            if (float.IsInfinity(newOffset.X))
                newOffset.X = offset.X;
            if (float.IsInfinity(newOffset.Y))
                newOffset.Y = offset.Y;

            Vector2 origin = texture.originCenter;

            if (alignment != Enums.Alignment.Center)
                origin += GetOffsetFromAlignment();
            else
                origin = texture.originCenter + -offset;

            //outline for selecting
            if (owner.selected)
            {
                batch.Draw(useTexture, owner.transform.position - new Vector2(1, 0), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), origin, owner.transform.scale, SpriteEffects.None, 0);
                batch.Draw(useTexture, owner.transform.position - new Vector2(0, 1), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), origin, owner.transform.scale, SpriteEffects.None, 0);
                batch.Draw(useTexture, owner.transform.position - new Vector2(-1, 0), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), origin, owner.transform.scale, SpriteEffects.None, 0);
                batch.Draw(useTexture, owner.transform.position - new Vector2(0, -1), texture.GetCurrentSourceRectangle(), Color.Black, MathHelper.ToRadians(owner.transform.rotation), origin, owner.transform.scale, SpriteEffects.None, 0);
            }

            batch.Draw(texture.baseTexture, owner.transform.position, texture.GetCurrentSourceRectangle(), Color.White, 
                MathHelper.ToRadians(owner.transform.rotation), origin, owner.transform.scale, texture.flip, 0);

            if (DEBUGdrawCollidableTiles)
            {
                if (owner.collider is Colliders.ColliderRectangle)
                {
                    Colliders.ColliderRectangle c = (Colliders.ColliderRectangle)owner.collider;

                    int xround = (int)(owner.transform.position.X / GameWorld.TileSize);
                    int yround = (int)(owner.transform.position.Y / GameWorld.TileSize);

                    for (int x = xround - 1; x <= xround + 1; x++)
                    {
                        for (int y = yround - 1; y <= yround + 1; y++)
                        {
                            batch.DrawHollowRectangle(new Rectangle(x * GameWorld.TileSize, y * GameWorld.TileSize, GameWorld.TileSize, GameWorld.TileSize), 1, Color.Blue);
                        }
                    }
                }

                if (owner.collider != null && owner.collider.ground != null)
                {
                    batch.DrawHollowRectangle(new RectangleF(owner.collider.ground.owner.transform.GetPositionInWorldSpace(true), new Vector2(GameWorld.TileSize)), 1, Color.Orange);
                }

                if (owner.parent != null)
                {
                    batch.DrawHollowRectangle(new RectangleF(owner.parent.transform.GetPositionInWorldSpace(true), new Vector2(GameWorld.TileSize)), 1, Color.Pink);
                }
            }

            if (owner.collider != null && owner.collider.dynamic && owner.collider.onGround)
            {
                List<Vector2> points = owner.collider.GetCollisionPoints(owner.collider.ground);

                foreach (Vector2 point in points)
                {
                    batch.DrawHollowCircle(point, 4, Color.Black, 4, 16);
                }
            }

            if (isnew)
                useTexture.Dispose();   //so no memory issues
        }

        public override void OnFlipped(Enums.DirectionMirror direction)
        {
            base.OnFlipped(direction);

            if (direction == Enums.DirectionMirror.Horizontal)
                texture.flip |= SpriteEffects.FlipHorizontally;
            if (direction == Enums.DirectionMirror.Vertical)
                texture.flip |= SpriteEffects.FlipVertically;
        }

        public override object Clone()
        {
            return new RendererPanel(owner, (TextureInfo)texture.Clone(), offset);
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();
        }
    }
}
