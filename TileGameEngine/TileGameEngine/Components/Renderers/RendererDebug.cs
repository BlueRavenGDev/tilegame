﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using BlueRavenUtility;
using Microsoft.Xna.Framework;

namespace TileGameEngine.Components.Renderers
{
    public class RendererDebug : Renderer
    {
        public RendererDebug() : base()
        {
            serializable = false;
        }

        public RendererDebug(GameObject owner) : base(owner, Vector2.Zero, 128)
        {
            serializable = false;
        }

        public override object Clone()
        {
            return new RendererDebug(owner);
        }

        public override void Render(SpriteBatch batch, BasicEffect basicEffect)
        {
            if (!GameMain.editing)  //if we're not in debug mode, destroy ourselves to prevent errors and stuff.
                dead = true;

            Vector2 pos = owner.transform.position * (owner.tile ? GameWorld.TileSize : 1);

            batch.DrawRectangle(new RectangleF(pos.X - 8, pos.Y - 8, 16, 16), Color.Pink);
        }
    }
}
