﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using BlueRavenUtility;
using System.IO;
using TileGameEngine.Components.Scripts;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Colliders
{
    public abstract class Collider : Component, ICloneable
    {
        public bool dynamic { get; protected set; }

        public bool hybrid { get { return !owner.tile && !dynamic; } set { } }

        public Vector2 velocity;

        public float gravity;
        public float groundFriction;

        public bool onGround { get; protected set; }

        public List<ResolutionHolder> resolutions;
        private List<ResolutionHolder> prevResolutions;

        public AngleQuadrant quadrant;

        public Collider ground { get; private set; }

        public bool noclip { get; set; }

        public Collider() : base()
        {
            resolutions = new List<ResolutionHolder>();
        }

        public Collider(GameObject owner, bool dynamic = false) : base(owner)
        {
            this.dynamic = dynamic;

            resolutions = new List<ResolutionHolder>();
            prevResolutions = new List<ResolutionHolder>();

            values = new Dictionary<string, EditableValue>();
        }

        public virtual void UpdatePosition()
        {
            if (dynamic || (!dynamic && !owner.tile))
            {
                if (noclip)
                    return;

                if (!onGround)
                    velocity.Y += gravity * GameWorld.timescale;
                owner.transform.SetPositionFromWorldSpace(velocity * GameWorld.timescale, true);
            }

            if (dynamic)
            {
            }
        }

        public virtual void PostAllResolve()
        {
            foreach (ResolutionHolder rh1 in resolutions.ToList())
            {
                foreach (ResolutionHolder rh2 in resolutions.ToList())
                {
                    if (rh1 != rh2 && rh1.onGround && rh2.onGround)
                    {
                        if (rh1.collider.owner.transform.position.Y < rh2.collider.owner.transform.position.Y)  //tiles that are higher up will always take priority
                            resolutions.Remove(rh2);
                        else resolutions.Remove(rh1);
                    }
                }
            }

            int i = 0;
            foreach (ResolutionHolder rh in resolutions)
            {
                ResolutionHolder found = prevResolutions.Find(x => x.collider.owner.index == rh.collider.owner.index);
                if (found == null)  //our first time touching this
                {
                    rh.collider.OnStartTouch(this);
                    OnStartTouch(rh.collider);
                }
                else
                {
                    rh.collider.OnTouch(this);
                    OnTouch(rh.collider);
                }

                i++;

                if (rh.resolve != Vector2.Zero)
                    owner.transform.SetPositionFromWorldSpace(rh.resolve, true);

                if (rh.onGround)
                {
                    OnGround(rh.collider);
                    SetGround(rh.collider, rh.angle);
                    //if (resolutions.Count == 1 || i == resolutions.Count)
                }
                else
                {
                    AngleQuadrant quad = AngleToQuadrant(rh.angle);
                    if (quad == AngleQuadrant.Bottom)
                        velocity.Y = 0;
                    else if ((quad == AngleQuadrant.Left || quad == AngleQuadrant.Right))
                        velocity.X = 0;
                }
            }

            foreach (ResolutionHolder prh in prevResolutions)
            {
                ResolutionHolder found = resolutions.Find(x => x.collider.owner.index == prh.collider.owner.index);

                if (found == null)
                {
                    prh.collider.OnEndTouch(this);
                    OnEndTouch(prh.collider);
                }
            }

            prevResolutions = resolutions.ToList();
            resolutions = new List<ResolutionHolder>();
        }

        private void SetGround(Collider ground, float angle, float dist = -1)
        {

            this.ground = ground;   //record the first instance of ground we're on.
            this.quadrant = ground.quadrant;
        }

        public virtual void NotOnGround()
        {
            onGround = false;

            ground = null;  //unset the current ground

            resolutions.ForEach(x => { x.collider.OnEndTouch(this); OnEndTouch(x.collider); });
        }

        public virtual void OnGround(Collider ground)
        {
            if (dynamic)
            {
                onGround = true;
            }
        }

        public void SetUpdateConstants(float gravity, float groundFriction)
        {
            this.gravity = gravity;
            this.groundFriction = groundFriction;
        }

        public virtual List<Vector2> GetCollisionPoints(Collider other)
        {
            return new List<Vector2>(); //empty
        }

        public abstract bool CheckColliding(Collider other, bool forceCollide);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resolve">The vector to resolve by. Note: relative to the collider calling this method.</param>
        public virtual void ResolveCollision(Vector2 resolve, float angle, Collider caller, bool onGround)
        {
            resolutions.Add(new ResolutionHolder(caller, angle, resolve, onGround));
        }

        public AngleQuadrant AngleToQuadrant(float angle)
        {
            return (AngleQuadrant)(EngineMathHelper.Mod((int)(Math.Round(angle / 90)), 4));
        }

        #region Touchers
        /// <summary>
        /// Run when a collider starts touching this collider.
        /// </summary>
        public virtual void OnStartTouch(Collider collider)
        {
            foreach (Script script in owner.scripts.ToList())
            {
                script.OnStartTouch(collider);
            }
        }

        /// <summary>
        /// Run while a collider is touching this collider. Must Have been touching for more than one frame.
        /// </summary>
        public virtual void OnTouch(Collider collider)
        {
            foreach (Script script in owner.scripts.ToList())
            {
                script.OnTouch(collider);
            }
        }

        /// <summary>
        /// Run when a collider stops touching this collider.
        /// </summary>
        public virtual void OnEndTouch(Collider collider)
        {
            foreach (Script script in owner.scripts.ToList())
            {
                script.OnEndTouch(collider);
            }
        }
        #endregion

        #region velocity stuff
        public void SetVelocity(float x, float y)
        {
            SetVelocity(new Vector2(x, y));
        }

        public void SetVelocity(Vector2 v)
        {
            this.velocity = v;
        }

        public void AddVelocity(float x, float y)
        {
            AddVelocity(new Vector2(x, y));
        }

        public void AddVelocity(Vector2 v)
        {
            this.velocity += v;
        }
        #endregion

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("gravity", new EditableValue(x => { gravity = (float)x; }, delegate() { return gravity; }));
            values.Add("quadrant", new EditableValue(x => { quadrant = (AngleQuadrant)x; }, delegate () { return (int)quadrant; }));
        }

        public override abstract object Clone();

        public override int GetByteSize()
        {
            return 1 +  //dynamic
                1 +     //onground
                4 + 4 + //velocity xy
                4 +     //gravity
                4 +     //groundFriction
                4;      //quadrant
        }

        public override void Serialize(BinaryWriter writer)
        {   //note: byte size is written in GameObject's serializing code, as it's easier to try/catch there.
            writer.Write(dynamic);
            writer.Write(onGround);
            writer.Write(velocity.X);
            writer.Write(velocity.Y);

            writer.Write(gravity);
            writer.Write(groundFriction);

            writer.Write((int)quadrant);
        }

        public override void Deserialize(BinaryReader reader)
        {
            dynamic = reader.ReadBoolean();
            onGround = reader.ReadBoolean();
            velocity.X = reader.ReadSingle();
            velocity.Y = reader.ReadSingle();

            gravity = reader.ReadSingle();
            groundFriction = reader.ReadSingle();

            quadrant = (AngleQuadrant)reader.ReadInt32();
        }

        public override void OnDeath()
        {
            owner.collider = null;
        }
    }

    public class ResolutionHolder
    {
        public readonly Collider collider;
        public readonly float angle;
        public readonly Vector2 resolve;
        public readonly bool onGround;

        /// <summary>
        /// Creates a new resolutionholder object.
        /// Class and not Struct due to the requirement for duplicate reference checking.
        /// </summary>
        /// <param name="collider">The collider this object recieved the resolution from.</param>
        /// <param name="resolve">The vector to resolve by.</param>
        /// <param name="onGround">Does this resolution put the collider on the ground?</param>
        public ResolutionHolder(Collider collider, float angle, Vector2 resolve, bool onGround)
        {
            this.collider = collider;
            this.angle = angle;
            this.resolve = resolve;
            this.onGround = onGround;
        }
    }
}
