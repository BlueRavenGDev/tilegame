﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Colliders
{
    public class ColliderTile : Collider
    {
        public static int pullHeight;   //the distance from which the ground will "pull" a dynamic object to it. Note: only applied on the TOPS of surfaces.
        public static int pushDepth;    //the deepest point to check

        public static bool checkPull = true;

        public static bool DEBUGdrawCollision;

        public enum TileCollideType
        {
            Top,
            Left,
            Bottom,
            Right,
            All
        }

        public TileCollideType tileCollideType;

        public ColliderTile() : base()
        {

        }

        public ColliderTile(GameObject owner, TileCollideType collideType) : base(owner)
        {
            this.tileCollideType = collideType;

            if (collideType.Has(TileCollideType.Top))
                quadrant = quadrant.Add(AngleQuadrant.Top);
            if (collideType.Has(TileCollideType.Right))
                quadrant = quadrant.Add(AngleQuadrant.Right);
            if (collideType.Has(TileCollideType.Bottom))
                quadrant = quadrant.Add(AngleQuadrant.Bottom);
            if (collideType.Has(TileCollideType.Left))
                quadrant = quadrant.Add(AngleQuadrant.Left);
            if (collideType.Has(TileCollideType.All))
                quadrant = quadrant.Add(AngleQuadrant.Bottom).Add(AngleQuadrant.Left).Add(AngleQuadrant.Right).Add(AngleQuadrant.Top);
        }

        public override bool CheckColliding(Collider other, bool force)
        {
            if (other.noclip)
                return false;

            bool found = false;

            List<Vector2> points = other.GetCollisionPoints(this);
            foreach (Vector2 point in points)
            {
                Vector2 pos = owner.transform.GetPositionInWorldSpace(true);
                Vector2 otherPos = point;
                Vector2 otherPosGround = points[0];

                if (tileCollideType.Has(TileCollideType.Top))
                {
                    float xleft = pos.X;
                    float xright = pos.X + GameWorld.TileSize;
                    if (otherPos.X >= xleft && otherPos.X < xright) //check to make sure we're in the x bounds of the tile.
                    {
                        bool canResolve = (other.velocity.Y >= 0 || other.onGround);    //if moving up and NOT on the ground, we can pass through tiles.
                        float depth = pos.Y - otherPos.Y;  //distance from the top of the tile

                        if (depth <= 0 && (depth > -pushDepth || depth >= pullHeight) && canResolve)   //between the top of the tile and top + pushdepth, between (relative) 0 & -32
                        {//also only resolve if the player is moving downwards, so we can have seamless jumping up out of the tile.
                            other.ResolveCollision(new Vector2(0, pos.Y - otherPosGround.Y), 0, this, true);
                            return true;
                        }
                        else if (depth > 0 && (depth < pullHeight) && other.onGround && canResolve)     //between the top of the tile and the top - pullHeight, between (relative) 0 & 8
                        {
                            other.ResolveCollision(new Vector2(0, pos.Y - otherPosGround.Y), 0, this, true);
                            found = true;
                        }
                    }
                }
                if (tileCollideType.Has(TileCollideType.Left) || tileCollideType == TileCollideType.All)
                {
                    float ytop = pos.Y;
                    float ybot = pos.Y + GameWorld.TileSize;

                    if (otherPos.Y >= ytop && otherPos.Y <= ybot)
                    {
                        float depth = pos.X - otherPos.X;

                        if (depth < 0 && depth > -pushDepth)
                        {
                            other.ResolveCollision(new Vector2(depth, 0), 90, this, false);
                            found = true;
                        }
                    }
                }
                if (tileCollideType.Has(TileCollideType.Right) || tileCollideType == TileCollideType.All)
                {
                    float ytop = pos.Y;
                    float ybot = pos.Y + GameWorld.TileSize;

                    if (otherPos.Y >= ytop && otherPos.Y <= ybot)
                    {
                        float depth = pos.X + GameWorld.TileSize - otherPos.X;

                        if (depth > 0 && depth < pushDepth)
                        {
                            other.ResolveCollision(new Vector2(depth, 0), 270, this, false);
                            found = true;
                        }
                    }
                }
                if (tileCollideType.Has(TileCollideType.Bottom) || tileCollideType == TileCollideType.All)
                {
                    float xleft = pos.X;
                    float xright = pos.X + GameWorld.TileSize;

                    float depth = (pos.Y + GameWorld.TileSize) - otherPos.Y;

                    if (otherPos.X > xleft && otherPos.X <= xright)
                    {
                        if (depth > 0 && (depth < pushDepth))
                        {
                            other.ResolveCollision(new Vector2(0, depth), 180, this, false);
                            found = true;
                        }
                    }
                }
            }

            if (found)
                return true;

            return false;
        }

        public void DrawDebug(SpriteBatch batch)
        {
            if (!DEBUGdrawCollision)
                return;
            Vector2 pos = owner.transform.GetPositionInWorldSpace(true);

            batch.DrawHollowRectangle(new RectangleF(pos - new Vector2(0, pullHeight), GameWorld.TileSize, pullHeight + pushDepth), 1, Color.Red);
        }

        public override void ResolveCollision(Vector2 resolve, float angle, Collider caller, bool onGround)
        {
            //not a dynamic object; will not ever be resolved
            //NOOP
        }

        public override void OnStartTouch(Collider collider)
        {
            base.OnStartTouch(collider);

            if (collider.dynamic)
            {
                collider.owner.AddParent(owner);
            }
        }

        public override void OnTouch(Collider collider)
        {
            base.OnTouch(collider);
        }

        public override void OnEndTouch(Collider collider)
        {
            base.OnEndTouch(collider);

            if (collider.dynamic)
            {
                collider.owner.RemoveParent(owner);
            }
        }

        public override object Clone()
        {
            ColliderTile ct = new ColliderTile(owner, tileCollideType);
            ct.tileCollideType = this.tileCollideType;
            return ct;
        }

        public override int GetByteSize()
        {
            return base.GetByteSize() +
                4;                      //tilecollidetype
        }
        public override void Serialize(BinaryWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)tileCollideType);
        }

        public override void Deserialize(BinaryReader reader)
        {
            base.Deserialize(reader);

            tileCollideType = (TileCollideType)reader.ReadInt32();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("collideType", new EditableValue(x => { tileCollideType = (TileCollideType)x; }, delegate () { return (int)tileCollideType; }));
        }
    }
}
