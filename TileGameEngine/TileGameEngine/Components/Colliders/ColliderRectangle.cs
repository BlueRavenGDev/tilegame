﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TileGameEngine.Components.Colliders
{
    public class ColliderRectangle : Collider
    {
        public float height { get; private set; }
        public float width { get; private set; }

        public ColliderRectangle() : base()
        {

        }

        public ColliderRectangle(GameObject owner, float width, float height) : base(owner, true)
        {
            this.height = height;
            this.width = width;
        }

        public override List<Vector2> GetCollisionPoints(Collider other)
        {
            List<Vector2> points = new List<Vector2>();

            if (other.quadrant.Has(AngleQuadrant.Right))
            {
                points.Add(owner.transform.GetPositionInWorldSpace(true) + (-owner.transform.worldUp * height) + owner.transform.worldLeft * width);   //bottomleft
                points.Add(owner.transform.GetPositionInWorldSpace(true) + owner.transform.worldUp * height + owner.transform.worldLeft * width);   //topleft
            }
            if (other.quadrant.Has(AngleQuadrant.Left))
            {
                points.Add(owner.transform.GetPositionInWorldSpace(true) + (-owner.transform.worldUp * height) + owner.transform.worldRight * width);  //bottomright
                points.Add(owner.transform.GetPositionInWorldSpace(true) + owner.transform.worldUp * height + owner.transform.worldRight * width);  //topright
            }
            if (other.quadrant.Has(AngleQuadrant.Top))
            {
                points.Add(owner.transform.GetPositionInWorldSpace(true) + (-owner.transform.worldUp * height) + owner.transform.worldRight * width);  //bottomright
                points.Add(owner.transform.GetPositionInWorldSpace(true) + (-owner.transform.worldUp * height) + owner.transform.worldLeft * width);   //bottomleft
            }
            if (other.quadrant.Has(AngleQuadrant.Bottom))
            {
                points.Add(owner.transform.GetPositionInWorldSpace(true) + owner.transform.worldUp * height + owner.transform.worldLeft * width);   //topleft
                points.Add(owner.transform.GetPositionInWorldSpace(true) + owner.transform.worldUp * height + owner.transform.worldRight * width);  //topright
            }

            return points;
        }

        public override bool CheckColliding(Collider other, bool xCheck)
        {
            return false;
        }
        

        public override object Clone()
        {
            return new ColliderRectangle(owner, height, width);
        }

        public override int GetByteSize()
        {
            return base.GetByteSize() + 4;  //radius
        }

        public override void Serialize(BinaryWriter writer)
        {
            base.Serialize(writer);

            writer.Write(height);
        }

        public override void Deserialize(BinaryReader reader)
        {
            base.Deserialize(reader);

            height = reader.ReadSingle();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();
        }
    }
}
