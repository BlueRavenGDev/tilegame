﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BlueRavenUtility;
using Microsoft.Xna.Framework.Graphics;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Colliders
{
    public class ColliderTileCheckNear : Collider
    {
        private int xcheck, ycheck;
        private bool atpoint;

        public ColliderTileCheckNear()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="atpoint">If set to false, this tile will add the xcheck and ycheck values to its current position in the tile array to find the tile to check.
        /// If set to true, will check at the xcheck and ycheck values specifically.</param>
        public ColliderTileCheckNear(GameObject owner, int xcheck, int ycheck, bool atpoint = false) : base(owner, false)
        {
            this.xcheck = xcheck;
            this.ycheck = ycheck;
            this.atpoint = atpoint;
        }

        public override bool CheckColliding(Collider other, bool forceCollide)
        {
            if (other.noclip)
                return false;

            Collider otherO = null;

            Vector2 pos = owner.transform.GetPositionInWorldSpace(true);

            List<Vector2> points = other.GetCollisionPoints(this);
            foreach (Vector2 point in points)
            {
                Vector2 otherPos = point;

                Vector2 dist = (otherPos - pos);
                if (forceCollide || (dist.X > 0 && dist.X <= GameWorld.TileSize && dist.Y > 0 && dist.Y <= GameWorld.TileSize))
                {
                    if (!atpoint)
                        otherO = owner.parentWorld.tiles[owner.transform.layer][(int)owner.transform.position.X + xcheck, (int)owner.transform.position.Y + ycheck].collider;
                    else otherO = owner.parentWorld.tiles[owner.transform.layer][xcheck, ycheck].collider;

                    if (otherO != null)
                        return otherO.CheckColliding(other, true);
                    else GameMain.console.Output.Append("ColliderTileCheckNear at " + owner.transform.position.ToString() + " pointing to invalid location: " + new Vector2(xcheck, ycheck).ToString());
                }
            }
            return false;
        }

        public override void OnRotated(Enums.DirectionClock direction)
        {
            base.OnRotated(direction);

            Vector2 fvec = new Vector2(xcheck, ycheck);

            if (atpoint)
                fvec = owner.transform.position - fvec;

            fvec = VectorHelper.GetPerp(fvec, direction == Enums.DirectionClock.CounterClockwise);

            if (atpoint)
                fvec = fvec + owner.transform.position;

            xcheck = (int)fvec.X;
            ycheck = (int)fvec.Y;
        }

        public override void OnFlipped(Enums.DirectionMirror direction)
        {
            base.OnFlipped(direction);

            Vector2 fvec = new Vector2(xcheck, ycheck);

            if (atpoint)
                fvec = owner.transform.position - fvec;

            if (direction == Enums.DirectionMirror.Horizontal)
                fvec.X = -fvec.X;
            else if (direction == Enums.DirectionMirror.Vertical)
                fvec.Y = -fvec.Y;

            if (atpoint)
                fvec = fvec + owner.transform.position;

            xcheck = (int)fvec.X;
            ycheck = (int)fvec.Y;
        }

        public void DrawDebug(SpriteBatch batch)
        {
            if (!ColliderTile.DEBUGdrawCollision)
                return;

            RectangleF rect = RectangleF.Empty;
            if (atpoint)
                rect = new RectangleF(xcheck * GameWorld.TileSize, ycheck * GameWorld.TileSize, GameWorld.TileSize, GameWorld.TileSize);
            else
            {
                Vector2 t = owner.transform.GetPositionInWorldSpace();
                rect = new RectangleF(t.X + xcheck * GameWorld.TileSize, t.Y + ycheck * GameWorld.TileSize, GameWorld.TileSize, GameWorld.TileSize);
            }

            batch.DrawHollowRectangle(rect.Expand(2, 2), 2, Color.Red);

            batch.DrawLine(owner.transform.position * GameWorld.TileSize + new Vector2(GameWorld.TileSize / 2), 
                (atpoint ? new Vector2(xcheck, ycheck) : owner.transform.position * GameWorld.TileSize + new Vector2(xcheck, ycheck) * GameWorld.TileSize) + new Vector2(GameWorld.TileSize / 2), 
                Color.Red, 4);
        }

        public override object Clone()
        {
            return new ColliderTileCheckNear(owner, xcheck, ycheck, atpoint);
        }

        public override int GetByteSize()
        {
            return base.GetByteSize() + 3;  //xcheck, ycheck, atpoint
        }

        public override void Serialize(BinaryWriter writer)
        {
            base.Serialize(writer);

            writer.Write(xcheck);
            writer.Write(ycheck);
            writer.Write(atpoint);
        }

        public override void Deserialize(BinaryReader reader)
        {
            base.Deserialize(reader);

            xcheck = reader.ReadInt32();
            ycheck = reader.ReadInt32();
            atpoint = reader.ReadBoolean();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("xcheck", new EditableValue(x => { xcheck = (int)x; }, delegate () { return xcheck; }));
            values.Add("ycheck", new EditableValue(x => { ycheck = (int)x; }, delegate () { return ycheck; }));
            values.Add("atpoint", new EditableValue(x => { atpoint = (bool)x; }, delegate () { return atpoint; }));
        }
    }
}
