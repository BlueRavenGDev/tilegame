﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using BlueRavenUtility;

namespace TileGameEngine.Components.Colliders
{
    public class ColliderHandler
    {
        private List<Collider> dynamics;
        private bool foundDynamics;

        public ColliderHandler()
        {
        }

        public void Update(GameWorld world)
        {
            if (!foundDynamics)
            {
                dynamics = world.colliders.FindAll(c => c.dynamic); //done this way because no duplicates
                foundDynamics = true;
            }

            dynamics.ForEach(d =>
            {
                if (d.owner.active)
                {
                    d.UpdatePosition();

                    int xround = (int)(d.owner.transform.position.X / GameWorld.TileSize);
                    int yround = (int)(d.owner.transform.position.Y / GameWorld.TileSize);

                    bool found = false;

                    for (int x = xround - 1; x <= xround + 1; x++)
                    {
                        for (int y = yround - 1; y <= yround + 1; y++)
                        {
                            if (!world.IsValidTile(x, y, d.owner.transform.layer))
                                continue;

                            Collider goC = world.tiles[d.owner.transform.layer][x, y].collider;

                            if (goC == null)
                                continue;
                            if (goC.CheckColliding(d, false))
                            {
                                //d.OnGround(world.tiles[x, y].collider);
                                found = true;
                            }
                        }
                    }

                    foreach (Collider go in world.hybridColliders)
                    {
                        go.UpdatePosition();

                        if (go.CheckColliding(d, false))
                            found = true;
                    }

                    if (!found)
                        d.NotOnGround();

                    //if (found)
                    {
                        d.PostAllResolve();
                    }
                }
            });

            world.colliders.CheckAndDelete(x => x.dead);
            world.hybridColliders.CheckAndDelete(x => x.dead);
        }

        public void AddCollider(GameWorld world, Collider collider, bool forceFindDynamics = true)
        {
            if (collider.hybrid)  //tile type is hybrid
            {
                world.hybridColliders.Add(collider);
            }
            world.colliders.Add(collider);

            if (forceFindDynamics)
                foundDynamics = false;
        }
    }
}
