﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Colliders
{
    public class ColliderNOOP : Collider
    {
        public ColliderNOOP() : base()
        {

        }

        public ColliderNOOP(GameObject owner) : base(owner)
        {

        }

        public override bool CheckColliding(Collider other, bool forceCollide)
        {
            RectangleF bounds = new RectangleF(owner.transform.GetPositionInWorldSpace(true) - new Vector2(GameWorld.TileSize / 2), new Vector2(GameWorld.TileSize));
            if (bounds.Contains(other.owner.transform.GetPositionInWorldSpace(true)))
            {
                other.ResolveCollision(Vector2.Zero, 0, this, false);
                return true;
            }
            return false;
        }

        public override object Clone()
        {
            return new ColliderNOOP(owner);
        }
    }
}
