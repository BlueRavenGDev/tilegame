﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using BlueRavenUtility;
using System.IO;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Colliders
{
    public enum AngleQuadrant
    {
        Top,
        Right,
        Bottom,
        Left
    }

    public class ColliderTileSlope : Collider
    {
        private float _angle;
        public float angle { get { return _angle; } set
            {
                _angle = EngineMathHelper.Mod(value, 360);
                quadrant = (AngleQuadrant)(EngineMathHelper.Mod((int)(Math.Round(_angle / 90)), 4));
            }
        }

        public float offset { get; private set; }

        public static bool DEBUGdrawSlopeCollision;

        //private AngleQuadrant quadrant;

        private float left, right, top, bot;

        public ColliderTileSlope() : base()
        {

        }

        public ColliderTileSlope(GameObject owner, float angle, float offset) : base(owner, false)
        {
            this.angle = angle;
            this.offset = offset;

            quadrant = (AngleQuadrant)(EngineMathHelper.Mod((int)(Math.Round(angle / 90)), 4));
        }

        public override bool CheckColliding(Collider other, bool forceCollide)
        {
            if (other.noclip)
                return false;

            Vector2 pos = owner.transform.GetPositionInWorldSpace(true);

            List<Vector2> points = other.GetCollisionPoints(this);

            foreach (Vector2 point in points)
            {
                Vector2 otherPos = point;

                if (forceCollide || CheckInsideBounds(otherPos))   //in bounds
                {
                    Vector2 center = pos + new Vector2(GameWorld.TileSize / 2);    //since pos is the topleft, we want the center

                    Vector2 angleVec = VectorHelper.GetAngleNormVector(angle);

                    float depth = 0;

                    Vector2 resolveVec = Vector2.Zero;
                    bool canResolve = false;

                    if (quadrant == AngleQuadrant.Top)
                    {
                        canResolve = (other.velocity.Y >= 1 || other.onGround);    //if moving up and NOT on the ground, we can pass through tiles.

                        float height = (angleVec.Y * (otherPos.X - center.X));
                        depth = (otherPos.Y - center.Y) + height + offset;

                        resolveVec = new Vector2(0, -depth);
                    }
                    else if (quadrant == AngleQuadrant.Right)
                    {
                        canResolve = (other.velocity.X <= 1 || other.onGround);

                        float height = (angleVec.X * (otherPos.Y - center.Y));
                        depth = (otherPos.X - center.X) + height + offset;

                        resolveVec = new Vector2(-depth, 0);
                    }
                    else if (quadrant == AngleQuadrant.Bottom)
                    {
                        canResolve = (other.velocity.Y <= 1 || other.onGround);    //if moving up and NOT on the ground, we can pass through tiles.

                        float height = (angleVec.Y * (otherPos.X - center.X));
                        depth = (otherPos.Y - center.Y) - height - offset;

                        resolveVec = new Vector2(0, -depth);
                    }
                    else if (quadrant == AngleQuadrant.Left)
                    {
                        canResolve = (other.velocity.X >= 1 || other.onGround);

                        float height = (angleVec.X * (otherPos.Y - center.Y));
                        depth = (otherPos.X - center.X) - height - offset;

                        resolveVec = new Vector2(-depth, 0);
                    }

                    bool asground = false;

                    Collider otherGround = other.ground;
                    if (otherGround != null && otherGround is ColliderTileSlope)
                    {
                        ColliderTileSlope cs = (ColliderTileSlope)otherGround;
                        float otherGroundAngle = cs.angle;

                        bool inquad = cs.quadrant == quadrant || ((quadrant == AngleQuadrant.Left || quadrant == AngleQuadrant.Right) && Math.Abs(angle) < 90 && Math.Abs(angle) > 180);
                        if (Math.Abs(Math.Abs(angle) - Math.Abs(otherGroundAngle)) < 22.5f || inquad)
                            asground = true;    //if we're in the same quadrant as the current ground, this can be new ground. otherwise, we have to be within 15deg.
                    }
                    else
                    {
                        bool inquad = quadrant == AngleQuadrant.Top || 
                            ((quadrant == AngleQuadrant.Left || quadrant == AngleQuadrant.Right) && Math.Abs(angle) < 90 || Math.Abs(angle) > 180);
                        if (inquad)
                            asground = true;
                    }

                    bool canPush = (depth > 0 && depth <= ColliderTile.pushDepth);
                    if (quadrant == AngleQuadrant.Bottom || quadrant == AngleQuadrant.Right)
                        canPush = (depth < 0 && depth >= -ColliderTile.pushDepth);
                    bool canPull = (asground && depth <= 0 && depth > -ColliderTile.pullHeight);
                    if (quadrant == AngleQuadrant.Bottom || quadrant == AngleQuadrant.Right)
                        canPull = (asground && depth >= 0 && depth < ColliderTile.pullHeight);
                    if ((canPush || canPull) && canResolve || forceCollide)
                    {
                        other.ResolveCollision(resolveVec, angle, this, asground);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool CheckInsideBounds(Vector2 otherPos)
        {
            Vector2 pos = owner.transform.position * GameWorld.TileSize;

            left = 0;
            right = 0;
            top = 0;
            bot = 0;

            if (quadrant == AngleQuadrant.Top)
            {
                left = pos.X;
                right = pos.X + GameWorld.TileSize;
                top = pos.Y - ColliderTile.pullHeight;
                bot = pos.Y + GameWorld.TileSize + ColliderTile.pushDepth;
            }
            else if (quadrant == AngleQuadrant.Right)
            {
                left = pos.X - ColliderTile.pushDepth;
                right = pos.X + GameWorld.TileSize + ColliderTile.pullHeight;
                top = pos.Y;
                bot = pos.Y + GameWorld.TileSize;
            }
            else if (quadrant == AngleQuadrant.Bottom)
            {
                left = pos.X;
                right = pos.X + GameWorld.TileSize;
                top = pos.Y - ColliderTile.pushDepth;
                bot = pos.Y + GameWorld.TileSize + ColliderTile.pullHeight;
            }
            else if (quadrant == AngleQuadrant.Left)
            {
                left = pos.X - ColliderTile.pushDepth;
                right = pos.X + GameWorld.TileSize + ColliderTile.pushDepth;
                top = pos.Y;
                bot = pos.Y + GameWorld.TileSize;
            }

            bool t = otherPos.X >= left && otherPos.X <= right && otherPos.Y >= top && otherPos.Y <= bot;

            if (!t)
            {
                left = 0;
                right = 0;
                top = 0;
                bot = 0;
            }
            return t;
        }

        public override void OnRotated(Enums.DirectionClock direction)
        {
            base.OnRotated(direction);

            if (direction == Enums.DirectionClock.Clockwise)
                angle = EngineMathHelper.Mod((angle + 90), 360);
            if (direction == Enums.DirectionClock.CounterClockwise)
                angle = EngineMathHelper.Mod((angle - 90), 360);
            offset = -offset;
        }

        public override void OnFlipped(Enums.DirectionMirror direction)
        {
            base.OnFlipped(direction);

            if (direction == Enums.DirectionMirror.Horizontal)
            {
                angle = -angle;
            }
            else if (direction == Enums.DirectionMirror.Vertical)
            {
                angle = 180 - angle;
            }
        }

        public void DrawDebug(SpriteBatch batch)
        {
            if (!DEBUGdrawSlopeCollision)
                return;
            Vector2 pos = owner.transform.position * GameWorld.TileSize;
            Vector2 center = pos + new Vector2(GameWorld.TileSize / 2);
            Vector2 angleVec = VectorHelper.GetAngleNormVector(angle);
            Vector2 perpVec = VectorHelper.GetPerp(angleVec);
            pos += new Vector2(GameWorld.TileSize / 2);

            Vector2 offsetVec = Vector2.Zero;
            if (quadrant == AngleQuadrant.Top)
                offsetVec = new Vector2(0, -offset);
            else if (quadrant == AngleQuadrant.Right)
                offsetVec = new Vector2(-offset, 0);
            else if (quadrant == AngleQuadrant.Bottom)
                offsetVec = new Vector2(0, offset);
            else if (quadrant == AngleQuadrant.Left)
                offsetVec = new Vector2(offset, 0);

            batch.DrawLine(pos + offsetVec, pos + (perpVec * 64) + offsetVec, Color.Black);

            batch.DrawLine(center - angleVec * 16 + offsetVec, center + angleVec * 16 + offsetVec, Color.Black);

            Vector2 t = new Vector2(right, bot) - new Vector2(left, top);
            RectangleF rect = new RectangleF(left, top, t);

            //batch.DrawRectangle(rect, Color.Blue);
        }

        public override void ResolveCollision(Vector2 resolve, float angle, Collider caller, bool onGround)
        {
        }

        public override object Clone()
        {
            return new ColliderTileSlope(owner, angle, offset);
        }

        public override int GetByteSize()
        {
            return base.GetByteSize() + 4 + 4;      //angle 4, offset 4
        }

        public override void Serialize(BinaryWriter writer)
        {
            base.Serialize(writer);

            writer.Write(angle);
            writer.Write(offset);
        }

        public override void Deserialize(BinaryReader reader)
        {
            base.Deserialize(reader);

            angle = reader.ReadSingle();
            offset = reader.ReadSingle();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("angle", new EditableValue(x => { angle = (float)x % 360; }, delegate () 
            {
                return angle;
            }));
            values.Add("offset", new EditableValue(x => { offset = (float)x; }, delegate () { return offset; }));
        }
    }
}
