﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components
{
    public struct Preset
    {
        public string key;
        public Func<GameObject> value;
        public bool paintable;

        public Preset(string key, Func<GameObject> value, bool paintable = false)
        {
            this.key = key;
            this.value = value;
            this.paintable = paintable;
        }
    }
}
