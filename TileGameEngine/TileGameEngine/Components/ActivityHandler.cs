﻿using BlueRavenUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components
{
    public class ActivityHandler
    {
        public bool anyChange;

        public void CheckActive(GameWorld world)
        {
            if (anyChange)
            {
                anyChange = false;

                world.gameObjects.ForEach(x =>
                {
                    if (!x.isUsed)
                        x.Destroy(true);
                    else
                    {
                        if (x.forceActive || (x.transform.GetPositionInWorldSpace() - Camera.camera.worldCenter).Length() < 1024 || (x.parent != null && x.parent.active))
                        {   //near enough to the camera or the parent is actuve
                            x.active = true;
                        }
                        else x.active = false;
                    }
                });
            }
        }
    }
}
