﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptAutoScroller : Script
    {
        private Vector2 start, end, scrollrate;
        private Vector2 current;

        private bool relative;

        public ScriptAutoScroller(GameObject owner, Vector2 start, Vector2 end, Vector2 scrollrate, bool relative = true) : base(owner)
        {
            this.start = start;
            this.end = end;
            this.scrollrate = scrollrate;

            this.relative = relative;
        }

        public override object Clone()
        {
            return new ScriptAutoScroller(owner, start, end, scrollrate, relative);
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            if (!relative)
            {
                current = start + scrollrate * active;

                current = Vector2.Clamp(current, start, end);
            }
            else
            {
                current = start + owner.transform.position + scrollrate * active;

                current = Vector2.Clamp(current, start + owner.transform.position, end + owner.transform.position);
            }

            Camera.camera.target = current;
        }

        public override int GetByteSize()
        {
            return 4 + 4 +  //start xy
                4 + 4 +     //end xy
                4 + 4 +     //scrollrate xy
                1;          //relative
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(start.X);
            writer.Write(start.Y);

            writer.Write(end.X);
            writer.Write(end.Y);

            writer.Write(scrollrate.X);
            writer.Write(scrollrate.Y);

            writer.Write(relative);
        }

        public override void Deserialize(BinaryReader reader)
        {
            start.X = reader.ReadSingle();
            start.Y = reader.ReadSingle();

            end.X = reader.ReadSingle();
            end.Y = reader.ReadSingle();

            scrollrate.X = reader.ReadSingle();
            scrollrate.Y = reader.ReadSingle();

            relative = reader.ReadBoolean();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("start", new EditableValue(x => start = (Vector2)x, delegate () { return start; }));
            values.Add("end", new EditableValue(x => end = (Vector2)x, delegate () { return end; }));
            values.Add("scrollrate", new EditableValue(x => scrollrate = (Vector2)x, delegate () { return scrollrate; }));

            values.Add("relative", new EditableValue(x => relative = (bool)x, delegate () { return relative; }));
        }
    }
}
