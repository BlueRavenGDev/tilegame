﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Components.Colliders;
using Microsoft.Xna.Framework.Graphics;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptWorldWrap : Script
    {
        private Rectangle bounds;

        public ScriptWorldWrap() : base()
        {

        }

        public ScriptWorldWrap(GameObject owner, Rectangle bounds) : base(owner)
        {
            this.bounds = bounds;
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            foreach (Collider collider in world.colliders)
            {
                if (collider.dynamic)
                {
                    Vector2 pos = collider.owner.transform.GetPositionInWorldSpace(true);
                    Vector2 dist = pos - bounds.Location.ToVector2();//owner.transform.GetPositionInWorldSpace(true);

                    CheckWrap(collider, ref dist);

                    //dist.X = EngineMathHelper.Mod(dist.X, bounds.Width);     //wrap the position around the bounds of the rectangle
                    //dist.Y = EngineMathHelper.Mod(dist.Y, bounds.Height);

                    Vector2 fpos = dist + bounds.Location.ToVector2();
                    collider.owner.transform.SetPositionFromWorldSpace(fpos);
                }
            }
        }

        public override void OnTransformChanged(Vector2 oldPos, Vector2 oldScale, float oldRotation, int oldLayer)
        {
            base.OnTransformChanged(oldPos, oldScale, oldRotation, oldLayer);

            bounds.X = (int)owner.transform.GetPositionInWorldSpace(true).X;
            bounds.Y = (int)owner.transform.GetPositionInWorldSpace(true).Y;
        }

        public virtual void CheckWrap(Collider collider, ref Vector2 dist)
        {
            if (dist.X <= 0)
            {
                //dist.X += bounds.Width;
                Wrap(collider, Enums.DirectionMirror.Horizontal, new Vector2(bounds.Width, 0), ref dist);
            }
            else if (dist.X >= bounds.Width)
            {
                //dist.X -= bounds.Width;
                Wrap(collider, Enums.DirectionMirror.Horizontal, new Vector2(-bounds.Width, 0), ref dist);
            }
            if (dist.Y <= 0)
            {
                //dist.Y += bounds.Height;
                Wrap(collider, Enums.DirectionMirror.Vertical, new Vector2(0, bounds.Height), ref dist);
            }
            else if (dist.Y >= bounds.Height)
            {
                dist.Y -= bounds.Height;
                Wrap(collider, Enums.DirectionMirror.Vertical, new Vector2(0, -bounds.Height), ref dist);
            }
        }

        public virtual void Wrap(Collider collider, Enums.DirectionMirror direction, Vector2 distance, ref Vector2 dist)
        {
            Vector2 distCam = Camera.camera.target - collider.owner.transform.GetPositionInWorldSpace(true);
            Camera.camera.data[0] = distCam.X;
            Camera.camera.data[1] = distCam.Y;

            dist += distance;
            Camera.camera.SetMoveMode(CameraMoveMode.Snap, 1);
        }

        public override void OnDrawDebug(SpriteBatch batch)
        {
            base.OnDrawDebug(batch);

            if (GameMain.editing)
                batch.DrawHollowRectangle(bounds, 8, Color.Pink);
        }

        public override object Clone()
        {
            return new ScriptWorldWrap(owner, bounds);
        }

        public override int GetByteSize()
        {
            return 4 + 4 + 4 + 4;   //bounds xy wh
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(bounds.X);
            writer.Write(bounds.Y);
            writer.Write(bounds.Width);
            writer.Write(bounds.Height);   
        }

        public override void Deserialize(BinaryReader reader)
        {
            bounds = new Rectangle(
                reader.ReadInt32(), 
                reader.ReadInt32(), 
                reader.ReadInt32(),
                reader.ReadInt32());
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("bounds", new EditableValue(x => bounds = (Rectangle)x, delegate () { return bounds; }));
        }
    }
}
