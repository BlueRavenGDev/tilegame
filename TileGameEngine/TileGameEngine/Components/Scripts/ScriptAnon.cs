﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Components.Colliders;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptAnon : Script
    {
        public ScriptAnon() : base()
        {

        }

        public ScriptAnon(GameObject owner) :base(owner)
        {

        }

        public override object Clone()
        {
            return new ScriptAnon(owner);
        }

        public override void Serialize(BinaryWriter writer)
        {
        }

        public override void Deserialize(BinaryReader reader)
        {
        }

        public override void Update(GameWorld world)
        {
            if (owner.collider is ColliderTileSlope)
            {
                ColliderTileSlope cts = (ColliderTileSlope)owner.collider;

                cts.angle++;
            }
        }

        public override void AddEditorValues()
        {
            throw new NotImplementedException();
        }

        public override int GetByteSize()
        {
            throw new NotImplementedException();
        }
    }
}
