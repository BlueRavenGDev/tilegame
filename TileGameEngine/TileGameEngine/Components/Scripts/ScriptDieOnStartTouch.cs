﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Components.Colliders;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptDieOnStartTouch : Script
    {
        public ScriptDieOnStartTouch() : base()
        {

        }

        public ScriptDieOnStartTouch(GameObject owner) : base(owner)
        {

        }

        public override void OnStartTouch(Collider collider)
        {
            base.OnStartTouch(collider);

            owner.Destroy();
        }

        public override object Clone()
        {
            return new ScriptDieOnStartTouch(owner);
        }

        public override int GetByteSize()
        {
            return 0;
        }

        public override void Serialize(BinaryWriter writer)
        {
            
        }

        public override void Deserialize(BinaryReader reader)
        {
            
        }
    }
}
