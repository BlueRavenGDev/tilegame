﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptMoveSine : Script
    {
        private Enums.DirectionMirror direction;
        private float halfCycleTime, distance;
        private bool left;

        /// <summary>
        /// Creates a new Script telling the owner to move in a sine wave.
        /// </summary>
        /// <param name="direction">The axis to move in; horizontal or vertical. Can be both, to result in a diagonal movement.</param>
        /// <param name="halfCycleTime">How long it takes for the cycle to complete half of a cycle.</param>
        /// <param name="distance">How far the object moves to either side.</param>
        /// <param name="left">The starting direction to move.</param>
        public ScriptMoveSine(GameObject owner, Enums.DirectionMirror direction, float halfCycleTime, float distance, bool left = true) : base(owner)
        {
            this.direction = direction;
            this.halfCycleTime = halfCycleTime;
            this.distance = distance;
            this.left = left;
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            Vector2 pos = owner.transform.GetPositionInWorldSpace();
            float percent = (world.time % halfCycleTime) / halfCycleTime;
            if (left)
                percent = Math.Abs(percent - 1);    //flip the percent
            float offset = ((float)Math.Sin(Math.PI * (percent * 2)) * distance);

            if (direction.Has(Enums.DirectionMirror.Horizontal))
                pos.X = owner.transform.startPosition.X + offset;
            if (direction.Has(Enums.DirectionMirror.Vertical))
                pos.Y = owner.transform.startPosition.Y + offset;

            owner.transform.SetPositionFromWorldSpace(pos);
        }

        public override void Reset()
        {
            owner.transform.SetPositionFromWorldSpace(owner.transform.startPosition);
            base.Reset();
        }

        public override object Clone()
        {
            return new ScriptMoveSine(owner, direction, halfCycleTime, distance);
        }

        public override int GetByteSize()
        {
            return 4 + 4 + 4;   //direction, halfCycleTime, distance
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write((int)direction);
            writer.Write(halfCycleTime);
            writer.Write(distance);
        }

        public override void Deserialize(BinaryReader reader)
        {
            direction = (Enums.DirectionMirror)reader.ReadInt32();
            halfCycleTime = reader.ReadSingle();
            distance = reader.ReadSingle();
        }
    }
}
