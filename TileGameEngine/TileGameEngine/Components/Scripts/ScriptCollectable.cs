﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public abstract class ScriptCollectable : Script
    {
        private string collectortag;
        
        public float radius { get; private set; }

        public Vector2 offset;

        /// <param name="collectortag">Objects tagged with the given tag will be able to pick this object up.</param>
        public ScriptCollectable(GameObject owner, float radius, string collectortag) : base(owner)
        {
            this.radius = radius;
            this.collectortag = collectortag;
        }

        public override void Update(GameWorld world)
        {
            foreach (GameObject go in world.gameObjects.ToList())
            {
                if (go.tags.Contains(collectortag))
                {
                    if (IsInRange(go) && CanPickup(go))
                    {
                        OnPickup(go);
                        owner.Destroy();
                    }
                }
            }
        }

        public virtual bool IsInRange(GameObject collector)
        {
            return (collector.transform.position - (owner.transform.position + offset)).Length() < radius;
        }

        public abstract bool CanPickup(GameObject collector);

        public abstract void OnPickup(GameObject collector);

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(collectortag);
            writer.Write(radius);
        }

        public override void Deserialize(BinaryReader reader)
        {
            collectortag = reader.ReadString();
            radius = reader.ReadSingle();
        }
    }
}
