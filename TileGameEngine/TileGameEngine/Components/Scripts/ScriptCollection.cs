﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptCollection : ICollection
    {
        private List<Script> scripts;

        public ScriptCollection(GameObject owner, List<Script> scripts)
        {
            this.scripts = scripts;
        }

        public void Add(Script script)
        {
            scripts.Add(script);
        }

        public int Count
        {
            get
            {
                return scripts.Count;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void CopyTo(Array array, int index)
        {
            foreach (Script script in scripts)
            {
                array.SetValue(script, index);
                index++;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(scripts);
        }

        public class Enumerator : IEnumerator
        {
            private List<Script> scripts;
            private int Cursor;

            public Enumerator(List<Script> scripts)
            {
                this.scripts = scripts;
                Cursor = -1;
            }

            public object Current
            {
                get
                {
                    if (Cursor < 0 || (Cursor == scripts.Count))
                        throw new InvalidOperationException();
                    return scripts[Cursor];
                }
            }

            public bool MoveNext()
            {
                if (Cursor < scripts.Count)
                    Cursor++;

                return (!(Cursor == scripts.Count));
            }

            public void Reset()
            {
                Cursor = -1;
            }
        }
    }
}
