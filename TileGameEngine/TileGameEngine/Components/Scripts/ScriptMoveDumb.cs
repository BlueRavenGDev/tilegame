﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptMoveDumb : Script
    {
        private float speed, top;
        public bool left;

        public ScriptMoveDumb(GameObject owner, float speed, float top, bool left = true) : base(owner)
        {
            this.speed = speed;
            this.top = top;
            this.left = left;
        }

        public override object Clone()
        {
            return new ScriptMoveDumb(owner, speed, top, left);
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            if (owner.collider != null)
            {
                if (left)
                    owner.collider.velocity.X -= 2;
                else owner.collider.velocity.X += 2;

                owner.collider.velocity.X = MathHelper.Clamp(owner.collider.velocity.X, -top, top);
            }
        }

        public override int GetByteSize()
        {
            return 4 + 4 +  //speed, top
                1;          //left
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(speed);
            writer.Write(top);
            writer.Write(left);
        }

        public override void Deserialize(BinaryReader reader)
        {
            speed = reader.ReadSingle();
            top = reader.ReadSingle();
            left = reader.ReadBoolean();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("speed", new EditableValue(x => speed = (float)x, delegate () { return speed; }));
            values.Add("maxSpeed", new EditableValue(x => top = (float)x, delegate () { return top; }));
            values.Add("left", new EditableValue(x => left = (bool)x, delegate () { return left; }));
        }
    }
}
