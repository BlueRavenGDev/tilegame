﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptDamagable : Script
    {
        public int health;
        public float radius;
        public bool canDealDamage, canTakeDamage;
        public int damage;
        public string damagedByTag;

        private float _invulnTime;
        public float invulnTime { get { return _invulnTime; } set { _invulnTime = value * GameWorld.timescale; invulnerable = _invulnTime > 0; } }
        protected int invulnTimeMax = 6;
        public bool invulnerable { get; private set; }

        protected bool damagedFrame;

        private ScriptDamagable prevDamageDealer,    //the previous gameobject to deal damage to this script
                                prevDamageDealtTo;   //the previous gameobject this script has dealt damage to

        public ScriptDamagable(GameObject owner, float radius, int health, int damage, string damagedByTag) : base(owner)
        {
            this.radius = radius;
            this.health = health;
            this.damage = damage;
            this.damagedByTag = damagedByTag;
            canDealDamage = true;
            canTakeDamage = true;
        }

        public override void Update(GameWorld world)
        {
            invulnTime--;

            damagedFrame = false;

            foreach (GameObject enemy in world.gameObjects.ToList())
            {
                if (enemy.tags.Contains(damagedByTag))
                {
                    ScriptDamagable enemyDamageScript;
                    enemyDamageScript = enemy.GetComponent<ScriptDamagable>();
                    if (enemyDamageScript != null)
                    {
                        if (IsNear(enemyDamageScript))
                        {
                            if (CanTakeDamage(enemyDamageScript))
                            {
                                TakeDamage(enemyDamageScript, enemyDamageScript.damage);
                                enemyDamageScript.OnDealDamage(this);
                            }
                        }
                    }
                }
            }

            if (health <= 0)
            {
                if (CanDie())
                    OnKilled();
            }
        }

        private bool IsNear(ScriptDamagable other)
        {
            Vector2 pos = owner.transform.GetPositionInWorldSpace(true);
            if (owner.tile)
            {
                pos += new Vector2(GameWorld.TileSize / 2); //tiles are by default definined by their top left point, so translate to their center instead
            }
            Vector2 posOther = other.owner.transform.GetPositionInWorldSpace(true);
            if (other.owner.tile)
            {
                posOther *= GameWorld.TileSize;
                posOther += new Vector2(GameWorld.TileSize / 2);
            }

            Vector2 dist = pos - posOther;
            float distance = dist.Length();
            float radSum = radius + other.radius;
            return distance < radSum;
        }

        public virtual bool CanTakeDamage(ScriptDamagable other)
        {
            return other.canDealDamage && canTakeDamage && invulnTime <= 0;
        }

        protected virtual void TakeDamage(ScriptDamagable other, int damage)
        {
            health -= damage;
            invulnTime = invulnTimeMax;

            damagedFrame = true;

            prevDamageDealer = other;
        }

        public void TakeDamage(int damage)
        {
            health -= damage;
            invulnTime = invulnTimeMax;

            if (health <= 0)
            {
                if (CanDie())
                    OnDeath();
            }
        }

        public virtual bool CanDie()
        {
            return true;
        }

        public virtual void OnKilled()
        {
            prevDamageDealer.OnKill(this);
            owner.Destroy();
        }

        public virtual void OnDealDamage(ScriptDamagable other)
        {
            prevDamageDealtTo = other;
        }

        public virtual void OnKill(ScriptDamagable other)
        {

        }

        public override int GetByteSize()
        {
            return 4 +                              //health
                damagedByTag.Length * sizeof(char); //damagedByTag
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(health);
            writer.Write(damagedByTag);
        }

        public override void Deserialize(BinaryReader reader)
        {
            health = reader.ReadInt32();
            damagedByTag = reader.ReadString();
        }

        public override object Clone()
        {
            return new ScriptDamagable(owner, radius, health, damage, damagedByTag);
        }
    }
}
