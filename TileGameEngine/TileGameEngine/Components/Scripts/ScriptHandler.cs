﻿using BlueRavenUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptHandler
    {
        public void Update(GameWorld world)
        {
            foreach (List<Script> scripts in world.updateScripts.ToList())
            {
                foreach (Script script in scripts.ToList())
                {
                    if (script.owner.active)
                        script.Update(world);
                }

                scripts.CheckAndDelete(new Func<Script, bool>(x => { return x.dead; }));  //if any script is dead, remove it from the list.
            }

            world.updateScripts.CheckAndDelete(new Func<List<Script>, bool>(x => { return x.Count == 0; }));  //if any list of scripts is empty, delete it.
        }
    }
}
