﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine.Editor;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptEntitySpawner : Script
    {
        private string presetSpawn;
        private bool spawned;

        private bool once;
        private int timeBetweenSpawns;
        private readonly int timeBetweenSpawnsMax;
        private int maxSpawns;
        private readonly int maxSpawnsMax;

        public TextureInfo spawnTexture;

        public ScriptEntitySpawner()
        {
            once = true;
            maxSpawns = -1;
        }
        /// <summary>
        /// Creates a new ScriptEntitySpawner - a script that, when run, spawns a preset of the given name.
        /// </summary>
        /// <param name="presetSpawn">The name of the preset to spawn in. See <see cref="GameMain.RegisterPresets"/> for all the presets.</param>
        /// <param name="once">Should this entity spawn only once?</param>
        /// <param name="timeBetweenSpawns">The amount of time this entity should take between spawning. Defaults to 10 seconds (600 frames)</param>
        /// <param name="maxSpawns">The maximum amount of spawns this entity should spawn before stopping.</param>
        public ScriptEntitySpawner(GameObject owner, string presetSpawn, bool once = true, int timeBetweenSpawns = 600, int maxSpawns = -1) : base(owner)
        {
            this.presetSpawn = presetSpawn;

            this.once = once;

            this.timeBetweenSpawns = timeBetweenSpawns;
            timeBetweenSpawnsMax = timeBetweenSpawns;

            this.maxSpawns = maxSpawns;
            maxSpawnsMax = maxSpawns;

            owner.forceActive = true;
        }

        public override void Update(GameWorld world)
        {
            if (!once && timeBetweenSpawns-- <= 0)
            {
                spawned = false;
                timeBetweenSpawns = timeBetweenSpawnsMax;
            }

            if (!spawned)
            {
                spawned = true;
                Spawn(world);
            }
        }

        public void Spawn(GameWorld world)
        {
            if (maxSpawns > 0 || maxSpawns == -1)
            {
                GameObject preset = null;
                if (spawnTexture == null)
                    preset = GameMain.CloneFromPreset(presetSpawn, owner.transform.position, true, owner.tile);
                else
                    preset = GameMain.CloneFromPreset(presetSpawn, owner.transform.position, spawnTexture, owner.tile);

                preset.selectable = false;
                preset.serializable = false;

                world.AddGameObject(preset, preset.tile);
                owner.children.Add(preset);

                if (maxSpawns != -1)
                    maxSpawns--;    //don't want to count down if maxSpawns is -1, as it's important we don't change it
                
            }
        }

        public override void Reset()
        {
            spawned = false;
            owner.children.ForEach(x => x.Destroy(true));
        }

        public override object Clone()
        {
            return new ScriptEntitySpawner(owner, presetSpawn);
        }

        public override int GetByteSize()
        {
            return presetSpawn.Length * sizeof(char);
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(presetSpawn);
        }

        public override void Deserialize(BinaryReader reader)
        {
            presetSpawn = reader.ReadString();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();

            values.Add("presetname", new EditableValue(x => presetSpawn = (string)x, delegate () { return presetSpawn; }));
        }
    }
}
