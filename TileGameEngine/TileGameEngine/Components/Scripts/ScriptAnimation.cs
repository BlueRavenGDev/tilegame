﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptAnimation : Script
    {
        public bool withAltTextures;

        public ScriptAnimation() : base()
        {

        }

        public ScriptAnimation(GameObject owner, bool withAltTextures = true) : base(owner)
        {
            this.withAltTextures = withAltTextures;
        }

        public override void Update(GameWorld world)
        {
            owner.renderer?.texture.UpdateFrame();

            if (withAltTextures)
            {
                foreach (TextureInfo ti in owner.renderer?.altTextures)
                {
                    if (ti != null)
                        ti.UpdateFrame();
                }
            }
        }

        public override object Clone()
        {
            return new ScriptAnimation(owner, withAltTextures);
        }

        public override int GetByteSize()
        {
            return 1;       //withAltTextures
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(withAltTextures);
        }

        public override void Deserialize(BinaryReader reader)
        {
            withAltTextures = reader.ReadBoolean();
        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();
        }
    }
}
