﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TileGameEngine.Components;
using TileGameEngine.Components.Colliders;

namespace TileGameEngine.Components.Scripts
{
    public abstract class Script : Component
    {
        public float active { get; private set; }
        public bool isActive;

        public Script() : base()
        {
            isActive = true;
        }

        public Script(GameObject owner) : base(owner)
        {
            isActive = true;
        }

        public virtual void Update(GameWorld world)
        {
            if (isActive)
                active += 1 * GameWorld.timescale;
        }

        public virtual void OnTransformChanged(Vector2 oldPos, Vector2 oldScale, float oldRotation, int oldLayer)
        {

        }

        public virtual void OnStartTouch(Collider collider)
        {

        }

        public virtual void OnTouch(Collider collider)
        {

        }

        public virtual void OnEndTouch(Collider collider)
        {

        }

        public virtual void OnDraw(SpriteBatch batch)
        {

        }

        public virtual void OnDrawDebug(SpriteBatch batch)
        {

        }

        public virtual void Reset()
        {
            active = 0;
        }

        public override void OnDeath()
        {
            owner.scripts.Remove(this);
        }

        public abstract override int GetByteSize(); //since we have nothing to save, might as well make it abstract.

        public abstract override object Clone();
    }
}
