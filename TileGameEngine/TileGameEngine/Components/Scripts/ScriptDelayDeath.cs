﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptDelayDeath : Script
    {
        private int timerMax;
        private int timer;

        public ScriptDelayDeath(GameObject owner, int timer) : base(owner)
        {
            this.timerMax = timer;
            this.timer = timer;
        }

        public override void Update(GameWorld world)
        {
            timer--;

            if (timer <= 0)
            {
                owner.Destroy();
            }
        }

        public override object Clone()
        {
            return new ScriptDelayDeath(owner, timerMax);
        }

        public override int GetByteSize()
        {
            return 4 + 4;   //timer & timerMax
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(timerMax);
            writer.Write(timer);
        }

        public override void Deserialize(BinaryReader reader)
        {
            timerMax = reader.ReadInt32();
            timer = reader.ReadInt32();
        }
    }
}
