﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Components.Scripts
{
    public class ScriptMoveFollow : Script
    {
        private string followTag;
        private float accel, max;
        private bool horizontalOnly;

        GameObject target;

        public ScriptMoveFollow(GameObject owner, string followTag, float accel, float max, bool horizontalOnly = true) : base(owner)
        {
            this.followTag = followTag;
            this.accel = accel;
            this.max = max;
            this.horizontalOnly = horizontalOnly;
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            if (active % 32==0 || active == 1) //we don't need to be searching every frame.
                target = owner.parentWorld.FindGameObjectWithTag(followTag);

            if (!horizontalOnly)
            {
                if (owner.collider != null)
                {
                    owner.collider.noclip = true;

                    Vector2 dist = target.transform.position - owner.transform.position;
                    dist = Vector2.Normalize(dist);

                    dist *= accel;

                    owner.collider.velocity += dist;
                }
            }
            else
            {
                if (owner.collider != null)
                {
                    Vector2 dist = target.transform.position - owner.transform.position;

                    if (dist.X > 0)
                        owner.collider.velocity.X += accel;
                    else owner.collider.velocity.X -= accel;

                    if (Math.Abs(owner.collider.velocity.X) > max)
                        owner.collider.velocity.X = max * Math.Sign(owner.collider.velocity.X);
                }
            }
        }

        public override object Clone()
        {
            return new ScriptMoveFollow(owner, followTag, accel, max, horizontalOnly);
        }

        public override int GetByteSize()
        {
            return followTag.Length * sizeof(char) +    //followTag
                4 + 4 +                                 //accel, max
                1;                                      //horizontalOnly
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(followTag);
            writer.Write(accel);
            writer.Write(max);
            writer.Write(horizontalOnly);
        }

        public override void Deserialize(BinaryReader reader)
        {
            followTag = reader.ReadString();
            accel = reader.ReadSingle();
            max = reader.ReadSingle();
            horizontalOnly = reader.ReadBoolean();   
        }
    }
}
