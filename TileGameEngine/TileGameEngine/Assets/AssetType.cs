﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Assets
{
    public class AssetType<T>
    {
        public Dictionary<string, T> assets;

        private string defaultKey;
        public AssetType(string defaultKey)
        {
            this.defaultKey = defaultKey;
            assets = new Dictionary<string, T>();
        }

        public T GetAsset(string name)
        {
            try
            {
                return assets[name];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                if (defaultKey != default(string))
                    return assets[defaultKey];
                return default(T);
            }
        }

        public void Add(string key, T value)
        {
            assets.Add(key, value);
        }
    }
}
