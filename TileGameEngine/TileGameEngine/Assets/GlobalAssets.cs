﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TileGameEngine.Assets
{
    public class GlobalAssets
    {
        public static bool exists;

        public BasicEffect basicEffect;

        public AssetType<Texture2D> textures;
        public AssetType<SpriteFont> fonts;

        public GlobalAssets(string errorTextureKey, string errorFontKey)
        {
            exists = true;

            textures = new AssetType<Texture2D>(errorTextureKey);
            fonts = new AssetType<SpriteFont>(errorFontKey);
        }

        /// <summary>
        /// Handle loading of global assets - things that will be loaded at all times, no matter what world, area, or menu - here.
        /// </summary>
        public virtual void Load(ContentManager content, GraphicsDevice device)
        {
            basicEffect = new BasicEffect(device);
            basicEffect.Projection = Matrix.CreateOrthographicOffCenter
                (0, device.Viewport.Width,     // left, right
                device.Viewport.Height, 0,    // bottom, top
                0, 1);
        }
    }
}
