﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using BlueRavenUtility;

namespace TileGameEngine.Assets
{
    public class WorldAssets : Assets
    {
        //public Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

        public AssetType<SpriteFont> fonts;
        public AssetType<Texture2D> textures;

        public WorldAssets()
        {
            fonts = new AssetType<SpriteFont>("error");
            textures = new AssetType<Texture2D>("error");
        }

        /// <summary>
        /// Handle loading of world assets - assets specific to the current loaded world - here.
        /// </summary>
        /// <param name="textureloadnames">array of names of textures. Any textures with a matching name *at the end of a file* excluding its filetype will be loaded.</param>
        public virtual void Load(ContentManager content, string fulldirectoryname, string[] textureloadnames)
        {
            DirectoryInfo d = new DirectoryInfo(fulldirectoryname + "/Content");
            List<FileInfo> files = d.GetFiles("*", SearchOption.AllDirectories).ToList();

            foreach (FileInfo file in files)
            {
                try
                {
                    string name = file.Name.Split('.')[0];

                    string keyname = TextHelper.FirstCharacterToLower(name);

                    string[] split = file.DirectoryName.Split('\\');
                    int index = split.ToList().IndexOf("Content");
                    string dirName = "";

                    try
                    {
                        dirName = split[index + 1];
                    }
                    catch
                    {
                        dirName = split[index];
                    }
                    
                    string directory = "";
                    for (int i = index + 1; i < split.Length; i++)
                    {   //super efficency
                        directory += split[i] + '/';
                    }

                    if (dirName == "Textures")
                    {
                        if (!textures.assets.ContainsKey(keyname))
                        {   //if names is null, just load everything.
                            if (textureloadnames == null || textureloadnames.Contains(name))
                                textures.Add(keyname, content.Load<Texture2D>(directory + name));
                        }
                    }

                    if (dirName == "Fonts")
                    {
                        if (!fonts.assets.ContainsKey(keyname))
                        {
                            fonts.Add(keyname, content.Load<SpriteFont>(directory + name));
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
