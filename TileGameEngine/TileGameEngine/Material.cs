﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Graphics;

namespace TileGameEngine
{
    public class Material
    {
        public Effect shader;

        public Dictionary<string, object> shaderParameters;

        public Material(Effect shader, Dictionary<string, object> shaderParameters)
        {
            this.shader = shader;
            this.shaderParameters = shaderParameters;
        }
    }
}
