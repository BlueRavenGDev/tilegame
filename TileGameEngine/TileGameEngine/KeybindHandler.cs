﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine
{
    public class KeybindHandler
    {
        public Dictionary<string, Keybind> keybinds;
        
        public KeybindHandler()
        {
            keybinds = new Dictionary<string, Keybind>();
        }

        public void RegisterKeybind(string name, Keybind keybind)
        {
            keybinds.Add(name, keybind);
        }

        public Keybind GetKeybind(string name)
        {
            try
            {
                return keybinds[name];
            }
            catch
            {
                return new Keybind();
            }
        }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(keybinds.Count);

            foreach (KeyValuePair<string, Keybind> kvkb in keybinds)
            {
                writer.Write(kvkb.Key);
                kvkb.Value.Serialize(writer);
            }
        }

        public void Deserialize(BinaryReader reader)
        {
            int count = reader.ReadInt32();

            for (int i = 0; i < count; i++)
            {
                string name = reader.ReadString();
                Keybind kb = new Keybind();
                kb.Deserialize(reader);
                keybinds.Add(name, kb);
            }
        }
    }

    public struct Keybind
    {
        private readonly Keys defaultKeybind;
        public Keys key;

        public Keybind(Keys defaultKeybind)
        {
            this.defaultKeybind = defaultKeybind;
            key = (Keys)0;
        }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write((int)key);
        }

        public void Deserialize(BinaryReader reader)
        {
            key = (Keys)reader.ReadInt32();
        }

        public void Reset()
        {
            key = defaultKeybind;
        }
    }
}
