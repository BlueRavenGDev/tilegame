﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Editor
{
    public delegate void GetObjectDelegate(out EditableValue value);

    public class EditableValue
    {
        public Action<object> setObjectFunc { get; private set; }
        public  Func<object> getObjectFunc { get; private set; }

        public EditableValue(Action<object> setObjFunc, Func<object> getObjFunc)
        {
            this.setObjectFunc = setObjFunc;
            this.getObjectFunc = getObjFunc;
        }

        public static EditableValue operator +(EditableValue t, EditableValue o)
        {
            t.setObjectFunc += o.setObjectFunc;
            t.getObjectFunc += o.getObjectFunc;
            return t;
        }
    }

    public interface IEditable
    {
        Dictionary<string, EditableValue> values { get; set; }

        void AddEditorValues();
    }
}
