﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileGameEngine.Editor
{
    public interface ISelectable
    {
        bool selected { get; set; }

        bool selectable { get; set; }

        bool IsSelecting();
    }
}
