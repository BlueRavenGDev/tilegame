﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;

namespace TileGameEngine.Editor.UI
{
    public partial class EditableValueList : UserControl
    {
        private List<IEditable> editables;

        public Dictionary<string, EditableValue> ldict; //localdict

        public EditableValueList(IEditable editable)
        {
            InitializeComponent();
            editables = new List<IEditable>();
            this.editables.Add(editable);
        }

        public EditableValueList(List<IEditable> editables)
        {
            InitializeComponent();
            this.editables = new List<IEditable>();
            this.editables = editables;
        }

        private void EditableValueList_Load(object sender, EventArgs e)
        {
            ldict = new Dictionary<string, EditableValue>();
            List<string> keyWithMultValues = new List<string>();

            editables.ForEach(x => x.AddEditorValues());

            foreach (IEditable editable in editables)
            {
                foreach (KeyValuePair<string, EditableValue> d in editable.values)
                {
                    if (ldict.ContainsKey(d.Key))
                    {
                        ldict[d.Key] += d.Value;   //if there are more than one of the same key, we put the delegates together so they both get called, so they both still 'exist' in a sense.
                        keyWithMultValues.Add(d.Key);
                    }
                    else
                        ldict.Add(d.Key, d.Value);
                }
            }

            editableValuesDGV.Columns.Add("Key", "KEY");
            editableValuesDGV.Columns[0].ReadOnly = true;   //no changing keys.
            editableValuesDGV.Columns.Add("Type", "TYPE");
            editableValuesDGV.Columns[1].ReadOnly = true;

            editableValuesDGV.Columns.Add("Values", "VALUES");

            foreach (KeyValuePair<string, EditableValue> item in ldict)
            {
                string typevalue, objectvalue;
                typevalue = item.Value.getObjectFunc().GetType().ToString().Split('.').Last();

                if (keyWithMultValues.Contains(item.Key))
                    objectvalue = "(multiple)";
                else objectvalue = item.Value.getObjectFunc().ToString();

                objectvalue = InterpretDefaultStrings(typevalue, objectvalue);

                editableValuesDGV.Rows.Add(item.Key, typevalue, objectvalue);
            }
        }

        private void editableValuesDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2) //if we're trying to change a value
            {
                SetCell(e.RowIndex);
            }
        }

        public void SetCell(int row)
        {   //since the 'key' value will always be in  the first cell of the row, we use Cells[0]
            EditableValue val = ldict[(string)editableValuesDGV.Rows[row].Cells[0].Value];    //the value in the IEditable
            string objectvalue = (string)editableValuesDGV.Rows[row].Cells[2].Value;
            string typevalue = (string)editableValuesDGV.Rows[row].Cells[1].Value;
            //typevalue = typevalue.Split('.').Last();

            object objfinal = null;

            editableValuesDGV.Rows[row].DefaultCellStyle.BackColor = System.Drawing.Color.White; //unmark row

            bool malformed = false;

            try
            {
                objfinal = GetObjectOfType(typevalue, objectvalue);
            }
            catch (Exception e)
            {
                GameMain.console.Output.Append(e.ToString());
                malformed = true;
            }

            if (malformed || objfinal == null)
            {
                editableValuesDGV.Rows[row].DefaultCellStyle.BackColor = System.Drawing.Color.Red; //mark the row so we know it's giving an error
                return; //return so we *don't* get an error.
            }

            val.setObjectFunc(objfinal);
        }

        public string InterpretDefaultStrings(string type, string objvalue)
        {
            if (objvalue != "(multiple)")
            {
                if (type.ToLower() == "vector2")
                {
                    string s = objvalue.Replace("{", "");
                    s = s.Replace("}", "");
                    s = s.Replace("X", "");
                    s = s.Replace("Y", "");
                    s = s.Replace(":", "");

                    string[] split = s.Split(' ');
                    return split[0] + "," + split[1];
                }
                else if (type.ToLower() == "color")
                {
                    string s = objvalue.Replace("{", "");
                    s = s.Replace("}", "");

                    string[] split = objvalue.Split(' ');

                    return split[0] + ", " + split[1] + ", " + split[2] + ", " + split[3];
                }
                else if (type.ToLower() == "rectangle")
                {
                    string s = objvalue.Replace("{", "");
                    s = s.Replace("}", "");
                    s = s.Replace("X", "");
                    s = s.Replace("Y", "");
                    s = s.Replace(":", "");
                    s = s.Replace("Width", "");
                    s = s.Replace("Height", "");

                    string[] split = s.Split(' ');

                    return split[0] + ", " + split[1] + ", " + split[2] + ", " + split[3];
                }
                else return objvalue;
            }
            return objvalue;
        }

        public object GetObjectOfType(string type, string obj)
        {
            if (type.ToLower() == "string")
            {
                return (string)obj;
            }
            else if (type.ToLower() == "int" || type.ToLower().StartsWith("int"))
            {
                return int.Parse(obj);
            }
            else if (type.ToLower() == "float" || type.ToLower() == "single")
            {
                return float.Parse(obj);
            }
            else if (type.ToLower() == "bool" || type.ToLower() == "boolean")
            {
                return bool.Parse(obj);
            }
            else if (type.ToLower() == "vector2")
            {
                string[] split = obj.Split(',');
                return new Vector2(int.Parse(split[0]), int.Parse(split[1]));
            }
            else if (type.ToLower() == "color")
            {
                string[] split = obj.Split(',');
                return new Microsoft.Xna.Framework.Color(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]), split.Length > 3 ? int.Parse(split[3]) : 255);
            }
            else if (type.ToLower() == "rectangle")
            {
                string[] split = obj.Split(',');
                return new Microsoft.Xna.Framework.Rectangle(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]), int.Parse(split[3]));
            }
            else return null;
        }

        private void editableValuesDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
