﻿namespace TileGameEngine.Editor.UI
{
	partial class TextureSelector
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pb_imageselector = new System.Windows.Forms.PictureBox();
			this.btn_loadimage = new System.Windows.Forms.Button();
			this.pnl_holdertexture = new System.Windows.Forms.Panel();
			this.nud_zoomscale = new System.Windows.Forms.NumericUpDown();
			this.lbl_zoomscale = new System.Windows.Forms.Label();
			this.lbl_mousexpos = new System.Windows.Forms.Label();
			this.lbl_mouseypos = new System.Windows.Forms.Label();
			this.nud_tilesize = new System.Windows.Forms.NumericUpDown();
			this.lbl_tilesize = new System.Windows.Forms.Label();
			this.btn_paintselected = new System.Windows.Forms.Button();
			this.cb_flipx = new System.Windows.Forms.CheckBox();
			this.cb_flipy = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.pb_imageselector)).BeginInit();
			this.pnl_holdertexture.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_zoomscale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_tilesize)).BeginInit();
			this.SuspendLayout();
			// 
			// pb_imageselector
			// 
			this.pb_imageselector.Location = new System.Drawing.Point(3, 3);
			this.pb_imageselector.Name = "pb_imageselector";
			this.pb_imageselector.Size = new System.Drawing.Size(572, 431);
			this.pb_imageselector.TabIndex = 0;
			this.pb_imageselector.TabStop = false;
			this.pb_imageselector.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_imageselector_Paint);
			this.pb_imageselector.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pb_imageselector_MouseClick);
			this.pb_imageselector.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pb_imageselector_MouseMove);
			// 
			// btn_loadimage
			// 
			this.btn_loadimage.Location = new System.Drawing.Point(12, 61);
			this.btn_loadimage.Name = "btn_loadimage";
			this.btn_loadimage.Size = new System.Drawing.Size(78, 43);
			this.btn_loadimage.TabIndex = 1;
			this.btn_loadimage.Text = "Load Image";
			this.btn_loadimage.UseVisualStyleBackColor = true;
			this.btn_loadimage.Click += new System.EventHandler(this.btn_loadimage_Click);
			// 
			// pnl_holdertexture
			// 
			this.pnl_holdertexture.AutoScroll = true;
			this.pnl_holdertexture.Controls.Add(this.pb_imageselector);
			this.pnl_holdertexture.Location = new System.Drawing.Point(93, 61);
			this.pnl_holdertexture.Name = "pnl_holdertexture";
			this.pnl_holdertexture.Size = new System.Drawing.Size(640, 640);
			this.pnl_holdertexture.TabIndex = 2;
			// 
			// nud_zoomscale
			// 
			this.nud_zoomscale.DecimalPlaces = 4;
			this.nud_zoomscale.Location = new System.Drawing.Point(12, 110);
			this.nud_zoomscale.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			131072});
			this.nud_zoomscale.Name = "nud_zoomscale";
			this.nud_zoomscale.Size = new System.Drawing.Size(78, 22);
			this.nud_zoomscale.TabIndex = 1;
			this.nud_zoomscale.Value = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.nud_zoomscale.ValueChanged += new System.EventHandler(this.nud_zoomscale_ValueChanged);
			// 
			// lbl_zoomscale
			// 
			this.lbl_zoomscale.AutoSize = true;
			this.lbl_zoomscale.Location = new System.Drawing.Point(9, 135);
			this.lbl_zoomscale.Name = "lbl_zoomscale";
			this.lbl_zoomscale.Size = new System.Drawing.Size(83, 17);
			this.lbl_zoomscale.TabIndex = 3;
			this.lbl_zoomscale.Text = "Zoom Scale";
			// 
			// lbl_mousexpos
			// 
			this.lbl_mousexpos.AutoSize = true;
			this.lbl_mousexpos.Location = new System.Drawing.Point(12, 7);
			this.lbl_mousexpos.Name = "lbl_mousexpos";
			this.lbl_mousexpos.Size = new System.Drawing.Size(33, 17);
			this.lbl_mousexpos.TabIndex = 4;
			this.lbl_mousexpos.Text = "X: 0";
			// 
			// lbl_mouseypos
			// 
			this.lbl_mouseypos.AutoSize = true;
			this.lbl_mouseypos.Location = new System.Drawing.Point(12, 35);
			this.lbl_mouseypos.Name = "lbl_mouseypos";
			this.lbl_mouseypos.Size = new System.Drawing.Size(33, 17);
			this.lbl_mouseypos.TabIndex = 5;
			this.lbl_mouseypos.Text = "Y: 0";
			// 
			// nud_tilesize
			// 
			this.nud_tilesize.Location = new System.Drawing.Point(12, 155);
			this.nud_tilesize.Maximum = new decimal(new int[] {
			1024,
			0,
			0,
			0});
			this.nud_tilesize.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.nud_tilesize.Name = "nud_tilesize";
			this.nud_tilesize.Size = new System.Drawing.Size(78, 22);
			this.nud_tilesize.TabIndex = 1;
			this.nud_tilesize.Value = new decimal(new int[] {
			16,
			0,
			0,
			0});
			this.nud_tilesize.ValueChanged += new System.EventHandler(this.nud_tilesize_ValueChanged);
			// 
			// lbl_tilesize
			// 
			this.lbl_tilesize.AutoSize = true;
			this.lbl_tilesize.Location = new System.Drawing.Point(9, 180);
			this.lbl_tilesize.Name = "lbl_tilesize";
			this.lbl_tilesize.Size = new System.Drawing.Size(62, 17);
			this.lbl_tilesize.TabIndex = 6;
			this.lbl_tilesize.Text = "Tile Size";
			// 
			// btn_paintselected
			// 
			this.btn_paintselected.Location = new System.Drawing.Point(658, 12);
			this.btn_paintselected.Name = "btn_paintselected";
			this.btn_paintselected.Size = new System.Drawing.Size(75, 45);
			this.btn_paintselected.TabIndex = 7;
			this.btn_paintselected.Text = "Paint";
			this.btn_paintselected.UseVisualStyleBackColor = true;
			this.btn_paintselected.Click += new System.EventHandler(this.btn_paintselected_Click);
			// 
			// cb_flipx
			// 
			this.cb_flipx.AutoSize = true;
			this.cb_flipx.Location = new System.Drawing.Point(12, 200);
			this.cb_flipx.Name = "cb_flipx";
			this.cb_flipx.Size = new System.Drawing.Size(65, 21);
			this.cb_flipx.TabIndex = 8;
			this.cb_flipx.Text = "Flip X";
			this.cb_flipx.UseVisualStyleBackColor = true;
			this.cb_flipx.CheckedChanged += new System.EventHandler(this.cb_flipx_CheckedChanged);
			// 
			// cb_flipy
			// 
			this.cb_flipy.AutoSize = true;
			this.cb_flipy.Location = new System.Drawing.Point(12, 227);
			this.cb_flipy.Name = "cb_flipy";
			this.cb_flipy.Size = new System.Drawing.Size(65, 21);
			this.cb_flipy.TabIndex = 9;
			this.cb_flipy.Text = "Flip Y";
			this.cb_flipy.UseVisualStyleBackColor = true;
			this.cb_flipy.CheckedChanged += new System.EventHandler(this.cb_flipy_CheckedChanged);
			// 
			// TextureSelector
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(767, 709);
			this.Controls.Add(this.cb_flipy);
			this.Controls.Add(this.cb_flipx);
			this.Controls.Add(this.btn_paintselected);
			this.Controls.Add(this.lbl_tilesize);
			this.Controls.Add(this.nud_tilesize);
			this.Controls.Add(this.lbl_mouseypos);
			this.Controls.Add(this.lbl_mousexpos);
			this.Controls.Add(this.lbl_zoomscale);
			this.Controls.Add(this.nud_zoomscale);
			this.Controls.Add(this.pnl_holdertexture);
			this.Controls.Add(this.btn_loadimage);
			this.Name = "TextureSelector";
			this.Text = "Texture Selection Window";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TextureSelector_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.pb_imageselector)).EndInit();
			this.pnl_holdertexture.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nud_zoomscale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_tilesize)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pb_imageselector;
		private System.Windows.Forms.Button btn_loadimage;
		private System.Windows.Forms.Panel pnl_holdertexture;
		private System.Windows.Forms.NumericUpDown nud_zoomscale;
		private System.Windows.Forms.Label lbl_zoomscale;
		private System.Windows.Forms.Label lbl_mousexpos;
		private System.Windows.Forms.Label lbl_mouseypos;
		private System.Windows.Forms.NumericUpDown nud_tilesize;
		private System.Windows.Forms.Label lbl_tilesize;
		private System.Windows.Forms.Button btn_paintselected;
		private System.Windows.Forms.CheckBox cb_flipx;
		private System.Windows.Forms.CheckBox cb_flipy;
	}
}