﻿namespace TileGameEngine.Editor.UI
{
    partial class EditableValueList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editableValuesDGV = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.editableValuesDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // editableValuesDGV
            // 
            this.editableValuesDGV.AllowUserToAddRows = false;
            this.editableValuesDGV.AllowUserToDeleteRows = false;
            this.editableValuesDGV.AllowUserToResizeColumns = false;
            this.editableValuesDGV.AllowUserToResizeRows = false;
            this.editableValuesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.editableValuesDGV.Location = new System.Drawing.Point(3, 3);
            this.editableValuesDGV.Name = "editableValuesDGV";
            this.editableValuesDGV.RowTemplate.Height = 24;
            this.editableValuesDGV.Size = new System.Drawing.Size(368, 445);
            this.editableValuesDGV.TabIndex = 2;
            this.editableValuesDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.editableValuesDGV_CellContentClick);
            this.editableValuesDGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.editableValuesDGV_CellValueChanged);
            // 
            // EditableValueList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.editableValuesDGV);
            this.Name = "EditableValueList";
            this.Size = new System.Drawing.Size(374, 451);
            this.Load += new System.EventHandler(this.EditableValueList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.editableValuesDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView editableValuesDGV;
    }
}
