﻿namespace TileGameEngine.Editor.UI
{
    partial class EditPanelWorld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createPrefabToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadPrefabToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allNotTilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_select = new System.Windows.Forms.RadioButton();
            this.rb_add = new System.Windows.Forms.RadioButton();
            this.nud_layerselector = new System.Windows.Forms.NumericUpDown();
            this.gb_world = new System.Windows.Forms.GroupBox();
            this.gb_gameobj = new System.Windows.Forms.GroupBox();
            this.tc_gameobjects = new System.Windows.Forms.TabControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_entityname = new System.Windows.Forms.TextBox();
            this.cb_firstonly = new System.Windows.Forms.CheckBox();
            this.nud_gridsize = new System.Windows.Forms.NumericUpDown();
            this.cb_drawgrid = new System.Windows.Forms.CheckBox();
            this.btn_delalloftype = new System.Windows.Forms.Button();
            this.gb_selectionmode = new System.Windows.Forms.GroupBox();
            this.cb_selectgroups = new System.Windows.Forms.CheckBox();
            this.rb_selectnottiles = new System.Windows.Forms.RadioButton();
            this.rb_selecttiles = new System.Windows.Forms.RadioButton();
            this.rb_selectall = new System.Windows.Forms.RadioButton();
            this.lbl_layer = new System.Windows.Forms.Label();
            this.btn_rotc = new System.Windows.Forms.Button();
            this.btn_rotcc = new System.Windows.Forms.Button();
            this.btn_fliphoriz = new System.Windows.Forms.Button();
            this.btn_flipvert = new System.Windows.Forms.Button();
            this.btn_selectimage = new System.Windows.Forms.Button();
            this.cb_drawtilemarkers = new System.Windows.Forms.CheckBox();
            this.cb_drawothermarkers = new System.Windows.Forms.CheckBox();
            this.cb_drawlayeronly = new System.Windows.Forms.CheckBox();
            this.gb_drawing = new System.Windows.Forms.GroupBox();
            this.lbl_gridsize = new System.Windows.Forms.Label();
            this.gb_transform = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_layerselector)).BeginInit();
            this.gb_gameobj.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_gridsize)).BeginInit();
            this.gb_selectionmode.SuspendLayout();
            this.gb_drawing.SuspendLayout();
            this.gb_transform.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1027, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.createPrefabToolStripMenuItem1,
            this.loadPrefabToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // createPrefabToolStripMenuItem1
            // 
            this.createPrefabToolStripMenuItem1.Name = "createPrefabToolStripMenuItem1";
            this.createPrefabToolStripMenuItem1.Size = new System.Drawing.Size(166, 26);
            this.createPrefabToolStripMenuItem1.Text = "Save Prefab";
            this.createPrefabToolStripMenuItem1.Click += new System.EventHandler(this.savePrefabToolStripMenuItem_Click);
            // 
            // loadPrefabToolStripMenuItem1
            // 
            this.loadPrefabToolStripMenuItem1.Name = "loadPrefabToolStripMenuItem1";
            this.loadPrefabToolStripMenuItem1.Size = new System.Drawing.Size(166, 26);
            this.loadPrefabToolStripMenuItem1.Text = "Load Prefab";
            this.loadPrefabToolStripMenuItem1.Click += new System.EventHandler(this.loadPrefabToolStripMenuItem1_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allTilesToolStripMenuItem,
            this.allNotTilesToolStripMenuItem,
            this.allToolStripMenuItem});
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // allTilesToolStripMenuItem
            // 
            this.allTilesToolStripMenuItem.Name = "allTilesToolStripMenuItem";
            this.allTilesToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.allTilesToolStripMenuItem.Text = "All Tiles";
            this.allTilesToolStripMenuItem.Click += new System.EventHandler(this.allTilesToolStripMenuItem_Click);
            // 
            // allNotTilesToolStripMenuItem
            // 
            this.allNotTilesToolStripMenuItem.Name = "allNotTilesToolStripMenuItem";
            this.allNotTilesToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.allNotTilesToolStripMenuItem.Text = "All Not Tiles";
            this.allNotTilesToolStripMenuItem.Click += new System.EventHandler(this.allNotTilesToolStripMenuItem_Click);
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.Delete)));
            this.allToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.allToolStripMenuItem.Text = "All";
            // 
            // rb_select
            // 
            this.rb_select.AutoSize = true;
            this.rb_select.Location = new System.Drawing.Point(12, 31);
            this.rb_select.Name = "rb_select";
            this.rb_select.Size = new System.Drawing.Size(68, 21);
            this.rb_select.TabIndex = 1;
            this.rb_select.TabStop = true;
            this.rb_select.Text = "Select";
            this.rb_select.UseVisualStyleBackColor = true;
            // 
            // rb_add
            // 
            this.rb_add.AutoSize = true;
            this.rb_add.Location = new System.Drawing.Point(12, 58);
            this.rb_add.Name = "rb_add";
            this.rb_add.Size = new System.Drawing.Size(71, 21);
            this.rb_add.TabIndex = 2;
            this.rb_add.TabStop = true;
            this.rb_add.Text = "Create";
            this.rb_add.UseVisualStyleBackColor = true;
            // 
            // nud_layerselector
            // 
            this.nud_layerselector.Location = new System.Drawing.Point(12, 188);
            this.nud_layerselector.Name = "nud_layerselector";
            this.nud_layerselector.Size = new System.Drawing.Size(84, 22);
            this.nud_layerselector.TabIndex = 4;
            this.nud_layerselector.ValueChanged += new System.EventHandler(this.nudLayerSelector_ValueChanged);
            // 
            // gb_world
            // 
            this.gb_world.Location = new System.Drawing.Point(241, 32);
            this.gb_world.Name = "gb_world";
            this.gb_world.Size = new System.Drawing.Size(380, 471);
            this.gb_world.TabIndex = 6;
            this.gb_world.TabStop = false;
            this.gb_world.Text = "World";
            // 
            // gb_gameobj
            // 
            this.gb_gameobj.Controls.Add(this.tc_gameobjects);
            this.gb_gameobj.Controls.Add(this.groupBox2);
            this.gb_gameobj.Location = new System.Drawing.Point(627, 32);
            this.gb_gameobj.Name = "gb_gameobj";
            this.gb_gameobj.Size = new System.Drawing.Size(388, 494);
            this.gb_gameobj.TabIndex = 7;
            this.gb_gameobj.TabStop = false;
            this.gb_gameobj.Text = "GameObject";
            // 
            // tc_gameobjects
            // 
            this.tc_gameobjects.Location = new System.Drawing.Point(6, 21);
            this.tc_gameobjects.Name = "tc_gameobjects";
            this.tc_gameobjects.SelectedIndex = 0;
            this.tc_gameobjects.Size = new System.Drawing.Size(380, 471);
            this.tc_gameobjects.TabIndex = 8;
            this.tc_gameobjects.SelectedIndexChanged += new System.EventHandler(this.tc_gameobjects_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(386, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(380, 471);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "GameObject";
            // 
            // tb_entityname
            // 
            this.tb_entityname.Location = new System.Drawing.Point(12, 160);
            this.tb_entityname.Name = "tb_entityname";
            this.tb_entityname.Size = new System.Drawing.Size(134, 22);
            this.tb_entityname.TabIndex = 8;
            this.tb_entityname.Text = "Preset Name";
            this.tb_entityname.TextChanged += new System.EventHandler(this.tb_entityname_TextChanged);
            // 
            // cb_firstonly
            // 
            this.cb_firstonly.AutoSize = true;
            this.cb_firstonly.Location = new System.Drawing.Point(6, 129);
            this.cb_firstonly.Name = "cb_firstonly";
            this.cb_firstonly.Size = new System.Drawing.Size(90, 21);
            this.cb_firstonly.TabIndex = 9;
            this.cb_firstonly.Text = "First Only";
            this.cb_firstonly.UseVisualStyleBackColor = true;
            this.cb_firstonly.CheckedChanged += new System.EventHandler(this.cb_firstonly_CheckedChanged);
            // 
            // nud_gridsize
            // 
            this.nud_gridsize.Location = new System.Drawing.Point(12, 521);
            this.nud_gridsize.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.nud_gridsize.Name = "nud_gridsize";
            this.nud_gridsize.Size = new System.Drawing.Size(84, 22);
            this.nud_gridsize.TabIndex = 10;
            this.nud_gridsize.ValueChanged += new System.EventHandler(this.nud_gridsize_ValueChanged);
            // 
            // cb_drawgrid
            // 
            this.cb_drawgrid.AutoSize = true;
            this.cb_drawgrid.Checked = true;
            this.cb_drawgrid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_drawgrid.Location = new System.Drawing.Point(6, 21);
            this.cb_drawgrid.Name = "cb_drawgrid";
            this.cb_drawgrid.Size = new System.Drawing.Size(93, 21);
            this.cb_drawgrid.TabIndex = 11;
            this.cb_drawgrid.Text = "Draw Grid";
            this.cb_drawgrid.UseVisualStyleBackColor = true;
            this.cb_drawgrid.CheckedChanged += new System.EventHandler(this.cb_showgrid_CheckedChanged);
            // 
            // btn_delalloftype
            // 
            this.btn_delalloftype.Location = new System.Drawing.Point(152, 160);
            this.btn_delalloftype.Name = "btn_delalloftype";
            this.btn_delalloftype.Size = new System.Drawing.Size(75, 50);
            this.btn_delalloftype.TabIndex = 12;
            this.btn_delalloftype.Text = "Delete All";
            this.btn_delalloftype.UseVisualStyleBackColor = true;
            this.btn_delalloftype.Click += new System.EventHandler(this.btn_delalloftype_Click);
            // 
            // gb_selectionmode
            // 
            this.gb_selectionmode.Controls.Add(this.cb_selectgroups);
            this.gb_selectionmode.Controls.Add(this.rb_selectnottiles);
            this.gb_selectionmode.Controls.Add(this.rb_selecttiles);
            this.gb_selectionmode.Controls.Add(this.rb_selectall);
            this.gb_selectionmode.Controls.Add(this.cb_firstonly);
            this.gb_selectionmode.Location = new System.Drawing.Point(12, 216);
            this.gb_selectionmode.Name = "gb_selectionmode";
            this.gb_selectionmode.Size = new System.Drawing.Size(215, 156);
            this.gb_selectionmode.TabIndex = 13;
            this.gb_selectionmode.TabStop = false;
            this.gb_selectionmode.Text = "Selection";
            // 
            // cb_selectgroups
            // 
            this.cb_selectgroups.AutoSize = true;
            this.cb_selectgroups.Checked = true;
            this.cb_selectgroups.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_selectgroups.Location = new System.Drawing.Point(6, 102);
            this.cb_selectgroups.Name = "cb_selectgroups";
            this.cb_selectgroups.Size = new System.Drawing.Size(120, 21);
            this.cb_selectgroups.TabIndex = 3;
            this.cb_selectgroups.Text = "Select Groups";
            this.cb_selectgroups.UseVisualStyleBackColor = true;
            this.cb_selectgroups.CheckedChanged += new System.EventHandler(this.cb_selectgroups_CheckedChanged);
            // 
            // rb_selectnottiles
            // 
            this.rb_selectnottiles.AutoSize = true;
            this.rb_selectnottiles.Location = new System.Drawing.Point(6, 75);
            this.rb_selectnottiles.Name = "rb_selectnottiles";
            this.rb_selectnottiles.Size = new System.Drawing.Size(128, 21);
            this.rb_selectnottiles.TabIndex = 2;
            this.rb_selectnottiles.TabStop = true;
            this.rb_selectnottiles.Tag = "2";
            this.rb_selectnottiles.Text = "Select Not Tiles";
            this.rb_selectnottiles.UseVisualStyleBackColor = true;
            this.rb_selectnottiles.CheckedChanged += new System.EventHandler(this.rb_selectmode_CheckedChanged);
            // 
            // rb_selecttiles
            // 
            this.rb_selecttiles.AutoSize = true;
            this.rb_selecttiles.Location = new System.Drawing.Point(6, 48);
            this.rb_selecttiles.Name = "rb_selecttiles";
            this.rb_selecttiles.Size = new System.Drawing.Size(102, 21);
            this.rb_selecttiles.TabIndex = 1;
            this.rb_selecttiles.TabStop = true;
            this.rb_selecttiles.Tag = "1";
            this.rb_selecttiles.Text = "Select Tiles";
            this.rb_selecttiles.UseVisualStyleBackColor = true;
            this.rb_selecttiles.CheckedChanged += new System.EventHandler(this.rb_selectmode_CheckedChanged);
            // 
            // rb_selectall
            // 
            this.rb_selectall.AutoSize = true;
            this.rb_selectall.Checked = true;
            this.rb_selectall.Location = new System.Drawing.Point(6, 21);
            this.rb_selectall.Name = "rb_selectall";
            this.rb_selectall.Size = new System.Drawing.Size(87, 21);
            this.rb_selectall.TabIndex = 0;
            this.rb_selectall.TabStop = true;
            this.rb_selectall.Tag = "0";
            this.rb_selectall.Text = "Select All";
            this.rb_selectall.UseVisualStyleBackColor = true;
            this.rb_selectall.CheckedChanged += new System.EventHandler(this.rb_selectmode_CheckedChanged);
            // 
            // lbl_layer
            // 
            this.lbl_layer.AutoSize = true;
            this.lbl_layer.Location = new System.Drawing.Point(102, 190);
            this.lbl_layer.Name = "lbl_layer";
            this.lbl_layer.Size = new System.Drawing.Size(44, 17);
            this.lbl_layer.TabIndex = 14;
            this.lbl_layer.Text = "Layer";
            // 
            // btn_rotc
            // 
            this.btn_rotc.Location = new System.Drawing.Point(8, 21);
            this.btn_rotc.Name = "btn_rotc";
            this.btn_rotc.Size = new System.Drawing.Size(201, 23);
            this.btn_rotc.TabIndex = 15;
            this.btn_rotc.Text = "Rotate Clockwise";
            this.btn_rotc.UseVisualStyleBackColor = true;
            this.btn_rotc.Click += new System.EventHandler(this.btn_rotc_Click);
            // 
            // btn_rotcc
            // 
            this.btn_rotcc.Location = new System.Drawing.Point(8, 50);
            this.btn_rotcc.Name = "btn_rotcc";
            this.btn_rotcc.Size = new System.Drawing.Size(201, 23);
            this.btn_rotcc.TabIndex = 16;
            this.btn_rotcc.Text = "Rotate Counter-Clockwise";
            this.btn_rotcc.UseVisualStyleBackColor = true;
            this.btn_rotcc.Click += new System.EventHandler(this.btn_rotcc_Click);
            // 
            // btn_fliphoriz
            // 
            this.btn_fliphoriz.Location = new System.Drawing.Point(8, 79);
            this.btn_fliphoriz.Name = "btn_fliphoriz";
            this.btn_fliphoriz.Size = new System.Drawing.Size(201, 23);
            this.btn_fliphoriz.TabIndex = 17;
            this.btn_fliphoriz.Text = "Flip Horizontally";
            this.btn_fliphoriz.UseVisualStyleBackColor = true;
            this.btn_fliphoriz.Click += new System.EventHandler(this.btn_fliphoriz_Click);
            // 
            // btn_flipvert
            // 
            this.btn_flipvert.Location = new System.Drawing.Point(8, 108);
            this.btn_flipvert.Name = "btn_flipvert";
            this.btn_flipvert.Size = new System.Drawing.Size(201, 23);
            this.btn_flipvert.TabIndex = 18;
            this.btn_flipvert.Text = "Flip Vertically";
            this.btn_flipvert.UseVisualStyleBackColor = true;
            this.btn_flipvert.Click += new System.EventHandler(this.btn_flipvert_Click);
            // 
            // btn_selectimage
            // 
            this.btn_selectimage.Location = new System.Drawing.Point(241, 509);
            this.btn_selectimage.Name = "btn_selectimage";
            this.btn_selectimage.Size = new System.Drawing.Size(120, 23);
            this.btn_selectimage.TabIndex = 19;
            this.btn_selectimage.Text = "Image Select";
            this.btn_selectimage.UseVisualStyleBackColor = true;
            this.btn_selectimage.Click += new System.EventHandler(this.btn_selectimage_Click);
            // 
            // cb_drawtilemarkers
            // 
            this.cb_drawtilemarkers.AutoSize = true;
            this.cb_drawtilemarkers.Checked = true;
            this.cb_drawtilemarkers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_drawtilemarkers.Location = new System.Drawing.Point(6, 48);
            this.cb_drawtilemarkers.Name = "cb_drawtilemarkers";
            this.cb_drawtilemarkers.Size = new System.Drawing.Size(144, 21);
            this.cb_drawtilemarkers.TabIndex = 20;
            this.cb_drawtilemarkers.Text = "Draw Tile Markers";
            this.cb_drawtilemarkers.UseVisualStyleBackColor = true;
            this.cb_drawtilemarkers.CheckedChanged += new System.EventHandler(this.cb_drawtilemarkers_CheckedChanged);
            // 
            // cb_drawothermarkers
            // 
            this.cb_drawothermarkers.AutoSize = true;
            this.cb_drawothermarkers.Checked = true;
            this.cb_drawothermarkers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_drawothermarkers.Location = new System.Drawing.Point(6, 75);
            this.cb_drawothermarkers.Name = "cb_drawothermarkers";
            this.cb_drawothermarkers.Size = new System.Drawing.Size(157, 21);
            this.cb_drawothermarkers.TabIndex = 21;
            this.cb_drawothermarkers.Text = "Draw Other Markers";
            this.cb_drawothermarkers.UseVisualStyleBackColor = true;
            this.cb_drawothermarkers.CheckedChanged += new System.EventHandler(this.cb_drawothermarkers_CheckedChanged);
            // 
            // cb_drawlayeronly
            // 
            this.cb_drawlayeronly.AutoSize = true;
            this.cb_drawlayeronly.Location = new System.Drawing.Point(6, 102);
            this.cb_drawlayeronly.Name = "cb_drawlayeronly";
            this.cb_drawlayeronly.Size = new System.Drawing.Size(135, 21);
            this.cb_drawlayeronly.TabIndex = 22;
            this.cb_drawlayeronly.Text = "Draw Layer Only";
            this.cb_drawlayeronly.UseVisualStyleBackColor = true;
            this.cb_drawlayeronly.CheckedChanged += new System.EventHandler(this.cb_drawlayeronly_CheckedChanged);
            // 
            // gb_drawing
            // 
            this.gb_drawing.Controls.Add(this.cb_drawgrid);
            this.gb_drawing.Controls.Add(this.cb_drawlayeronly);
            this.gb_drawing.Controls.Add(this.cb_drawtilemarkers);
            this.gb_drawing.Controls.Add(this.cb_drawothermarkers);
            this.gb_drawing.Location = new System.Drawing.Point(12, 549);
            this.gb_drawing.Name = "gb_drawing";
            this.gb_drawing.Size = new System.Drawing.Size(215, 130);
            this.gb_drawing.TabIndex = 23;
            this.gb_drawing.TabStop = false;
            this.gb_drawing.Text = "Drawing";
            // 
            // lbl_gridsize
            // 
            this.lbl_gridsize.AutoSize = true;
            this.lbl_gridsize.Location = new System.Drawing.Point(102, 523);
            this.lbl_gridsize.Name = "lbl_gridsize";
            this.lbl_gridsize.Size = new System.Drawing.Size(66, 17);
            this.lbl_gridsize.TabIndex = 24;
            this.lbl_gridsize.Text = "Grid Size";
            // 
            // gb_transform
            // 
            this.gb_transform.Controls.Add(this.btn_rotc);
            this.gb_transform.Controls.Add(this.btn_rotcc);
            this.gb_transform.Controls.Add(this.btn_fliphoriz);
            this.gb_transform.Controls.Add(this.btn_flipvert);
            this.gb_transform.Location = new System.Drawing.Point(12, 378);
            this.gb_transform.Name = "gb_transform";
            this.gb_transform.Size = new System.Drawing.Size(215, 137);
            this.gb_transform.TabIndex = 25;
            this.gb_transform.TabStop = false;
            this.gb_transform.Text = "Transform";
            // 
            // EditPanelWorld
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 691);
            this.Controls.Add(this.gb_transform);
            this.Controls.Add(this.lbl_gridsize);
            this.Controls.Add(this.gb_drawing);
            this.Controls.Add(this.btn_selectimage);
            this.Controls.Add(this.lbl_layer);
            this.Controls.Add(this.gb_selectionmode);
            this.Controls.Add(this.btn_delalloftype);
            this.Controls.Add(this.nud_gridsize);
            this.Controls.Add(this.tb_entityname);
            this.Controls.Add(this.gb_gameobj);
            this.Controls.Add(this.gb_world);
            this.Controls.Add(this.nud_layerselector);
            this.Controls.Add(this.rb_add);
            this.Controls.Add(this.rb_select);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EditPanelWorld";
            this.Text = "EditPanel";
            this.Load += new System.EventHandler(this.EditPanelWorld_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_layerselector)).EndInit();
            this.gb_gameobj.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nud_gridsize)).EndInit();
            this.gb_selectionmode.ResumeLayout(false);
            this.gb_selectionmode.PerformLayout();
            this.gb_drawing.ResumeLayout(false);
            this.gb_drawing.PerformLayout();
            this.gb_transform.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.RadioButton rb_select;
        private System.Windows.Forms.RadioButton rb_add;
        private System.Windows.Forms.NumericUpDown nud_layerselector;
        private System.Windows.Forms.GroupBox gb_world;
        private System.Windows.Forms.GroupBox gb_gameobj;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tc_gameobjects;
        private System.Windows.Forms.TextBox tb_entityname;
        private System.Windows.Forms.CheckBox cb_firstonly;
        private System.Windows.Forms.NumericUpDown nud_gridsize;
        private System.Windows.Forms.CheckBox cb_drawgrid;
        private System.Windows.Forms.ToolStripMenuItem createPrefabToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadPrefabToolStripMenuItem1;
        private System.Windows.Forms.Button btn_delalloftype;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allTilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allNotTilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.GroupBox gb_selectionmode;
        private System.Windows.Forms.CheckBox cb_selectgroups;
        private System.Windows.Forms.RadioButton rb_selectnottiles;
        private System.Windows.Forms.RadioButton rb_selecttiles;
        private System.Windows.Forms.RadioButton rb_selectall;
        private System.Windows.Forms.Label lbl_layer;
        private System.Windows.Forms.Button btn_rotc;
        private System.Windows.Forms.Button btn_rotcc;
        private System.Windows.Forms.Button btn_fliphoriz;
        private System.Windows.Forms.Button btn_flipvert;
        private System.Windows.Forms.Button btn_selectimage;
        private System.Windows.Forms.CheckBox cb_drawtilemarkers;
        private System.Windows.Forms.CheckBox cb_drawothermarkers;
        private System.Windows.Forms.CheckBox cb_drawlayeronly;
        private System.Windows.Forms.GroupBox gb_drawing;
        private System.Windows.Forms.Label lbl_gridsize;
        private System.Windows.Forms.GroupBox gb_transform;
    }
}