﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TileGameEngine.Editor.UI
{
    public partial class TextureSelector : Form
    {
        public float zoomscale = 1;

        private Bitmap originalBitmap;

        private Vector2 mousePos;

        private int tileSize = 16;
        private System.Drawing.Rectangle currentSelectedRect;
        public string texturename { get; private set; }

        private GameMain main;

        public bool setTexture { get; private set; }

        private bool flipX, flipY;

        public TextureSelector(GameMain main)
        {
            InitializeComponent();

            this.main = main;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_loadimage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog image = new OpenFileDialog())
            {
                image.InitialDirectory = "C:\\Users\\taylo\\Documents\\Programming\\C#\\Game\\TileGame\\TileGame\\TileGame\\Content\\Textures";
                image.Title = "Open Image";
                image.Filter = "png files (*.png)|*.png";

                if (image.ShowDialog() == DialogResult.OK)
                {
                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property
                    originalBitmap = new Bitmap(image.FileName);
                    pb_imageselector.Image = originalBitmap;
                    pb_imageselector.Size = originalBitmap.Size;

                    texturename = image.FileName.Split('\\').Last().Split('.').First();

                    setTexture = true;
                }
            }
        }

        private void TextureSelector_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void nud_zoomscale_ValueChanged(object sender, EventArgs e)
        {
            zoomscale = (float)nud_zoomscale.Value;

            Size newSize = new Size((int)(originalBitmap.Width * zoomscale), (int)(originalBitmap.Height * zoomscale));
            Bitmap newBitmap = new Bitmap(originalBitmap, newSize);
            pb_imageselector.Image = newBitmap;
            pb_imageselector.Size = newSize;

            currentSelectedRect = new System.Drawing.Rectangle(currentSelectedRect.X, currentSelectedRect.Y, (int)(tileSize * zoomscale), (int)(tileSize * zoomscale));
            pb_imageselector.Invalidate();
        }

        private void pb_imageselector_MouseMove(object sender, MouseEventArgs e)
        {
            mousePos.X = e.X;
            mousePos.Y = e.Y;

            lbl_mousexpos.Text = "X: " + mousePos.X / zoomscale;
            lbl_mouseypos.Text = "Y: " + mousePos.Y / zoomscale;

            pb_imageselector.Invalidate();
        }

        private void pb_imageselector_Paint(object sender, PaintEventArgs e)
        {
            Vector2 vec = new Vector2(mousePos.X.RoundDown((int)(tileSize * zoomscale)), mousePos.Y.RoundDown((int)(tileSize * zoomscale)));
            System.Drawing.Rectangle crect = new System.Drawing.Rectangle((int)vec.X, (int)vec.Y, (int)(tileSize * zoomscale), (int)(tileSize * zoomscale));
            System.Drawing.Rectangle selrect = currentSelectedRect;

            using (Pen pen = new Pen(System.Drawing.Color.Orange, 2))
            {
                e.Graphics.DrawRectangle(pen, crect);
            }

            using (Pen pen = new Pen(System.Drawing.Color.Red, 2))
            {
                e.Graphics.DrawRectangle(pen, selrect);
            }
        }

        private void nud_tilesize_ValueChanged(object sender, EventArgs e)
        {
            tileSize = (int)nud_tilesize.Value;
            pb_imageselector.Invalidate();
        }

        private void pb_imageselector_MouseClick(object sender, MouseEventArgs e)
        {
            Vector2 vec = new Vector2(mousePos.X.RoundDown((int)(tileSize * zoomscale)), mousePos.Y.RoundDown((int)(tileSize * zoomscale)));
            System.Drawing.Rectangle crect = new System.Drawing.Rectangle((int)vec.X, (int)vec.Y, (int)(tileSize * zoomscale), (int)(tileSize * zoomscale));
            currentSelectedRect = crect;

            pb_imageselector.Invalidate();
        }

        public Microsoft.Xna.Framework.Rectangle GetSourceRect()
        {
            return new Microsoft.Xna.Framework.Rectangle((int)(currentSelectedRect.X / zoomscale), (int)(currentSelectedRect.Y / zoomscale), (int)(currentSelectedRect.Width / zoomscale), (int)(currentSelectedRect.Height / zoomscale));
        }

        public Vector2 GetScale()
        {
            return new Vector2(GameWorld.TileSize / tileSize);
        }

        public SpriteEffects GetFlip()
        {
            SpriteEffects fse = SpriteEffects.None;

            if (flipX) fse |= SpriteEffects.FlipHorizontally;
            if (flipY) fse |= SpriteEffects.FlipVertically;

            return fse;
        }

        public TextureInfo GetTexInfo()
        {
            TextureInfo ftexinfo = new TextureInfo(texturename, GetSourceRect(), false);
            ftexinfo.flip = GetFlip();

            return ftexinfo;
        }

        private void btn_paintselected_Click(object sender, EventArgs e)
        {
            main.world.PaintSelected();
        }

        private void cb_flipx_CheckedChanged(object sender, EventArgs e)
        {
            flipX = cb_flipx.Checked;
        }

        private void cb_flipy_CheckedChanged(object sender, EventArgs e)
        {
            flipY = cb_flipy.Checked;
        }
    }
}
