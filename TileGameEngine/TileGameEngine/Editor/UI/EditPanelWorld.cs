﻿using BlueRavenUtility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TileGameEngine.Components.Scripts;

namespace TileGameEngine.Editor.UI
{
    public enum EditorMode
    {
        Selecting,
        Adding
    }

    public enum SelectionMode
    {
        SelectAll,
        SelectTiles,
        SelectNotTiles
    }

    public partial class EditPanelWorld : Form
    {
        private GameMain main;
        public SelectionMode currentSelectionMode;
        //public bool selectTiles;
        public bool firstSelectedOnly;
        public bool selectGroups = true;

        EditableValueList goValueList;

        public EditorMode currentEditingMode;
        public int currentEditingLayer;

        public string currentAddingPreset;

        public int currentGridSize;
        public bool drawGrid = true;
        public bool drawTileMarkers = true;
        public bool drawOtherMarkers = true;
        public bool drawLayerOnly;

        public EditPanelWorld(GameMain main)
        {
            InitializeComponent();
            this.main = main;
            gb_gameobj.Visible = false;
        }

        private void EditPanelWorld_Load(object sender, EventArgs e)
        {
            EditableValueList evl = new EditableValueList(main.world);
            evl.Location = new Point(2, 16);
            gb_world.Controls.Add(evl);

            tb_entityname.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            tb_entityname.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var autoComplete = new AutoCompleteStringCollection();
            List<string> autocompletable = new List<string>();
            GameMain.presets.ToList().ForEach(x => { if (!x.Key.StartsWith("internal_")) autocompletable.Add(x.Key); });    //presets marked with 'internal_' are not shown in autocomplete.
            autoComplete.AddRange(GameMain.presets.Keys.ToArray());
            tb_entityname.AutoCompleteCustomSource = autoComplete;

            rb_select.CheckedChanged += rb_modes_CheckChanged;
            rb_add.CheckedChanged += rb_modes_CheckChanged;
            rb_select.Tag = 0;
            rb_add.Tag = 1;

            nud_gridsize.Value = GameWorld.TileSize;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main.world.Serialize("", true);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main.world.Deserialize("", true);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main.world.ClearGameObjects();
        }


        private void rb_modes_CheckChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;

            if (button.Checked)
            {
                int buttonid = (int)button.Tag;
                currentEditingMode = (EditorMode)buttonid;
            }
        }

        public void AddSelectables(List<GameObject> selectables)
        {
            if (goValueList != null)
                goValueList.Dispose();  //we need to get rid of the original instance before we can start a new one.

            gb_gameobj.Visible = true;
            goValueList = new EditableValueList(selectables.ConvertAll(x => { return x as IEditable; }));
            //goValueList.Location = new Point(2, 16);
            List<IEditable> collidables = new List<IEditable>();
            List<IEditable> renderers = new List<IEditable>();
            List<Script> scripts = new List<Script>();

            foreach (GameObject go in selectables)
            {
                if (go.collider != null)
                    collidables.Add(go.collider as IEditable);
                if (go.renderer != null)
                    renderers.Add(go.renderer as IEditable);
                if (go.scripts.Count > 0)
                    scripts.AddRange(go.scripts);
            }

            tc_gameobjects.TabPages.Clear();

            tc_gameobjects.TabPages.Add("GameObject");
            tc_gameobjects.TabPages[0].Controls.Add(goValueList);   //gameobjects tab

            tc_gameobjects.TabPages.Add("Collider");
            EditableValueList colliderValueList = new EditableValueList(collidables);
            tc_gameobjects.TabPages[1].Controls.Add(colliderValueList);   //collider tab

            tc_gameobjects.TabPages.Add("Renderer");
            EditableValueList rendererValueList = new EditableValueList(renderers);
            tc_gameobjects.TabPages[2].Controls.Add(rendererValueList);   //renderer tab

            tc_gameobjects.TabPages.Add("Scripts");
            TabControl tc_gameobjects_scripts = new TabControl();
            tc_gameobjects_scripts.Size = new Size(380, 471);
            tc_gameobjects.TabPages[3].Controls.Add(tc_gameobjects_scripts);

            Dictionary<string, List<IEditable>> dicted = new Dictionary<string, List<IEditable>>();
            foreach (Script s in scripts)
            {
                string type = s.GetType().ToString().Split('.').Last();
                if (!dicted.ContainsKey(type))
                    dicted.Add(type, new List<IEditable>());
                dicted[type].Add(s);
            }

            foreach (KeyValuePair<string, List<IEditable>> d in dicted)
            {
                tc_gameobjects_scripts.TabPages.Add(d.Key);
                tc_gameobjects_scripts.TabPages[tc_gameobjects_scripts.TabCount - 1].Controls.Add(new EditableValueList(d.Value));  //scripts tab
            }
        }

        public void Deselect()
        {
            if (goValueList != null)
                goValueList.Dispose();
            gb_gameobj.Visible = false;
            tc_gameobjects.TabPages.Clear();
        }

        private void nudLayerSelector_ValueChanged(object sender, EventArgs e)
        {
            if (nud_layerselector.Value > GameWorld.maxLayers)
                nud_layerselector.Value = GameWorld.maxLayers;

            currentEditingLayer = (int)nud_layerselector.Value;
        }

        private void tc_gameobjects_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tb_entityname_TextChanged(object sender, EventArgs e)
        {
            currentAddingPreset = tb_entityname.Text;
        }

        #region grid
        private void nud_gridsize_ValueChanged(object sender, EventArgs e)
        {
            currentGridSize = (int)nud_gridsize.Value;
        }

        public void UpdateGridSize(int gridsize)
        {
            currentGridSize = gridsize;
            nud_gridsize.Value = gridsize;
        }

        private void cb_showgrid_CheckedChanged(object sender, EventArgs e)
        {
            drawGrid = cb_drawgrid.Checked;
        }
        #endregion

        #region prefab
        private void savePrefabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main.world.SaveSelectedAsPrefab();
        }

        private void loadPrefabToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            main.world.LoadFromPrefab();
        }
        #endregion

        #region delete
        private void btn_delalloftype_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure? This will delete EVERYTHING of this type.", "Confirm", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)
            {
                main.world.DeleteAllOfType(tb_entityname.Text);
            }
        }

        private void allTilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure? This will delete EVERYTHING of this type.", "Confirm", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)
            {
                main.world.DeleteAllTiles();
            }
        }

        private void allNotTilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure? This will delete EVERYTHING of this type.", "Confirm", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)
            {
                main.world.DeleteAllNotTiles();
            }
        }
        #endregion

        public void ChangeMode(EditorMode mode)
        {
            if ((int)rb_add.Tag == (int)mode)
                rb_add.Checked = true;
            else if ((int)rb_select.Tag == (int)mode)
                rb_select.Checked = true;
        }

        #region selection modes
        private void cb_selectgroups_CheckedChanged(object sender, EventArgs e)
        {
            selectGroups = cb_selectgroups.Checked;
        }

        private void rb_selectmode_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;

            if (button.Checked)
            {
                int buttonid = int.Parse(button.Tag.ToString());
                currentSelectionMode = (SelectionMode)buttonid;
            }
        }

        private void cb_firstonly_CheckedChanged(object sender, EventArgs e)
        {
            firstSelectedOnly = cb_firstonly.Checked;
        }
        #endregion

        #region flip/rotate
        private void btn_rotc_Click(object sender, EventArgs e)
        {
            main.world.RotateSelected(Enums.DirectionClock.Clockwise);
        }

        private void btn_rotcc_Click(object sender, EventArgs e)
        {
            main.world.RotateSelected(Enums.DirectionClock.CounterClockwise);
        }

        private void btn_fliphoriz_Click(object sender, EventArgs e)
        {
            main.world.FlipSelected(Enums.DirectionMirror.Horizontal);
        }

        private void btn_flipvert_Click(object sender, EventArgs e)
        {
            main.world.FlipSelected(Enums.DirectionMirror.Vertical);
        }
        #endregion

        private void btn_selectimage_Click(object sender, EventArgs e)
        {
            GameMain.textureSelector.Visible = true;
        }

        private void cb_drawtilemarkers_CheckedChanged(object sender, EventArgs e)
        {
            drawTileMarkers = cb_drawtilemarkers.Checked;
        }

        private void cb_drawothermarkers_CheckedChanged(object sender, EventArgs e)
        {
            drawOtherMarkers = cb_drawothermarkers.Checked;
        }

        private void cb_drawlayeronly_CheckedChanged(object sender, EventArgs e)
        {
            drawLayerOnly = cb_drawlayeronly.Checked;
        }
    }
}
