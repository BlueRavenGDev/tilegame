﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Windows.Forms;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using BlueRavenUtility;
using TileGameEngine.Components.Colliders;
using TileGameEngine.Components.Renderers;
using TileGameEngine.Components.Scripts;
using TileGameEngine.Assets;
using TileGameEngine.Editor;
using System.Threading;
using TileGameEngine.Components;
using TileGameEngine.Components.CollidersR;
using TileGameEngine.Editor.UI;

namespace TileGameEngine
{
    public abstract class GameWorld : IEditable
    {
        public const int MapVersion = 1, PrefabVersion = 1;

        public static int TileSize = 40;
        public static int maxTilesX = 1024, maxTilesY = 1024;
        public static int maxLayers = 4;

        public static WorldAssets assets;

        public List<GameObject> gameObjects;

        public List<ColliderDynamic> dynamics;
        public List<ColliderStaticTile> statics;
        public List<ColliderStaticTile> hybrids;

        public List<Collider> colliders;
        public List<Collider> hybridColliders;
        public List<Renderer> renderers;
        public List<List<Script>> updateScripts;

        public List<GameObject[,]> tiles;

        public ColliderHandlerR colliderHandlerR;

        public ColliderHandler colliderHandler;
        public RendererHandler rendererHandler;
        public ScriptHandler scriptHandler;

        public ActivityHandler activityHandler;

        public float time;
        public static float prevTimescale;
        private static float _timescale;
        public static float timescale {
            get { return _timescale; }
            set { prevTimescale = _timescale; _timescale = value; }
        }
        private float oldtimescale;

        public static bool DEBUGgrid;

        public Color bgColor;

        public List<GameObject> selectedObjects;
        public List<Vector2> selectPoints;

        public List<GameObject> copy;
        private List<List<GameObject>> groups;

        private bool selecting;

        private Vector2 cameraPos;

        public bool noUpdate;

        public static Thread saveloadThread;

        public GameWorld()
        {
            gameObjects = new List<GameObject>();
            tiles = new List<GameObject[,]>();

            for (int i = 0; i < maxLayers; i++)
                tiles.Add(new GameObject[maxTilesX, maxTilesY]);

            dynamics = new List<ColliderDynamic>();
            statics = new List<ColliderStaticTile>();
            hybrids = new List<ColliderStaticTile>();

            colliders = new List<Collider>();
            hybridColliders = new List<Collider>();
            renderers = new List<Renderer>();
            updateScripts = new List<List<Script>>();

            colliderHandlerR = new ColliderHandlerR();

            colliderHandler = new ColliderHandler();
            scriptHandler = new ScriptHandler();
            rendererHandler = new RendererHandler();

            activityHandler = new ActivityHandler();

            addInstanceFunctions.Add("TileGameEngine", str => { return Activator.CreateInstance(Type.GetType(str)); });

            values = new Dictionary<string, EditableValue>();

            selectedObjects = new List<GameObject>();
            selectPoints = new List<Vector2>();

            copy = new List<GameObject>();

            groups = new List<List<GameObject>>();

        }

        public void Initialize()
        {
            LoadWorld();

            Camera.camera.clamp = new Rectangle(0, 0, maxTilesX * TileSize, maxTilesY * TileSize);
        }

        public void LoadContent(ContentManager manager, string fulldirectoryname, GraphicsDevice device)
        {
            assets = new WorldAssets();

            if (!GlobalAssets.exists)
                throw new Exception("Global Assets object does not exist! A Global Assets object must be created before world assets are loaded.");
            assets.Load(new ContentManager(manager.ServiceProvider, manager.RootDirectory), fulldirectoryname, null);
        }

        public virtual void Update()
        {
            cameraPos = Vector2.Clamp(cameraPos, Camera.camera.clamp.Location.ToVector2(), Camera.camera.clamp.Location.ToVector2() + Camera.camera.clamp.Size.ToVector2());

            if (saveloadThread != null && saveloadThread.IsAlive)
                noUpdate = true;
            else if (saveloadThread != null && !saveloadThread.IsAlive) noUpdate = false;

            if (Camera.camera.hasMoved)
                activityHandler.anyChange = true;

            activityHandler.CheckActive(this);

            if (!noUpdate)
            {
                time += 1 * timescale;

                colliderHandlerR.Update(this);

                colliderHandler.Update(this);

                scriptHandler.Update(this);
            }
            if (GameMain.editing)
                EditUpdate();

            gameObjects.CheckAndDelete(x => { return x.dead; });
        }

        public void EditUpdate()
        {
            if (GameMain.editing)
            {
                if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.F1))
                    GameMain.editPanel.ChangeMode(EditorMode.Selecting);
                else if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.F2))
                    GameMain.editPanel.ChangeMode(EditorMode.Adding);

                float spd = GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.LeftShift) ? 16 : 8;
                if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.W))
                    cameraPos.Y -= spd;
                if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.A))
                    cameraPos.X -= spd;
                if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.S))
                    cameraPos.Y += spd;
                if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.D))
                    cameraPos.X += spd;

                Camera.camera.target = cameraPos;
            }

            if (GameMain.editPanel.currentEditingMode == EditorMode.Selecting && GameMain.mouse.LeftButtonPressed())
            {
                if (selectPoints.Count < 2)
                {
                    selectPoints.Add(GameMain.mouse.GetWorldPosition());
                }
                else if (selectPoints.Count == 2)
                {
                    if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.LeftControl))
                        selectPoints[0] = GameMain.mouse.GetWorldPosition();
                    else selectPoints[1] = GameMain.mouse.GetWorldPosition();
                }

                if (selectPoints.Count > 0)
                    selecting = true;
            }
            else if (GameMain.editPanel.currentEditingMode == EditorMode.Adding && GameMain.editPanel.currentAddingPreset != "" && GameMain.mouse.LeftButtonPressed())
            {
                EditAddGameObject();
            }

            if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                selectPoints.Clear();
                ClearSelected();

                selecting = false;
            }

            if (selecting)
            {
                if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Enter))
                {
                    if (selectPoints.Count == 2)
                    {
                        ClearSelected();    //clear selected objects before we can re-select

                        if (GameMain.editPanel.currentSelectionMode == Editor.UI.SelectionMode.SelectAll)
                        {
                            selectedObjects = FindAllObjectsIn(new RectangleF(selectPoints[0], selectPoints[1] - selectPoints[0]),
                                GameMain.editPanel.currentEditingLayer,
                                true,
                                x => { return x.selectable; }, GameMain.editPanel.firstSelectedOnly);
                            selectedObjects.AddRange(FindAllObjectsIn(new RectangleF(selectPoints[0], selectPoints[1] - selectPoints[0]),
                                GameMain.editPanel.currentEditingLayer,
                                false,
                                x => { return x.selectable; }, GameMain.editPanel.firstSelectedOnly));
                            selectedObjects = selectedObjects.Distinct().ToList();   //in case of duplicates
                            //we need to select both tiles and not tiles, so use FindAllObjects 2 times, one not selecting tiles, the other selecting tiles.
                        }
                        else
                        {
                            selectedObjects = FindAllObjectsIn(new RectangleF(selectPoints[0], selectPoints[1] - selectPoints[0]),
                                GameMain.editPanel.currentEditingLayer,
                                GameMain.editPanel.currentSelectionMode == Editor.UI.SelectionMode.SelectTiles,
                                x => { return x.selectable; }, GameMain.editPanel.firstSelectedOnly);
                        }


                        if (GameMain.editPanel.selectGroups)
                        {
                            selectedObjects.ToList().ForEach(selobj =>
                            {
                                groups.ForEach(group =>
                                {
                                    if (GameMain.editPanel.currentSelectionMode == Editor.UI.SelectionMode.SelectAll)
                                        selectedObjects.AddRange(group);
                                    else
                                    {
                                        group.ForEach(groupobj =>
                                        {
                                            if (GameMain.editPanel.currentSelectionMode == Editor.UI.SelectionMode.SelectTiles)
                                            {
                                                if (groupobj.tile)
                                                    selectedObjects.Add(groupobj);
                                            }
                                            else if (GameMain.editPanel.currentSelectionMode == Editor.UI.SelectionMode.SelectNotTiles)
                                            {   //this could be an else but for the sake of readability we're using an else if.
                                            if (!groupobj.tile)
                                                    selectedObjects.Add(groupobj);
                                            }
                                        });
                                    }
                                });
                            });
                        }

                        AddSelected();
                        selecting = false;

                        selectPoints.Clear();
                    }
                }
            }

            if (selectedObjects.Count > 0)
            {
                SelectedUpdate();
            }

            //pasting can be done without anything selected, obviously.
            if (GameMain.keyboard.KeyModifierPressed(Microsoft.Xna.Framework.Input.Keys.V, Microsoft.Xna.Framework.Input.Keys.LeftControl))
            {
                if (copy.Count > 0)
                {
                    int index = FindTopLeftest(copy);

                    GameObject topleftest = copy[index];

                    Vector2 prevPos = topleftest.transform.GetPositionInWorldSpace(true);

                    topleftest.transform.SetPositionFromWorldSpace(GameMain.mouse.GetWorldPosition());

                    Vector2 dist = topleftest.transform.GetPositionInWorldSpace(true) - prevPos;

                    for (int i = 0; i < copy.Count; i++)
                    {
                        if (i != index)
                        {
                            Vector2 distf = Vector2.Zero;

                            if (topleftest.tile && copy[i].tile)
                                distf = dist;   //both are tiles
                            else if (topleftest.tile && !copy[i].tile)
                                distf = dist * TileSize;    //the copy we're moving relative is a tile, and the first is a tile
                            else if (!topleftest.tile && copy[i].tile)
                                distf = (dist / TileSize).ToPoint().ToVector2();    //the first is NOT a tile, but the copy we're moving is
                            else if (!topleftest.tile && !copy[i].tile)
                                distf = dist;               //both are not tiles

                            copy[i].transform.SetPositionFromWorldSpace(copy[i].transform.GetPositionInWorldSpace(true) + distf);
                        }

                        SnapToGrid(copy[i]);
                    }

                    ClearSelected();
                    selectedObjects = copy.ToList();
                    AddSelected();

                    copy.ForEach(x => AddGameObject(x, x.tile));

                    List<GameObject> c2 = new List<GameObject>();
                    copy.ForEach(x => c2.Add((GameObject)x.Clone()));   //so we have fresh copies
                    copy.Clear();
                    copy = c2;
                }
            }
        }

        public void SelectedUpdate()
        {
            if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Delete) || GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Back))
            {
                selectedObjects.ForEach(x =>
                {
                    if (x.tile)
                    {
                        x.parentWorld.DeleteTile(x.transform.layer, x.transform.position.X, x.transform.position.Y);
                    }
                    x.Destroy(true);
                });
                selectedObjects.Clear();
            }

            bool moving = false;
            Vector2 moveDirec = Vector2.Zero;

            if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Left))
            {
                moving = true;
                moveDirec = new Vector2(-GameMain.editPanel.currentGridSize, 0);
            }
            else if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                moving = true;
                moveDirec = new Vector2(0, -GameMain.editPanel.currentGridSize);
            }
            else if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Right))
            {
                moving = true;
                moveDirec = new Vector2(GameMain.editPanel.currentGridSize, 0);
            }
            else if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                moving = true;
                moveDirec = new Vector2(0, GameMain.editPanel.currentGridSize);
            }

            if (moving)
            {
                try
                {
                    selectedObjects.ForEach(x =>
                    {
                        Vector2 prevPos = x.transform.position;

                        x.transform.SetPositionFromWorldSpace(moveDirec, true);
                        //x.transform.position += x.tile ? (moveDirec / TileSize) : moveDirec;

                        if (x.tile)
                        {
                            GameObject otherTile = tiles[x.transform.layer][(int)x.transform.position.X, (int)x.transform.position.Y];
                            if (otherTile != null && !selectedObjects.Contains(otherTile))  //if not null and does not contain othertile
                                tiles[x.transform.layer][(int)x.transform.position.X, (int)x.transform.position.Y].Destroy(true);
                            tiles[x.transform.layer][(int)x.transform.position.X, (int)x.transform.position.Y] = x;
                            //if the tile is in the selected tiles array, don't destroy or delete it, as it'll also be moved.

                            otherTile = tiles[x.transform.layer][(int)prevPos.X, (int)prevPos.Y];

                            if (otherTile != null && otherTile.transform.position != prevPos)
                            {
                                DeleteTile(x.transform.layer, prevPos.X, prevPos.Y);
                            }
                        }
                    });
                    SnapToGrid(selectedObjects);
                }
                catch (Exception e) { }
            }

            if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.L))
                FlipSelected(Enums.DirectionMirror.Horizontal);
            if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.I))
                FlipSelected(Enums.DirectionMirror.Vertical);

            if (GameMain.keyboard.KeyModifierPressed(Microsoft.Xna.Framework.Input.Keys.C, Microsoft.Xna.Framework.Input.Keys.LeftControl))
            {
                copy.Clear();
                selectedObjects.ForEach(x => { copy.Add((GameObject)x.Clone()); });
            }

            if (GameMain.keyboard.KeyModifierPressed(Microsoft.Xna.Framework.Input.Keys.G, Microsoft.Xna.Framework.Input.Keys.LeftControl))
            {   //grouping
                groups.Add(selectedObjects.ToList());
            }

            if (GameMain.keyboard.KeyModifierPressed(Microsoft.Xna.Framework.Input.Keys.U, Microsoft.Xna.Framework.Input.Keys.LeftControl))
            {   //ungrouping
                selectedObjects.ForEach(selobj =>
                {
                    int groupindex = 0;
                    foreach (List<GameObject> group in groups.ToList())
                    {   //remove the whole list (group) that contains this gameobject
                        groupindex++;
                        if (group.Contains(selobj))
                        {
                            groups.RemoveAt(groupindex);
                        }
                    }
                });
            }
        }

        public void EditAddGameObject()
        {
            Vector2 pos = GameMain.mouse.GetWorldPosition();

            if (GameMain.editPanel.drawGrid)
                pos = new Vector2(pos.X.RoundDown(GameMain.editPanel.currentGridSize), pos.Y.RoundDown(GameMain.editPanel.currentGridSize));

            GameObject go = GameMain.CloneFromPreset(GameMain.editPanel.currentAddingPreset, pos, true);
            if (go != null)
            {
                go.transform.layer = GameMain.editPanel.currentEditingLayer;
                AddGameObject(go, go.tile);
            }

            ClearSelected();

            selectedObjects.Add(go);
            AddSelected();
        }

        public virtual void LoadWorld()
        {

        }

        /// <summary>
        /// Adds a GameObject to the list of gameobjects.
        /// Also adds a collider, renderer, and list of scripts, if they exist, to their respective lists.
        /// NOTE: DO NOT USE if you have already set the gameobject's collider,renderer, and scripts in itsself. use <see cref="AddGameObject(GameObject)"/>.
        /// </summary>
        /// <returns>The reference to the Input gameobject.</returns>
        public virtual GameObject AddGameObject(GameObject gameObject, Collider collider, Renderer renderer, List<Script> scripts)
        {
            rendererHandler.AddRenderer(this, renderer);
            colliderHandler.AddCollider(this, collider);
            updateScripts.Add(scripts);

            if (gameObject.tile)
            {
                if (tiles[gameObject.transform.layer][(int)gameObject.transform.position.X, (int)gameObject.transform.position.Y] != null)
                    tiles[gameObject.transform.layer][(int)gameObject.transform.position.X, (int)gameObject.transform.position.Y].Destroy(true);

                tiles[gameObject.transform.layer][(int)gameObject.transform.position.X, (int)gameObject.transform.position.Y] = gameObject;
            }

            gameObject.collider = collider;
            gameObject.renderer = renderer;
            gameObject.scripts = scripts;

            if (gameObject.renderer == null && gameObject.isUsed && gameObject.serializable)
            {
                gameObject.renderer = new RendererDebug(gameObject);
                rendererHandler.AddRenderer(this, gameObject.renderer, false);
            }

            gameObjects.Add(gameObject);

            gameObject.isAdded = true;

            if (!gameObject.parent.isAdded)
            {
                AddGameObject(gameObject.parent);
            }

            return gameObject;
        }

        /// <summary>
        /// Adds a GameObject to the list of gameobjects.
        /// Also adds the gameobject's collider, renderer, and list of scripts, if they exist, to their respective lists.
        /// </summary>
        /// <returns>The Input gameobject.</returns>
        public virtual GameObject AddGameObject(GameObject gameObject, bool asTile = false)
        {
            if (gameObject.colliderStatic != null)
            {
                if (gameObject.colliderStatic.hybrid)
                    hybrids.Add(gameObject.colliderStatic);
                else statics.Add(gameObject.colliderStatic);
                gameObject.colliderStatic.owner = gameObject;

            }

            if (gameObject.colliderDynamic != null)
            {
                dynamics.Add(gameObject.colliderDynamic);
                gameObject.colliderDynamic.owner = gameObject;
            }

            if (gameObject.collider != null)
            {
                colliderHandler.AddCollider(this, gameObject.collider);
                gameObject.collider.owner = gameObject;
            }
            if (gameObject.renderer != null)
            {
                rendererHandler.AddRenderer(this, gameObject.renderer);
                gameObject.renderer.owner = gameObject;
            }
            if (gameObject.scripts != null && gameObject.scripts.Count > 0)
            {
                updateScripts.Add(gameObject.scripts);
                gameObject.scripts.ForEach(x => x.owner = gameObject);
            }

            if (gameObject.renderer == null && gameObject.isUsed && gameObject.serializable)
            {
                gameObject.renderer = new RendererDebug(gameObject);
                rendererHandler.AddRenderer(this, gameObject.renderer, false);
            }

            if (gameObject.transform == null)
                return gameObject;
            gameObject.transform.owner = gameObject;
            if (asTile)
            {
                if (gameObject.transform.position.X < 0)
                    gameObject.transform.position.X = 0;
                if (gameObject.transform.position.Y < 0)
                    gameObject.transform.position.Y = 0;

                GameObject tile = tiles[gameObject.transform.layer][(int)gameObject.transform.position.X, (int)gameObject.transform.position.Y];
                if (tile != null)
                    tile.Destroy(true);

                //Console.WriteLine(tiles[gameObject.transform.layer][(int)gameObject.transform.position.X, (int)gameObject.transform.position.Y]);
                tiles[gameObject.transform.layer][(int)gameObject.transform.position.X, (int)gameObject.transform.position.Y] = gameObject;
            }

            gameObjects.Add(gameObject);

            gameObject.isAdded = true;

            if (gameObject.parent != null && !gameObject.parent.isAdded)
            {
                AddGameObject(gameObject.parent);
            }

            return gameObject;
        }

        #region Check Tile Validity
        public bool IsValidTile(Vector2 position, int layer)
        {
            int x = (int)position.X;
            int y = (int)position.Y;

            return ValidTile(x, y, layer);
        }

        public bool IsValidTile(float x, float y, int layer)
        {
            int x1 = (int)x;
            int y1 = (int)y;

            return ValidTile(x1, y1, layer);
        }

        public bool IsValidTile(int x, int y, int layer)
        {
            return ValidTile(x, y, layer);
        }

        private bool ValidTile(int x, int y, int layer)
        {
            if ((y < 0 || y >= maxTilesY) || (x < 0 || x >= maxTilesX))
                return false;
            return tiles[layer][x, y] != null && layer < maxLayers && tiles[layer][x, y].tile;
        }
        #endregion

        public void Draw(SpriteBatch batch)
        {
            batch.GraphicsDevice.Clear(bgColor);

            rendererHandler.Render(this, batch);

            if (DEBUGgrid || (GameMain.editPanel.drawGrid && GameMain.editing))
            {
                Vector2 mousePos = GameMain.mouse.GetWorldPosition();
                mousePos.X = mousePos.X.RoundDown(TileSize);
                mousePos.Y = mousePos.Y.RoundDown(TileSize);

                int gridsize = GameWorld.TileSize;
                if (GameMain.editing)
                    gridsize = GameMain.editPanel.currentGridSize;
                for (int i = 0; i < maxTilesY; i++)
                {
                    batch.DrawLine(new Vector2(i * gridsize, 0),
                        new Vector2(i * gridsize, maxTilesY * gridsize),
                        Color.Black);
                }

                for (int i = 0; i < maxTilesX; i++)
                {
                    batch.DrawLine(new Vector2(0, i * gridsize),
                        new Vector2(maxTilesX * gridsize, i * gridsize),
                        Color.Black);
                }

                batch.DrawRectangle(new RectangleF(mousePos, TileSize, TileSize), new Color(Color.Red, 63));

                batch.DrawString(assets.fonts.GetAsset("bitfontMunro8"), GameMain.mouse.GetWorldPosition().ToString() + ", " + (mousePos / TileSize).ToString(), mousePos - new Vector2(0, 14), Color.White);
            }

            selectPoints.ForEach(x => 
            {
                batch.DrawHollowCircle(x, 9, Color.Black, 4);
                batch.DrawHollowCircle(x, 8, Color.Red);
            });
            if (selectPoints.Count == 2)
            {
                RectangleF rect = new RectangleF(selectPoints[0], selectPoints[1] - selectPoints[0]);
                RectangleF rectb = rect;
                rectb.Expand(1);
                batch.DrawHollowRectangle(rectb, 6, Color.Black);
                batch.DrawHollowRectangle(rect, 4, Color.Red);
            }

            if (selectedObjects.Count > 0)
            {
                RectangleF rect = GetSelectedContainmentRect();
                RectangleF rect1 = rect, rect2 = rect;
                batch.DrawHollowRectangle(rect1.Expand(1, 1), 1, Color.Black);
                batch.DrawHollowRectangle(rect2.Expand(-2, -2), 1, Color.Black);
                batch.DrawHollowRectangle(rect, 2, Color.Orange);
            }

            batch.DrawLine(Vector2.Zero, VectorHelper.GetAngleNormVector(45) * 128, Color.Black);
        }

        #region serializing
        public void Serialize(string saveAs, bool manualSelectFile=false)
        {
            string fsaveAs = saveAs.EndsWith(".tgmf") ? saveAs : saveAs + ".tgmf";

            #region Autoload
            if (!manualSelectFile)
            {
                using (FileStream stream = new FileStream(fsaveAs, FileMode.Create))
                {
                    BinaryWriter writer = new BinaryWriter(stream);

                    writer.Write((int)0);   //first byte is empty (0)

                    writer.Write(MapVersion);

                    writer.Write(bgColor.R);
                    writer.Write(bgColor.G);
                    writer.Write(bgColor.B);
                    writer.Write(bgColor.A);

                    writer.Write(maxLayers);

                    writer.Write(gameObjects.FindAll(x => x.serializable).Count);    //write the size of the list
                    gameObjects.ForEach(x =>
                    {
                        if (x.serializable)
                            x.Serialize(writer);
                    });
                }
            }
            #endregion
            #region Manualload
            else
            {
                Stream mystream;
                SaveFileDialog dialog = new SaveFileDialog();

                dialog.InitialDirectory = "C:\\Users\\taylo\\Documents\\Programming\\C#\\Game\\TileGame\\Maps";
                dialog.Filter = "Tile Game Map Format files (*.tgmf)|*.tgmf";
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if ((mystream = dialog.OpenFile()) != null)
                    {
                        BinaryWriter writer = new BinaryWriter(mystream);

                        writer.Write(false);        //first byte is empty (0)
                        writer.Write(MapVersion);   //mapversion

                        writer.Write(bgColor.R);    //rgba
                        writer.Write(bgColor.G);
                        writer.Write(bgColor.B);
                        writer.Write(bgColor.A);

                        writer.Write(maxLayers);    //layers

                        writer.Write(gameObjects.FindAll(x => x.serializable).Count);    //write the size of the list

                        gameObjects.ForEach(x =>
                        {
                            if (x.serializable)
                                x.Serialize(writer);
                        });

                        writer.Close();

                        mystream.Close();   //since no using, we have to close manually. Don't want it staying open.
                    }
                }
            }
            #endregion
        }

        public void Deserialize(string loadFrom, bool manualSelectFile = false)
        {
            string fsaveAs = loadFrom.EndsWith(".tgmf") ? loadFrom : loadFrom + ".tgmf";
            try
            {
                #region Autoload
                if (!manualSelectFile)
                {
                    using (FileStream stream = new FileStream(loadFrom, FileMode.Open))
                    {
                        BinaryReader reader = new BinaryReader(stream);

                        if (reader.ReadInt32() != 0)
                            throw new Exception("Tried to load a corrupted map file.");

                        if (reader.ReadInt32() != MapVersion)
                            throw new Exception("Tried to load a map of an incompatable map version.");
                        
                        bgColor.R = reader.ReadByte();
                        bgColor.G = reader.ReadByte();
                        bgColor.B = reader.ReadByte();
                        bgColor.A = reader.ReadByte();

                        maxLayers = reader.ReadInt32();

                        ClearGameObjects();

                        int count = reader.ReadInt32();

                        for (int i = 0; i < count; i++)
                        {
                            gameObjects.Add(new GameObject(this));
                        }

                        gameObjects.ForEach(go => go.Deserialize(reader));

                        reader.Close();
                    }
                }
                #endregion
                #region Manualload
                else
                {
                    Stream mystream;
                    OpenFileDialog dialog = new OpenFileDialog();

                    dialog.InitialDirectory = "C:\\Users\\taylo\\Documents\\Programming\\C#\\Game\\TileGame\\Maps";
                    dialog.Filter = "Tile Game Map Format files (*.tgmf)|*.tgmf";
                    dialog.RestoreDirectory = true;

                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        if ((mystream = dialog.OpenFile()) != null)
                        {
                            BinaryReader reader = new BinaryReader(mystream);

                            if (reader.ReadBoolean())    //first byte is 0. if not, corrupted.
                                throw new FormatException("Tried to load a corrupted map file.");

                            if (reader.ReadInt32() != MapVersion)   //map version
                                throw new FormatException("Tried to load a map of an incompatable map version.");

                            bgColor.R = reader.ReadByte();          //rgba
                            bgColor.G = reader.ReadByte();
                            bgColor.B = reader.ReadByte();
                            bgColor.A = reader.ReadByte();

                            maxLayers = reader.ReadInt32();         //maxlayers

                            ClearGameObjects();

                            int count = reader.ReadInt32();         //count

                            for (int i = 0; i < count; i++)
                            {
                                gameObjects.Add(new GameObject(this));
                            }

                            gameObjects.ForEach(go => {
                                go.Deserialize(reader);
                            });

                            reader.Close();
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                GameMain.console.Output.Append(e.ToString());
            }
        }

        public void SaveSelectedAsPrefab()
        {
            if (selectedObjects.Count > 0)
            {
                Stream mystream;
                SaveFileDialog dialog = new SaveFileDialog();

                dialog.InitialDirectory = "C:\\Users\\taylo\\Documents\\Programming\\C#\\Game\\TileGame\\Prefabs";
                dialog.Filter = "Tile Game Prefab Format files (*.tgpf)|*.tgpf";
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if ((mystream = dialog.OpenFile()) != null)
                    {
                        BinaryWriter writer = new BinaryWriter(mystream);

                        writer.Write((int)0);

                        writer.Write(PrefabVersion);

                        writer.Write(selectedObjects.FindAll(x => x.serializable).Count);    //write the size of the list

                        selectedObjects.ForEach(x =>
                        {
                            if (x.serializable)
                            {
                                x.tags.Add("prefab_" + dialog.FileName);
                                x.Serialize(writer);
                            }
                        });

                        writer.Close();

                        mystream.Close();   //since no using, we have to close manually. Don't want it staying open.
                    }
                }
            }
            else GameMain.console.Output.Append("Cannot save prefab - no objects currently selected!");
        }

        public void LoadFromPrefab()
        {
            Stream mystream;
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.InitialDirectory = "C:\\Users\\taylo\\Documents\\Programming\\C#\\Game\\TileGame\\Prefabs";
            dialog.Filter = "Tile Game Prefab Format files (*.tgpf)|*.tgpf";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if ((mystream = dialog.OpenFile()) != null)
                {
                    BinaryReader reader = new BinaryReader(mystream);

                    ClearSelected();
                    copy.Clear();

                    if (reader.ReadInt32() != 0)
                        throw new Exception("Tried to load a corrupted prefab file.");

                    if (reader.ReadInt32() != PrefabVersion)
                        throw new Exception("Tried to load a prefab of an incompatable prefab version.");

                    int count = reader.ReadInt32();

                    for (int i = 0; i < count; i++)
                    {
                        GameObject go = new GameObject(this);
                        //gameObjects.Add(go);
                        copy.Add(go);
                    }

                    copy.ForEach(go => go.Deserialize(reader, false));

                    reader.Close();
                }
            }
        }
        #endregion

        #region delete
        public void DeleteAllOfType(string type)
        {
            gameObjects.ForEach(x => { if (x.tags.Contains(type)) x.Destroy(true); });
        }

        public void DeleteAllTiles()
        {
            gameObjects.ForEach(x => { if (x.tile) x.Destroy(true); } );
        }
        
        public void DeleteAllNotTiles()
        {
            gameObjects.ForEach(x => { if (!x.tile) x.Destroy(true); });
        }

        public void DeleteTile(float layer, float x, float y, bool destroy = false)
        {
            DeleteTile((int)layer, (int)x, (int)y, destroy);
        }

        public void DeleteTile(int layer, int x, int y, bool destroy = false)
        {
            if (!destroy)
            {
                GameObject replObj = new GameObject(this);
                replObj.serializable = false;
                replObj.selectable = false;

                tiles[layer][x, y] = replObj;
            }
            else tiles[layer][x, y].Destroy(true);
        }
        #endregion

        public List<GameObject> FindAllObjectsIn(RectangleF rect, int layer, bool tile, Func<GameObject, bool> whitelist, bool firstonly = false)
        {
            List<GameObject> gos = new List<GameObject>();

            if (!tile)
            {
                foreach (GameObject go in gameObjects)
                {
                    try
                    {
                        if (!whitelist.Invoke(go))
                            continue;
                    }
                    catch { }

                    if (go.transform != null && rect.Contains(go.transform.GetPositionInWorldSpace(true)) && go.transform.layer == layer)
                    {
                        gos.Add(go);
                        if (firstonly)
                            return gos;
                    }
                }
            }
            else
            {
                int startx = ((int)rect.x + TileSize) / TileSize;
                int endx = ((int)(rect.x + rect.width) + TileSize) / TileSize;
                int starty = ((int)rect.y + TileSize) / TileSize;
                int endy = ((int)(rect.y + rect.height) + TileSize) / TileSize;

                for (int x = startx; x < endx; x++)
                {
                    for (int y = starty; y < endy; y++)
                    {
                        if (IsValidTile(x, y, layer))
                        {
                            try
                            {
                                if (!whitelist.Invoke(tiles[layer][x, y]))
                                    continue;
                            }
                            catch { }   //NOOP on catch; simply there to see if the whitelist exists. probably bad idea.

                            gos.Add(tiles[layer][x, y]);
                            if (firstonly)
                                return gos;
                        }
                    }
                }
            }
            return gos;
        }

        internal void ClearGameObjects()
        {
            gameObjects.Clear();
            colliders.Clear();
            hybridColliders.Clear();
            renderers.Clear();
            updateScripts.Clear();

            dynamics.Clear();
            statics.Clear();
            hybrids.Clear();

            tiles.Clear();

            for (int i = 0; i < maxLayers; i++)
                tiles.Add(new GameObject[maxTilesX, maxTilesY]);
        }

        #region selected
        private void ClearSelected()
        {
            GameMain.editPanel.Deselect();

            selectedObjects.ForEach(x => x.selected = false);
            gameObjects.ForEach(x => x.selected = false);
            selectedObjects.Clear();
        }

        /// <summary>
        /// Adds the selected gameobjects in <see cref="selectedObjects"/> to the edit panel, as well as setting them to be selected.
        /// </summary>
        private void AddSelected()
        {
            selectedObjects.ForEach(x => { if (x.selectable) x.selected = true; });
            GameMain.editPanel.AddSelectables(selectedObjects);
        }

        public void RotateSelected(Enums.DirectionClock direction)
        {
            Vector2 center = GetCenterOfSelection();    //note: world space, not tile space

            foreach (GameObject go in selectedObjects)
            {
                Vector2 prevPos = go.transform.GetPositionInWorldSpace(true);

                Vector2 translated = go.transform.GetPositionInWorldSpace() - center; //translate to origin

                if (direction == Enums.DirectionClock.Clockwise)
                {
                    translated = VectorHelper.GetPerp(translated);
                }
                else if (direction == Enums.DirectionClock.CounterClockwise)
                {
                    translated = VectorHelper.GetPerp(translated, true);
                }

                translated += center;   //translate back

                go.transform.SetPositionFromWorldSpace(translated);
                go.OnRotated(direction);

                if (go.tile)
                {
                    GameObject otherTile = tiles[go.transform.layer][(int)go.transform.position.X, (int)go.transform.position.Y];
                    if (otherTile != null && !selectedObjects.Contains(otherTile))  //if not null and does not contain othertile
                        tiles[go.transform.layer][(int)go.transform.position.X, (int)go.transform.position.Y].Destroy(true);
                    tiles[go.transform.layer][(int)go.transform.position.X, (int)go.transform.position.Y] = go;
                    //if the tile is in the selected tiles array, don't destroy or delete it, as it'll also be moved.

                    otherTile = tiles[go.transform.layer][(int)prevPos.X, (int)prevPos.Y];

                    if (otherTile.transform.position != prevPos)
                    {
                        DeleteTile(go.transform.layer, prevPos.X, prevPos.Y);
                    }
                }
            }
        }

        public void FlipSelected(Enums.DirectionMirror direction)
        {
            Vector2 center = GetCenterOfSelection();    //note: world space, not tile space

            foreach (GameObject go in selectedObjects)
            {
                Vector2 translated = go.transform.GetPositionInWorldSpace() - center; //translate to origin

                Vector2 prevPos = go.transform.GetPositionInWorldSpace(true);

                if (direction == Enums.DirectionMirror.Horizontal)
                {
                    translated.X = -translated.X;
                }
                else if (direction == Enums.DirectionMirror.Vertical)
                {
                    translated.Y = -translated.Y;
                }

                translated += center;   //translate back

                go.transform.SetPositionFromWorldSpace(translated);
                go.OnFlipped(direction);

                if (go.tile)
                {
                    GameObject otherTile = tiles[go.transform.layer][(int)go.transform.position.X, (int)go.transform.position.Y];
                    if (otherTile != null && !selectedObjects.Contains(otherTile))  //if not null and does not contain othertile
                        tiles[go.transform.layer][(int)go.transform.position.X, (int)go.transform.position.Y].Destroy(true);
                    tiles[go.transform.layer][(int)go.transform.position.X, (int)go.transform.position.Y] = go;
                    //if the tile is in the selected tiles array, don't destroy or delete it, as it'll also be moved.

                    otherTile = tiles[go.transform.layer][(int)prevPos.X, (int)prevPos.Y];

                    if (otherTile.transform.position != prevPos)
                    {
                        DeleteTile(go.transform.layer, prevPos.X, prevPos.Y);
                    }
                }
            }
        }

        public void PaintSelected()
        {
            foreach (GameObject go in selectedObjects)
            {
                if (go.renderer != null)
                {
                    if (go.renderer.texture != null)
                    {
                        go.renderer.texture = GameMain.textureSelector.GetTexInfo();
                        go.transform.scale = GameMain.textureSelector.GetScale();
                    }
                }
            }
        }

        private Vector2 GetCenterOfSelection()
        {
            float minX = -1, minY = -1;
            float maxX = -1, maxY = -1;

            foreach (GameObject go in selectedObjects)
            {
                Vector2 posgo = go.transform.GetPositionInWorldSpace();

                if (minX == -1 || posgo.X < minX)
                    minX = posgo.X;
                if (minY == -1 || posgo.Y < minY)
                    minY = posgo.Y;
            }

            foreach (GameObject go in selectedObjects)
            {
                Vector2 posgo = go.transform.GetPositionInWorldSpace();

                if (maxX == -1 || posgo.X > maxX)
                    maxX = posgo.X;
                if (maxY == -1 || posgo.Y > maxY)
                    maxY = posgo.Y;
            }

            return VectorHelper.GetMidPoint(new Vector2(minX, minY), new Vector2(maxX, maxY));
        }

        private RectangleF GetSelectedContainmentRect()
        {
            float minX = -1, minY = -1;
            float maxX = -1, maxY = -1;

            foreach (GameObject go in selectedObjects)
            {
                Vector2 posgo = go.transform.GetPositionInWorldSpace(true);

                if (minX == -1 || posgo.X < minX)
                    minX = posgo.X;
                if (minY == -1 || posgo.Y < minY)
                    minY = posgo.Y;
            }

            foreach (GameObject go in selectedObjects)
            {
                Vector2 posgo = go.transform.GetPositionInWorldSpace(true);

                if (maxX == -1 || posgo.X + TileSize > maxX)
                    maxX = posgo.X + TileSize;
                if (maxY == -1 || posgo.Y + TileSize > maxY)
                    maxY = posgo.Y + TileSize;
            }

            Vector2 pos = new Vector2(minX, minY);
            Vector2 wh = new Vector2(maxX, maxY) - pos;
            return new RectangleF(pos, wh);
        }

        public void SnapToGridRelative(List<GameObject> gameObjects)
        {
            GameObject topLeftest = gameObjects[FindTopLeftest(gameObjects)];

            Vector2 presnap = topLeftest.transform.GetPositionInWorldSpace(true);

            SnapToGrid(topLeftest);

            Vector2 dif = topLeftest.transform.GetPositionInWorldSpace(true) - presnap;

            gameObjects.ForEach(x => x.transform.SetPositionFromWorldSpace(x.transform.GetPositionInWorldSpace(true) - dif));
        }

        private int FindTopLeftest(List<GameObject> gameObjects)
        {
            int goIndex = -1;

            for (int i = 0; i < gameObjects.Count; i++)
            {
                if (goIndex == -1 || gameObjects[i].transform.GetPositionInWorldSpace(true).Length() < gameObjects[goIndex].transform.GetPositionInWorldSpace(true).Length())
                {
                    goIndex = i;
                }
            }

            if (goIndex == -1)  //to prevent indexoutofboundsexceptions
                goIndex = 0;

            return goIndex;
        }
        #endregion

        public GameObject FindGameObjectWithTag(string tag)
        {
            return gameObjects.Find(x => x.ContainsTag(tag));
        }

        public List<GameObject> FindAllGameObjectWithTag(string tag)
        {
            return gameObjects.FindAll(x => x.ContainsTag(tag));
        }

        public void SnapToGrid(List<GameObject> gameObjects)
        {
            gameObjects.ForEach(x => SnapToGrid(x));
        }

        public void SnapToGrid(GameObject gameObject)
        {
            int gridsize = GameMain.editPanel.currentGridSize;

            Vector2 op = gameObject.transform.GetPositionInWorldSpace(true);
            op.X = ((int)(op.X / gridsize)) * gridsize;
            op.Y = ((int)(op.Y / gridsize)) * gridsize;
            gameObject.transform.SetPositionFromWorldSpace(op);
        }

        public Dictionary<string, Func<string, object>> addInstanceFunctions = new Dictionary<string, Func<string, object>>();

        public object AddInstance(string instanceName)
        {
            string[] split = instanceName.Split('.');
            return addInstanceFunctions[split[0]]?.Invoke(instanceName);
        }

        public Dictionary<string, EditableValue> values { get; set; }

        public virtual void AddEditorValues()
        {
            values.Add("bgColor", new EditableValue(x => { bgColor = (Color)x; }, delegate () { return bgColor; }));
        }

        public virtual void OnEditingModeChanged(bool editing)
        {
            if (editing) gameObjects.ForEach(go =>
            {
                if (go.renderer == null && go.isUsed && go.serializable)
                {
                    go.renderer = new RendererDebug(go);
                    rendererHandler.AddRenderer(this, go.renderer, false);
                }

                if (go.destroyOnReset)
                    go.Destroy(true);
            });

            noUpdate = editing;

            RespawnAll();

            selectedObjects.Clear();
        }

        public void RespawnAll()
        {
            time = 0;
            foreach (GameObject go in gameObjects)
            {
                if (go.destroyOnReset)
                    go.Destroy(true);
            }

            updateScripts.ForEach(x => x.ForEach(y => y.Reset()));
        }
    }
}
