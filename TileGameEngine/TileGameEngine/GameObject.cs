﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using TileGameEngine.Components.Renderers;
using TileGameEngine.Components.Colliders;
using TileGameEngine.Components;
using TileGameEngine.Components.Scripts;
using System.IO;
using BlueRavenUtility;
using TileGameEngine.Components.CollidersR;
using TileGameEngine.Editor;

namespace TileGameEngine
{
    public class GameObject : ICloneable, IEditable
    {
        public GameObject parent;
        public List<GameObject> children;

        public bool isUsed { get;
            private set;
        }

        public bool serializeTransform = true;
        private Transform _transform;
        public Transform transform { get { return _transform; } set { _transform = value; } }

        public bool serializeRenderer = true;
        private Renderer _renderer;
        public Renderer renderer { get { return _renderer; } set { _renderer = value; isUsed = true; } }
        public bool rendererInWorldList { get { return parentWorld.renderers.Contains(renderer); } set { } }
        public int rendererWorldListIndex { get { return parentWorld.renderers.FindIndex(x => x == renderer); } }

        public bool serializeCollider = true;
        private Collider _collider;
        public Collider collider { get { return _collider; } set { _collider = value; isUsed = true; } }
        public bool colliderInWorldList { get { return parentWorld.colliders.Contains(collider); } set { } }
        public int colliderWorldListIndex { get { return parentWorld.colliders.FindIndex(x => x == collider); } }

        public ColliderDynamic colliderDynamic;
        public ColliderStaticTile colliderStatic;
        public int index { get { return parentWorld.gameObjects.FindIndex(x => x == this); } }
        private bool _isAdded;
        public bool isAdded { get { return _isAdded && index != -1; } set { _isAdded = value; } }

        public bool serializeScripts = true;
        public List<Script> scripts;
        public bool scriptsInWorldList { get { return parentWorld.updateScripts.Contains(scripts); } set { } }
        public int scriptsWorldListIndex { get { return parentWorld.updateScripts.FindIndex(x => x == scripts); } }

        public bool serializable = true;

        public GameWorld parentWorld;

        public bool dead { get; private set; }

        public List<string> tags;

        public float[] data;    //data does NOT persist through save/loading
        public bool tile;

        public Dictionary<string, EditableValue> values { get; set; }

        public bool selected { get; set; }
        public bool selectable { get; set; }

        public bool active;
        public bool forceActive;

        public bool destroyOnReset;
        public GameObject(GameWorld world)
        {
            this.parentWorld = world;

            scripts = new List<Script>();

            children = new List<GameObject>();

            tags = new List<string>();

            data = new float[8];

            selectable = true;

            values = new Dictionary<string, EditableValue>();

            active = true;
        }

        /// <summary>
        /// Adds the given gameobject as a child.
        /// Functionally the same as using <see cref="AddParent(GameObject)"/>.
        /// Run one or the other, not both.
        /// </summary>
        public void AddChild(GameObject child)
        {
            if (child.parent != null)
                child.parent.children.Remove(this);
            children.Add(child);
            child.parent = this;

            children.CheckAndDelete(x => x.dead);
        }

        /// <summary>
        /// Adds the given gameobject as the current's parent.
        /// Functionally the same as using <see cref="AddChild(GameObject)"/>.
        /// Run one or the other, not both.
        /// </summary>
        public void AddParent(GameObject parent)
        {
            if (parent != null)
                parent.children.Remove(this);
            this.parent = parent;
            this.parent.AddChild(this);

            parent.children.CheckAndDelete(x => x.dead);
        }

        public void RemoveParent(GameObject parent)
        {
            parent.children.Remove(this);
            parent.children.CheckAndDelete(x => x.dead);
            this.parent = null;
        }

        public void AddScript(Script script)
        {
            scripts.Add(script);
            isUsed = true;
        }

        public void AddScripts(List<Script> scripts)
        {
            scripts.AddRange(scripts);
            isUsed = true;
        }

        #region serializing

        #region transform
        public void SerializeTransform(BinaryWriter writer)
        {
            if (serializeTransform)
            {
                bool exists = transform != null;
                writer.Write(exists);

                if (exists)
                {
                    int count = 0;
                    transform.GetDynamicByteSize(ref count); //dynamic lengths
                    writer.Write(count);
                    writer.Write(transform.GetByteSize() + count);  //length

                    transform.Serialize(writer);            //transform
                }
            }
        }

        public void DeserializeTransform(BinaryReader reader)
        {
            if (serializeTransform)
            {
                if (reader.ReadBoolean())                                       //exists
                {
                    bool badlen = false;
                    transform = new Transform();
                    int dynLength = transform.ReadDynamicByteSize(reader);  //dynamic lengths
                    int length = reader.ReadInt32();                            //length

                    try
                    {
                        if (length != transform.GetByteSize() + dynLength)
                        {
                            badlen = true;
                            throw new FormatException("Transform byte size incorrect.");
                        }

                        transform.Deserialize(reader);
                        transform.owner = this;
                    }
                    catch (Exception e)
                    {
                        if (badlen)
                            reader.ReadBytes(length);
                        Console.WriteLine(e.ToString());
                    }
                }
            }
        }
        #endregion

        #region collider
        public void SerializeCollider(BinaryWriter writer)
        {
            if (serializeCollider)
            {
                bool exists = collider != null;
                writer.Write(exists && collider.serializable);

                if (exists && collider.serializable)
                {
                    writer.Write(collider.GetType().ToString());    //write the type first

                    int count = 0;
                    renderer.GetDynamicByteSize(ref count);
                    writer.Write(count);

                    writer.Write(collider.GetByteSize() + count);
                    
                    collider.Serialize(writer);
                }
            }
        }

        public void DeserializeCollider(BinaryReader reader)
        {
            if (serializeCollider)
            {
                if (reader.ReadBoolean())
                {
                    bool badlen = false;
                    string n = reader.ReadString();
                    collider = (Collider)parentWorld.AddInstance(n);
                    int dynLength = collider.ReadDynamicByteSize(reader);
                    int length = reader.ReadInt32();

                    try
                    {
                        if (length != collider.GetByteSize() + dynLength)
                        {
                            badlen = true;
                            throw new FormatException("Collider byte size incorrect.");
                        }
                        collider.Deserialize(reader);
                        collider.owner = this;
                        
                        parentWorld.colliderHandler.AddCollider(parentWorld, collider);
                    }
                    catch (Exception e)
                    {
                        if (badlen)
                        {
                            reader.ReadBytes(length);
                        }
                        Console.WriteLine(e.ToString());
                    }
                }
            }
        }
        #endregion

        #region renderer
        public void SerializeRenderer(BinaryWriter writer)
        {
            if (serializeRenderer)
            {
                bool exists = renderer != null;
                writer.Write(exists && renderer.serializable);

                if (exists && renderer.serializable)
                {
                    writer.Write(renderer.GetType().ToString());

                    int count = 0;
                    renderer.GetDynamicByteSize(ref count);
                    writer.Write(count);

                    writer.Write(renderer.GetByteSize() + count);

                    renderer.Serialize(writer);
                }
            }
        }

        public void DeserializeRenderer(BinaryReader reader)
        {
            if (serializeRenderer)
            {
                if (reader.ReadBoolean())
                {
                    bool badlen = false;
                    string n = reader.ReadString();
                    renderer = (Renderer)parentWorld.AddInstance(n);
                    int dynLength = renderer.ReadDynamicByteSize(reader);
                    int length = reader.ReadInt32();

                    try
                    {
                        if (length != renderer.GetByteSize() + dynLength)
                        {
                            badlen = true;
                            throw new FormatException("Renderer byte size incorrect.");
                        }

                        renderer.Deserialize(reader);
                        renderer.owner = this;
                        parentWorld.renderers.Add(renderer);
                    }
                    catch (Exception e)
                    {
                        if (badlen)
                        {
                            reader.ReadBytes(length);
                        }

                        Console.WriteLine(e.ToString());
                    }
                }
            }
        }
        #endregion

        #region scripts
        public void SerializeScripts(BinaryWriter writer)
        {
            if (serializeScripts)
            {
                writer.Write(scripts.FindAll(x => x.serializable).Count);

                foreach (Script script in scripts)
                {
                    if (script.serializable)
                    {
                        string n = script.GetType().ToString();
                        writer.Write(n);

                        int count = 0;
                        script.GetDynamicByteSize(ref count);
                        writer.Write(count);
                        writer.Write(script.GetByteSize() + count);


                        script.Serialize(writer);
                    }
                }
            }
        }

        public void DeserializeScripts(BinaryReader reader)
        {
            if (serializeScripts)
            {
                int scriptcount = reader.ReadInt32();

                for (int i = 0; i < scriptcount; i++)
                {
                    bool badlen = false;
                    string n = reader.ReadString();

                    Script script = (Script)parentWorld.AddInstance(n);
                    int dynLength = script.ReadDynamicByteSize(reader);
                    int length = reader.ReadInt32();
                    try
                    {
                        if (length != script.GetByteSize() + dynLength)
                        {
                            badlen = true;
                            throw new FormatException("Script byte size incorrect.");
                        }

                        script.Deserialize(reader);
                        AddScript(script);
                        script.owner = this;
                    }
                    catch (Exception e)
                    {
                        if (badlen)
                        {
                            reader.ReadBytes(length);
                        }

                        Console.WriteLine(e.ToString());
                    }
                }
                parentWorld.updateScripts.Add(scripts);
            }
        }
        #endregion

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(tile);

            SerializeTransform(writer);

            SerializeCollider(writer);

            SerializeRenderer(writer);

            SerializeScripts(writer);

            if (tags.Count > 0)
            {
                writer.Write(true);

                tags.ForEach(t =>
                {
                    writer.Write(true);
                    if (!t.StartsWith("nosave_"))
                        writer.Write(t);
                });

                writer.Write(false);
            }
            else writer.Write(false);
        }

        public void Deserialize(BinaryReader reader, bool add = true)
        {
            tile = reader.ReadBoolean();

            DeserializeTransform(reader);

            DeserializeCollider(reader);

            DeserializeRenderer(reader);

            DeserializeScripts(reader);

            if (reader.ReadBoolean())   //we have tags!
            {
                while(reader.ReadBoolean())
                {
                    tags.Add(reader.ReadString());
                }
            }

            if (tile && add)
            {
                parentWorld.tiles[transform.layer][(int)transform.position.X, (int)transform.position.Y] = this;
            }
        }
        #endregion

        public void OnRotated(Enums.DirectionClock direction)
        {
            collider?.OnRotated(direction);
            renderer?.OnRotated(direction);
            scripts.ForEach(x => x.OnRotated(direction));
        }

        public void OnFlipped(Enums.DirectionMirror direction)
        {
            collider?.OnFlipped(direction);
            renderer?.OnFlipped(direction);
            scripts.ForEach(x => x.OnFlipped(direction));
        }

        public void AddEditorValues()
        {
            values.Clear();

            if (transform != null)
            {
                transform.AddEditorValues();
                transform.values.ToList().ForEach(x => { values.Add(x.Key, x.Value); });
            }

            collider?.AddEditorValues();
            renderer?.AddEditorValues();
            scripts.ForEach(x => x.AddEditorValues());
        }

        public object Clone()
        {
            GameObject r = new GameObject(parentWorld);
            r.transform = new Transform(this, transform.position, transform.scale, transform.rotation);
            r.parent = parent;
            r.children = children.ToList();
            r.collider = (Collider)collider?.Clone();
            r.renderer = (Renderer)renderer?.Clone();
            scripts.ForEach(x => { r.AddScript((Script)x.Clone()); });
            r.tile = tile;
            r.forceActive = forceActive;

            return r;
        }

        /// <summary>
        /// Check to make sure none of our components 'object' to the destruction.
        /// </summary>
        /// <returns>Whether to continue being destroyed or not.</returns>
        public bool PreDestroy()
        {
            bool objection = false;
            while (true)
            {
                if (collider != null)
                {
                    if (collider.PreDestroy())
                    {
                        objection = true;
                        break;
                    }
                }
                if (renderer != null)
                {
                    if (renderer.PreDestroy())
                    {
                        objection = true;
                        break;
                    }
                }
                
                foreach (Script script in scripts)
                {
                    if (script.PreDestroy())
                    {
                        objection = true;
                        break;
                    }
                }
                break;
            }
            return objection;
        }

        public void Destroy(bool force = false)
        {
            if (!force && PreDestroy())
                return;

            dead = true;
            if (collider != null)
                collider.dead = true;
            if (renderer != null)
                renderer.dead = true;

            if (tile)
                parentWorld.DeleteTile(transform.layer, transform.position.X, transform.position.Y);

            scripts.ToList().ForEach(x => x.dead = true);
        }

        public void Destroy<T>() where T : Component
        {
            Component c = GetComponent<T>();
            c.dead = true;
        }

        public T GetComponent<T>() where T : Component
        {
            if (transform != null && transform.GetType() == typeof(T))
            {
                return transform as T;
            }
            else if (collider != null && (collider.GetType().IsAssignableFrom(typeof(T)) || typeof(T).IsAssignableFrom(collider.GetType())))
            {
                return collider as T;
            }
            else if (renderer != null && (renderer.GetType().IsAssignableFrom(typeof(T)) || typeof(T).IsAssignableFrom(renderer.GetType())))
            {
                return renderer as T;
            }
            else
            {
                T fs = null;
                scripts.ForEach(x =>
                {
                    if (x.GetType().IsAssignableFrom(typeof(T)) || typeof(T).IsAssignableFrom(x.GetType()))
                        fs = x as T;
                });

                return fs;
            }
        }

        public bool ContainsTag(string tag)
        {
            return tags.Contains(tag);
        }

        public override string ToString()
        {
            return base.ToString() + " tile:" + tile + " active:" + active + " index:" + index;
        }
    }
}
