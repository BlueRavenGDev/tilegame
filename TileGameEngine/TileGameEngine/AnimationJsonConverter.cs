﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace TileGameEngine
{
    public class AnimationJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Animation[]);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            List<Animation> animations = new List<Animation>();

            List<Frame> frames = new List<Frame>();

            if (reader.TokenType == JsonToken.EndObject)
                return null;

            if (reader.TokenType == JsonToken.StartObject)
            {
                
            }

            return new Animation[1];
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            
        }
    }
}
