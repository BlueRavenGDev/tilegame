﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using BlueRavenUtility;

using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using TileGameEngine.Editor;

namespace TileGameEngine
{
    public enum AnimationType
    {
        Return,       //after the animation has finished, return to a preset idle animation
        Loop,   //after the animation has finished, loop back to the start of it
        Pause      //after the animation has finished, pause on the last frame
    }

    public class TextureInfo : ICloneable, IEditable
    {
        public string name { get; private set; }
        public bool isGlobalAsset { get; private set; }

        public Vector2 center
        {
            get
            {
                if (sourceRectangle != Rectangle.Empty && !currentAnimation.usable)
                    return sourceRectangle.Center.ToVector2();
                else if (GetCurrentSourceRectangle() != Rectangle.Empty)
                    return GetCurrentSourceRectangle().Center.ToVector2();
                else
                    return new Vector2(width / 2, height / 2);
            }
        }
        public Vector2 originCenter
        {
            get
            {
                if (sourceRectangle != Rectangle.Empty && !currentAnimation.usable)
                    return sourceRectangle.Center.ToVector2();
                else if (GetCurrentSourceRectangle() != Rectangle.Empty)
                {
                    Rectangle csr = GetCurrentSourceRectangle();
                    return new Vector2(csr.Width / 2, csr.Height / 2);
                }
                else return new Vector2(width / 2, height / 2);
            }
        }

        public Vector2 scale;
        public Color color;

        public Rectangle sourceRectangle;

        public Texture2D baseTexture;

        public int width { get; private set; }
        public int height { get; private set; }

        public Dictionary<string, EditableValue> values { get; set; }

        public AnimationType animationType;

        public List<Animation> animations;
        public Animation currentAnimation;

        public Queue<Frame> frames;
        public Frame currentFrame;
        public float frameTime;

        public int renderPriority;

        public SpriteEffects flip;

        /// <summary>
        /// FOR DESERIALIZATION ONLY
        /// </summary>
        public TextureInfo()
        {
            animations = new List<Animation>();
            frames = new Queue<Frame>();

            values = new Dictionary<string, EditableValue>();
        }

        public TextureInfo(string textureName, bool isGlobalAsset = false)
        {
            SetTexture(textureName, isGlobalAsset);

            animations = new List<Animation>();
            frames = new Queue<Frame>();

            values = new Dictionary<string, EditableValue>();
        }

        public TextureInfo(string textureName, Rectangle sourceRectangle, bool isGlobalAsset = false)
        {
            SetTexture(textureName, isGlobalAsset);

            this.sourceRectangle = sourceRectangle;

            animations = new List<Animation>();
            frames = new Queue<Frame>();

            values = new Dictionary<string, EditableValue>();
        }

        private void SetTexture(string textureName, bool isGlobalAsset)
        {
            this.name = textureName;
            this.baseTexture = isGlobalAsset ? GameMain.assets.textures.GetAsset(textureName) : GameWorld.assets.textures.GetAsset(textureName);

            UnwrapSize();
        }

        private void UnwrapSize()
        {
            width = baseTexture.Width;
            height = baseTexture.Height;
        }

        public Vector2[] GetTexCoordsFromSourceRect()
        {
            Vector2[] coords = new Vector2[4];

            coords[0] = new Vector2(sourceRectangle.X / width, sourceRectangle.Y / height);
            coords[1] = new Vector2(sourceRectangle.X + sourceRectangle.Width / width, sourceRectangle.Y / height);
            coords[2] = sourceRectangle.Location.ToVector2() + sourceRectangle.Size.ToVector2() / new Vector2(width, height);
            coords[3] = new Vector2(sourceRectangle.X / width, sourceRectangle.Y + sourceRectangle.Height / height);

            return coords;
        }

        public void AddAnimation(List<Animation> animations, bool @override = false)
        {
            if (@override)
                this.animations = animations;
            else animations.ForEach(x => this.animations.Add(x));

            if (!currentAnimation.usable)
                StartAnimation(this.animations[0], true);
        }

        public void UpdateFrame()
        {
            if (animations.Count > 0)   //if we're actually an animated texture
            {
                if (currentAnimation.usable && currentFrame.usable)
                {
                    frameTime -= 1 * GameWorld.timescale;

                    if (frameTime <= 0)
                    {   //if our current frame is done
                        if (frames.Count > 0)   //if there are more frames in the currrent animation,
                            StartFrame(frames.Dequeue());   //start the next frame.
                        else
                        {
                            AnimationType type = animations[currentFrame.animationIndex].animationType;
                            if (type == AnimationType.Loop)
                            {
                                StartAnimation(currentAnimation);
                            }
                            else if (type == AnimationType.Pause)
                            {
                                StartFrame(currentAnimation.frames[currentAnimation.frames.Count - 1]); //go to last frame. Note that we do not enque to frames list.
                            }
                            else if (type == AnimationType.Return)
                            {
                                StartAnimation(animations[currentAnimation.returnIndex]);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Starts the given animation.
        /// </summary>
        /// <param name="startRunningFromStart">Starts the first frame of the given animation.</param>
        private void StartAnimation(Animation animation, bool startRunningFromStart = true)
        {
            currentAnimation = animation;
            animation.frames.ForEach(x => frames.Enqueue(x));

            if (startRunningFromStart)
                StartFrame(frames.Dequeue());
        }

        public void StartAnimation(int animIndex, bool force = false, bool forceSelf = false)
        {
            if (currentAnimation.index != animIndex || forceSelf)
            {
                currentAnimation = animations[animIndex];

                if (force)
                    frames.Clear();
                currentAnimation.frames.ForEach(x => frames.Enqueue(x));
            }
        }

        /// <summary>
        /// Forces all currently queued frames to have the given framerate.
        /// </summary>
        /// <param name="newFramerate">The framerate to set all frames to, with a minimum of 1.</param>
        public void ForceFramerate(int newFramerate)
        {
            List<Frame> copy = frames.ToList();
            for (int i = 0; i < copy.Count; i++)
            {
                copy[i] = new Frame(copy[i].animationIndex, copy[i].frameIndex, copy[i].sourceRect, newFramerate);
            }
            frames = new Queue<Frame>(copy);
        }

        /// <summary>
        /// Starts the next frame in the current animation.
        /// </summary>
        public void StartFrame(Frame frame)
        {
            currentFrame = frame;
            frameTime = currentFrame.timeMax;
        }

        public Rectangle GetCurrentSourceRectangle()
        {
            if (currentFrame.usable)
                return (currentFrame.sourceRect);
            return sourceRectangle;
        }

        public void TryFindAnimFile(string overrideFileName = "!")
        {
            try
            {
                File.WriteAllText("Content/test.json", JsonConvert.SerializeObject(animations, Formatting.Indented));
                using (StreamReader file = File.OpenText("Content/anim/" + (overrideFileName == "!" ? name : overrideFileName) + ".json"))
                {
                    var obj = JsonConvert.DeserializeObject<List<Animation>>(file.ReadToEnd());
                    //JsonSerializer serializer = new JsonSerializer();
                    //List<Animation> anims = (List<Animation>)serializer.Deserialize(file, typeof(List<Animation>));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //NOOP TODO WRITE WARN
            }
        }

        public object Clone()
        {
            TextureInfo clone = new TextureInfo(name, sourceRectangle, isGlobalAsset)
            {
                color = this.color,
                scale = this.scale,
                flip = this.flip
            };
            if (this.animations.Count > 0)
                clone.AddAnimation(this.animations, true);

            return clone;
        }

        public int GetByteSize()
        {
            int animSize = GetAnimByteSize();

            int namelen = 0;
            if (name != null)
                namelen = name.Length * sizeof(char);
            return 1 +              //global bool byte
                4 + 4 + 4 + 4 +     //rect xy, wh
                1 + 1 + 1 + 1 +     //color rgba
                4 + 4 +             //scale xy
                1 + 1 +             //flip values
                namelen +
                animSize +
                animations.Count * 4 + //animations size. Frames count included in animSize.
                4;                  //this int's size
        }

        private int GetAnimByteSize()
        {
            int animSize = 0;
            animations.ForEach(x => animSize += x.GetByteSize());
            return animSize;
        }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(name.Length * sizeof(char));
            writer.Write(GetAnimByteSize());

            writer.Write(GetByteSize());

            writer.Write(name);
            writer.Write(isGlobalAsset);
            writer.Write(renderPriority);

            writer.Write(sourceRectangle.X);
            writer.Write(sourceRectangle.Y);
            writer.Write(sourceRectangle.Width);
            writer.Write(sourceRectangle.Height);

            writer.Write(color.R);
            writer.Write(color.G);
            writer.Write(color.B);
            writer.Write(color.A);

            writer.Write(scale.X);
            writer.Write(scale.Y);

            writer.Write(flip.Has(SpriteEffects.FlipHorizontally));
            writer.Write(flip.Has(SpriteEffects.FlipVertically));

            writer.Write(animations.FindAll(x => x.usable).Count);

            foreach (Animation a in animations)
            {
                if (a.usable)
                {
                    writer.Write(a.index);
                    writer.Write((int)a.animationType);
                    writer.Write(a.returnIndex);

                    writer.Write(a.frames.Count);

                    foreach (Frame f in a.frames)
                    {
                        writer.Write(f.animationIndex);
                        writer.Write(f.frameIndex);
                        writer.Write(f.sourceRect.X);
                        writer.Write(f.sourceRect.Y);
                        writer.Write(f.sourceRect.Width);
                        writer.Write(f.sourceRect.Height);
                        writer.Write(f.timeMax);
                    }
                }
            }
            //TODO serialize animations
        }

        public void Deserialize(BinaryReader reader)
        {
            int namelen = reader.ReadInt32();   //name length
            int animlen = reader.ReadInt32();   //animations length

            int length = reader.ReadInt32();

            bool badlength = false;

            try
            {
                if (length != GetByteSize() + namelen + animlen)
                {
                    badlength = true;
                    throw new FormatException("TextureInfo byte size incorrect.");
                }

                name = reader.ReadString();
                isGlobalAsset = reader.ReadBoolean();
                renderPriority = reader.ReadInt32();

                this.baseTexture = isGlobalAsset ? GameMain.assets.textures.GetAsset(name) : GameWorld.assets.textures.GetAsset(name);

                sourceRectangle.X = reader.ReadInt32();
                sourceRectangle.Y = reader.ReadInt32();
                sourceRectangle.Width = reader.ReadInt32();
                sourceRectangle.Height = reader.ReadInt32();

                color.R = reader.ReadByte();
                color.G = reader.ReadByte();
                color.B = reader.ReadByte();
                color.A = reader.ReadByte();

                scale.X = reader.ReadSingle();
                scale.Y = reader.ReadSingle();

                if (reader.ReadBoolean())
                    flip.Add(SpriteEffects.FlipHorizontally);
                if (reader.ReadBoolean())
                    flip.Add(SpriteEffects.FlipVertically);

                animations.Clear();

                List<Animation> aa = new List<Animation>();

                int animcount = reader.ReadInt32();

                for (int i = 0; i < animcount; i++)
                {
                    int index = reader.ReadInt32();
                    AnimationType type = (AnimationType)reader.ReadInt32();
                    int returnIndex = reader.ReadInt32();

                    List<Frame> fs = new List<Frame>();

                    int framecount = reader.ReadInt32();

                    for (int j = 0; j < framecount; j++)
                    {
                        int animIndex = reader.ReadInt32();
                        int findex = reader.ReadInt32();
                        int rectx = reader.ReadInt32();
                        int recty = reader.ReadInt32();
                        int rectw = reader.ReadInt32();
                        int recth = reader.ReadInt32();
                        int timeMax = reader.ReadInt32();

                        Frame tf = new Frame(animIndex, findex, new Rectangle(rectx, recty, rectw, recth), timeMax);
                        fs.Add(tf);
                    }

                    Animation a = new Animation(index, type, returnIndex, fs.ToArray());
                    aa.Add(a);
                }

                if (aa.Count > 0)
                    AddAnimation(aa, true);
            }
            catch (Exception e)
            {
                if (badlength)
                {   //read the rest of the bytes so we're not negatively offset.
                    reader.ReadBytes(length - 4);   //length - length size (since we already deserialized that.
                    Console.WriteLine("bad length");
                }   //basically, if the length is wrong, we'll just totally ignore the rest of the component.

                Console.WriteLine(e);
            }
        }

        public void AddEditorValues()
        {
            values.Clear(); 

            values.Add("textureName", new EditableValue(x => { SetTexture((string)x, isGlobalAsset); }, delegate () { return name; }));
            values.Add("renderPriority", new EditableValue(x => { renderPriority = (int)x; }, delegate () { return renderPriority; }));
            values.Add("isGlobalAsset", new EditableValue(x => { isGlobalAsset = (bool)x; }, delegate () { return isGlobalAsset; }));
            values.Add("sourceRect", new EditableValue(x => { sourceRectangle = (Rectangle)x; }, delegate () { return sourceRectangle; }));

            values.Add("Flip X", new EditableValue(x => 
            {
                if ((bool)x) if (flip.Has(SpriteEffects.FlipHorizontally)) { flip.Remove(SpriteEffects.FlipHorizontally); } else flip.Add(SpriteEffects.FlipHorizontally);
            }, 
            delegate ()
            {
                return (flip & SpriteEffects.FlipHorizontally) == SpriteEffects.FlipHorizontally;
            }));

            values.Add("Flip Y", new EditableValue(x =>
            {
                if ((bool)x) if (flip.Has(SpriteEffects.FlipVertically)) { flip.Remove(SpriteEffects.FlipVertically); } else flip.Add(SpriteEffects.FlipVertically);
            },
            delegate ()
            {
                return (flip & SpriteEffects.FlipVertically) == SpriteEffects.FlipVertically;
            }));
        }

        public override string ToString()
        {
            return name;
        }
    }
}
