﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine;
using TileGameEngine.Components.Scripts;

namespace TileGame.Components.Scripts
{
    public class ScriptCollectableEnergy : ScriptCollectable
    {
        ScriptDamagable pds;

        public ScriptCollectableEnergy(GameObject owner) : base(owner, 16, "player")
        {
            offset = new Vector2(0, 32);
        }

        public override bool CanPickup(GameObject collector)
        {
            return true;
        }

        public override void OnPickup(GameObject collector)
        {
            if (pds == null)
            {
                pds = collector.GetComponent<ScriptDamagable>();
                if (pds == null)
                    return;
            }
            pds.health++;
        }

        public override object Clone()
        {
            return new ScriptCollectableEnergy(owner);
        }

        public override int GetByteSize()
        {
            return 0;
        }
    }
}
