﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine;
using TileGameEngine.Components.Scripts;
using TileGameEngine.Components.Colliders;
using Microsoft.Xna.Framework.Graphics;
using BlueRavenUtility;

namespace TileGame.Components.Scripts
{
    public class ScriptPlayerWaterTransport : Script
    {
        private int currentPoint;

        private List<Vector2> points;

        private bool running;

        private GameObject moving;

        private float movePercent;
        private float currentMoveP;

        public ScriptPlayerWaterTransport() : base()
        {
            points = new List<Vector2>();
        }

        public ScriptPlayerWaterTransport(GameObject owner, float movePercent, params Vector2[] points) : base(owner)
        {
            this.points = points.ToList();
            this.movePercent = movePercent;

            currentPoint = 0;
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);
            if (running)
            {
                if (moving == null)
                    Reset();

                currentMoveP += movePercent;

                moving.GetComponent<ScriptPlayerMovement>().inputLockoutTimer = 2;
                Vector2 lerp = Vector2.Lerp(owner.transform.GetPositionInWorldSpace(true) + points[currentPoint],
                    owner.transform.GetPositionInWorldSpace(true) + points[currentPoint + 1],
                    currentMoveP);
                moving.transform.SetPositionFromWorldSpace(lerp);

                if (currentMoveP > 1)
                {
                    if (currentPoint + 1 == points.Count - 1)
                    {
                        moving.collider.velocity = Vector2.Normalize(points[currentPoint + 1] - points[currentPoint]) * 6;
                        running = false;
                    }
                    else currentPoint++;
                    currentMoveP = 0;
                }
            }
        }

        public override void OnStartTouch(Collider collider)
        {
            base.OnStartTouch(collider);

            if (collider.dynamic && collider.owner.ContainsTag("player"))
            {
                running = true;
                moving = collider.owner;

                currentPoint = 0;
            }
        }

        public override void OnDrawDebug(SpriteBatch batch)
        {
            base.OnDrawDebug(batch);

            for (int i = 0; i < points.Count - 1; i++)
            {
                batch.DrawLine(points[i], points[i + 1], Color.Red, 4);
            }
        }

        public override void Reset()
        {
            base.Reset();

            moving = null;
            running = false;
            currentPoint = 0;
            currentMoveP = 0;
        }

        public override object Clone()
        {
            return new ScriptPlayerWaterTransport(owner, movePercent, points.ToArray());
        }

        public override void GetDynamicByteSize(ref int count)
        {
            count += points.Count * 8; //8 bytes per vector - 4 x, 4 y
        }

        public override int GetByteSize()
        {
            return 0;
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(movePercent);

            writer.Write(points.Count);

            foreach (Vector2 point in points)
            {
                writer.Write(point.X);
                writer.Write(point.Y);
            }
        }

        public override void Deserialize(BinaryReader reader)
        {
            movePercent = reader.ReadSingle();

            int count = reader.ReadInt32();

            for (int i = 0; i < count; i++)
            {
                points.Add(new Vector2(reader.ReadSingle(), reader.ReadSingle()));
            }
        }
    }
}
