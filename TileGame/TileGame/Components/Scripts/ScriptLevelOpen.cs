﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using TileGameEngine;
using TileGameEngine.Components.Scripts;
using Microsoft.Xna.Framework;
using BlueRavenUtility;

namespace TileGame.Components.Scripts
{
    public class ScriptLevelOpen : Script
    {
        private Color textColor;

        private bool done;
        private int textTime, textTimeToFade, textTimeMax;  //between time to fade and time max text fades out
        private int firstTimer, secondTimer;
        private int firstTimerMax = 30;
        private int secondTimerMax = 20;

        private string levelName;

        private SpriteFont font;
        public ScriptLevelOpen() : base()
        {
            font = GameWorld.assets.fonts.GetAsset("bitfontMunro72");
            owner.renderer.renderPriority = 256;
        }

        public ScriptLevelOpen(GameObject owner, string levelName) : base(owner)
        {
            this.levelName = levelName;

            textTimeToFade = firstTimerMax + secondTimerMax;
            textTimeMax = firstTimerMax + secondTimerMax + 60;
            textColor = Color.White;

            font = GameWorld.assets.fonts.GetAsset("bitfontMunro72");

            owner.renderer.renderPriority = 256;
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            if (firstTimer <= firstTimerMax)
                firstTimer++;
            if (firstTimer >= firstTimerMax / 2 && secondTimer <= firstTimerMax + secondTimerMax)
                secondTimer++;

            if (textTime <= textTimeMax)
            {
                textTime++;

                if (textTime > textTimeToFade && textTime <= textTimeMax)
                {
                    float textAP = Math.Abs(((float)(textTime - textTimeToFade) / (float)(textTimeMax - textTimeToFade)) - 1);
                    int a = (int)(255f * textAP);
                    textColor = new Color(textColor, a);
                }
                else if (textTime > textTimeMax) done = true;
            }
        }

        public override void OnDraw(SpriteBatch batch)
        {
            base.OnDraw(batch);

            if (!done && !GameMain.editing)
            {
                float firstP = (float)firstTimer / (float)firstTimerMax;
                float secondP = (float)secondTimer / (float)secondTimerMax;
                Vector2 firstOffsetVec = new Vector2(GameMain.WIDTH / 2 * firstP, 0);
                Vector2 secondOffsetVecMid = new Vector2(0, GameMain.HEIGHT / 2 * secondP);
                batch.DrawRectangle(new RectangleF(Camera.camera.Position - secondOffsetVecMid, GameMain.WIDTH, GameMain.HEIGHT / 2), Color.White);
                batch.DrawRectangle(new RectangleF(Camera.camera.Position + new Vector2(0, GameMain.HEIGHT / 2) + secondOffsetVecMid, GameMain.WIDTH, GameMain.HEIGHT / 2), Color.White);
                batch.DrawString(font, levelName, Camera.camera.Position + TextHelper.GetAlignmentOffset(font, levelName, new Rectangle(0, 0, GameMain.WIDTH, GameMain.HEIGHT), Enums.Alignment.Center), textColor);
                batch.DrawRectangle(new RectangleF(Camera.camera.Position - firstOffsetVec, GameMain.WIDTH / 2, GameMain.HEIGHT), Color.Black);
                batch.DrawRectangle(new RectangleF(Camera.camera.Position + new Vector2(GameMain.WIDTH / 2, 0) + firstOffsetVec, GameMain.WIDTH / 2, GameMain.HEIGHT), Color.Black);
            }
        }

        public override void Reset()
        {
            base.Reset();

            firstTimer = 0;
            secondTimer = 0;
            textTime = 0;
            done = false;
        }

        public override object Clone()
        {
            return null;
        }

        public override int GetByteSize()
        {
            return levelName.Length * sizeof(char);
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(levelName);
        }

        public override void Deserialize(BinaryReader reader)
        {
            levelName = reader.ReadString();
        }
    }
}
