﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileGameEngine;
using TileGameEngine.Components.Scripts;

namespace TileGame.Components.Scripts
{
    public class ScriptPlayerDamage : ScriptDamagable
    {
        bool atzero;
        int timescaleTime;

        private bool reset;

        public bool dying { get; private set; }
        private int dyingtimer, dyingtimerMax = 125;
        public ScriptPlayerDamage(GameObject owner) : base(owner, 0, 0, 1, "enemy")
        {
        }

        public override void Update(GameWorld world)
        {
            base.Update(world);

            timescaleTime--;

            if (timescaleTime <= 0 && !reset)
            {
                reset = true;
                GameWorld.timescale = 1;
            }
            else if (timescaleTime > 0) GameWorld.timescale = .125f;

            if (dying)
            {
                dyingtimer--;

                float zp = (float)dyingtimer / (float)dyingtimerMax;
                zp = Math.Abs(zp - 1);
                Camera.camera.Zoom.X = (float)EngineMathHelper.CosineInterpolate(Camera.camera.Zoom.X, 2.5f, zp);
                Camera.camera.Zoom.Y = (float)EngineMathHelper.CosineInterpolate(Camera.camera.Zoom.Y, 2.5f, zp);
                //Camera.camera.Zoom = Vector2.Lerp(Vector2.One, new Vector2(2.5f), zp);

                if (dyingtimer <= 60 && dyingtimer > 0 && !Camera.camera.fading)
                {
                    Camera.camera.SetFade(Color.Black, false, 60);
                }

                if (dyingtimer <= 0)
                    owner.Destroy(false);
            }
        }

        public override bool CanTakeDamage(ScriptDamagable other)
        {
            return base.CanTakeDamage(other) && !owner.GetComponent<ScriptPlayerMovement>().parrying;
        }

        protected override void TakeDamage(ScriptDamagable other, int damage)
        {
            atzero = false;

            bool wasatgreater = false;
            if (health > 0)
                wasatgreater = true;

            base.TakeDamage(other, damage);

            if (health <= 0 && wasatgreater)
                atzero = true;

            owner.collider.NotOnGround();

            ScriptPlayerMovement moveScript = owner.GetComponent<ScriptPlayerMovement>();

            owner.collider.velocity.X = -moveScript.facingDirection * 2;
            owner.collider.velocity.Y = -4;

            moveScript.inputLockoutTimer = 60;
            invulnTime = 120;
        }

        public override void OnDealDamage(ScriptDamagable other)
        {
            base.OnDealDamage(other);
           
            ScriptPlayerMovement spm = owner.GetComponent<ScriptPlayerMovement>();
            if (spm != null)
            {
                if (spm.parrying)
                {
                    float yspd = -Math.Min(6 + (int)(spm.yfalltime / 25), 12);  //how far up we jump after hitting is based off how long we were in the air.
                    owner.collider.velocity.Y = yspd;   //full jump height is achieved after 150 frames of falling (2.5 sec)
                                                        //note that falltime only increases while ysp > 0 (falling)
                    owner.collider.NotOnGround();

                    GameObject poof = GameMain.CloneFromPreset("internal_ent_particle_parrypoof", owner.transform.position + new Vector2(0, -32), false);
                    owner.parentWorld.AddGameObject(poof, false);
                    spm.canParry = true;

                    timescaleTime = 16;
                    reset = false;
                }
                else
                {
                    owner.collider.velocity = Vector2.Normalize(owner.transform.position - other.owner.transform.position) * 2;
                    owner.collider.NotOnGround();
                }
            }
        }

        public override bool CanDie()
        {
            return base.CanDie() && damagedFrame && !atzero;   //if we have <= 0hp AND have been damaged this frame, can die.
        }

        public override bool PreDestroy()
        {
            if (!dying)
            {
                dying = true;
                dyingtimer = dyingtimerMax;
                timescaleTime = 0;  //in case we hit an enemy on the same frame - setting timescale
                owner.GetComponent<ScriptPlayerMovement>().inputLockoutTimer = dyingtimerMax;
                owner.collider.gravity = 0.21875f / 4;
                Camera.camera.forceNoClamp = true;
                Camera.camera.SetMoveMode(CameraMoveMode.Snap, dyingtimerMax);
                return true;
            }
            return false;
        }

        public override void OnDeath()
        {
            Camera.camera.Zoom = Vector2.One;
            Camera.camera.forceNoClamp = false;
            owner.parentWorld.RespawnAll();
            base.OnDeath();
        }

        public override void Reset()
        {
            base.Reset();

            Camera.camera.Zoom = Vector2.One;
            Camera.camera.forceNoClamp = false;
        }

        public override object Clone()
        {
            return new ScriptPlayerDamage(owner);
        }
    }
}
