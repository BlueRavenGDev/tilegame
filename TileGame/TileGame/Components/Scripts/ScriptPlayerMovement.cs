﻿using BlueRavenUtility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TileGameEngine;
using TileGameEngine.Components.Colliders;
using System.IO;
using TileGameEngine.Components.Scripts;
using Microsoft.Xna.Framework.Graphics;

namespace TileGame.Components.Scripts
{
    public class ScriptPlayerMovement : Script
    {
        private bool initialJumpHeld;

        public const float friction = 0.046875f, acceleration = 0.046875f, airAccel = 0.09375f, deceleration = 0.5f, 
            gtop = 10, atop = 16, gravity = 0.21875f, slope = .125f, airDrag = 0.96875f;
        public const float horizontalScrollDist = 25, verticalScrollDist = 1, verticalScrollDistAir = 64;

        private float gspeed;

        public float yfalltime { get; private set; }

        private bool prevOnGround;

        public List<Vector2> prevPositions;

        public int facingDirection;

        private bool directionLock;
        private int _directionLockTimer;
        private int directionLockTimer { get { return _directionLockTimer; } set { _directionLockTimer = value; directionLock = _directionLockTimer > 0; } }

        private bool enabledGroundSpeedGravity = true, enabledReattachSpeed = true;

        private float _inputLockoutTimer;
        private bool inputLockout;
        public float inputLockoutTimer { get { return _inputLockoutTimer; } set { _inputLockoutTimer = value * GameWorld.timescale; inputLockout = _inputLockoutTimer > 0; } }

        private int _parryTimer;
        public int parryTimer { get { return _parryTimer; } set { _parryTimer = value; parrying = _parryTimer > 0; } }
        public bool parrying { get; private set; }
        public bool canParry;

        public bool canShoot;
        private int _shootTimer;
        private int shootTimer { get { return _shootTimer; } set { _shootTimer = value; canShoot = _shootTimer <= 0; } }

        private int tryFacingDirection;

        public bool jump { get; private set; }
        public ScriptPlayerMovement() : base()
        {

        }

        public ScriptPlayerMovement(GameObject owner) : base(owner)
        {
            prevPositions = new List<Vector2>();
        }

        public override void Update(GameWorld world)
        {
            if (!GameMain.editing)
            {
                parryTimer--;

                bool xMoving = false;
                MoveUpdate(ref xMoving);

                float anglenomod = 0;
                if (owner.collider.onGround)
                {
                    OnGround(ref anglenomod, xMoving);
                }
                else
                {
                    InAir();
                }

                prevOnGround = owner.collider.onGround;

                gspeed = MathHelper.Clamp(gspeed, -gtop, gtop);

                #region camera movement
                if (Camera.camera.moveMode != CameraMoveMode.Snap)
                {
                    float distFromCameraX = Camera.camera.target.X - owner.transform.position.X;
                    float distFromCameraY = Camera.camera.target.Y - owner.transform.position.Y;
                    if (Math.Abs(distFromCameraX) > horizontalScrollDist)
                    {
                        float c = Math.Abs(distFromCameraX);
                        float xmove = MathHelper.Clamp(c * Math.Sign(distFromCameraX) * -1, -16, 16);
                        Camera.camera.target = new Vector2(Camera.camera.target.X + xmove, Camera.camera.target.Y);
                    }

                    float ycheck = owner.collider.onGround ? verticalScrollDist : verticalScrollDistAir;
                    if (Math.Abs(distFromCameraY) > ycheck)
                    {
                        if (owner.collider.onGround)
                        {
                            float ymax = owner.collider.velocity.Y <= 6 ? 6 : 16;

                            float c = Math.Abs(distFromCameraY);
                            float ymove = MathHelper.Clamp(c * Math.Sign(distFromCameraY) * -1, -ymax, ymax);
                            Camera.camera.target = new Vector2(Camera.camera.target.X, Camera.camera.target.Y + ymove);
                        }
                        else
                        {
                            float c = Math.Abs(distFromCameraY);
                            float ymove = MathHelper.Clamp(c * Math.Sign(distFromCameraY) * -1, -16, 16);
                            Camera.camera.target = new Vector2(Camera.camera.target.X, Camera.camera.target.Y + ymove);
                        }
                    }
                }
                else
                {
                    Vector2 dist = new Vector2(Camera.camera.data[0], Camera.camera.data[1]);
                    Camera.camera.target = owner.transform.GetPositionInWorldSpace(true) + dist;
                }
                #endregion

                directionLockTimer--;
                if (owner.collider.velocity.X != 0 && !directionLock)
                {
                    if (!float.IsNaN(owner.collider.velocity.X))
                        facingDirection = Math.Sign(owner.collider.velocity.X);
                }

                shootTimer--;
                if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.F) && canShoot)
                {
                    OnShoot(world);
                }
            }
        }

        public void MoveUpdate(ref bool xMoving)
        {
            if (!owner.GetComponent<ScriptPlayerDamage>().dying)
            {
                if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.G))
                    owner.collider.noclip = !owner.collider.noclip;

                tryFacingDirection = facingDirection;
                if (!inputLockout)
                {
                    if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.A))
                    {
                        if (owner.collider.onGround)
                        {
                            if (gspeed > 0)
                                gspeed -= deceleration;
                            else gspeed -= acceleration;

                            xMoving = true;
                        }
                        else owner.collider.velocity.X -= airAccel;

                        if (owner.collider.noclip)
                            owner.transform.position.X -= 5;

                        tryFacingDirection = -1;
                    }
                    if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.D))
                    {
                        if (owner.collider.onGround)
                        {
                            if (gspeed < 0)
                                gspeed += deceleration;
                            else gspeed += acceleration;

                            xMoving = true;
                        }
                        else owner.collider.velocity.X += airAccel;

                        if (owner.collider.noclip)
                            owner.transform.position.X += 5;

                        tryFacingDirection = 1;
                    }
                }
                else inputLockoutTimer--;

                if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.W))
                {
                    if (owner.collider.noclip)
                        owner.transform.position.Y -= 5;
                }

                if (GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.S))
                {
                    if (owner.collider.noclip)
                        owner.transform.position.Y += 5;
                }

                if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.K))
                    owner.parentWorld.AddGameObject(GameMain.CloneFromPreset("internal_ent_crab", owner.transform.GetPositionInWorldSpace(true), false));
            }
        }

        public void OnGround(ref float anglenomod, bool xMoving)
        {
            float angle = 0;
            float angleRad = 0;
            Collider c = owner.collider;
            if (c.ground is ColliderTileSlope)
            {
                angle = EngineMathHelper.Mod(((ColliderTileSlope)c.ground).angle, 360);
                angleRad = MathHelper.ToRadians(angle);

                anglenomod = ((ColliderTileSlope)c.ground).angle;
            }

            if (c.ground.quadrant == AngleQuadrant.Right)
                owner.renderer.offset = new Vector2(-1, 0).RotateBy(angle) * 10;
            if (c.ground.quadrant == AngleQuadrant.Top)
                owner.renderer.offset = new Vector2(0, -1).RotateBy(angle) * 10;
            if (c.ground.quadrant == AngleQuadrant.Left)
                owner.renderer.offset = new Vector2(1, 0).RotateBy(angle) * 10;
            if (c.ground.quadrant == AngleQuadrant.Bottom)
                owner.renderer.offset = new Vector2(0, 1).RotateBy(angle) * 10;

            if (enabledGroundSpeedGravity)
                gspeed += slope * (float)Math.Sin(angleRad);

            //when reattaching to the ground, we can't just keep our old gspeed.
            if (enabledReattachSpeed && !prevOnGround)
            {
                OnReattachGround(angle, angleRad);
            }

            if (!xMoving)   //if no move button was pressed, the player should slow down.
                gspeed -= MathHelper.Min(Math.Abs(gspeed), friction) * Math.Sign(gspeed);

            Vector2 vecAng = new Vector2((float)Math.Cos(angleRad), (float)Math.Sin(angleRad));
            owner.collider.velocity.X = gspeed * vecAng.X;
            owner.collider.velocity.Y = gspeed * vecAng.Y;

            owner.transform.rotation = angle;

            if (Math.Abs(gspeed) < 2.5f && owner.collider.quadrant != AngleQuadrant.Top && (angle >= 90 && angle <= 270))
            {   //fall off
                inputLockoutTimer = 25;
                owner.collider.NotOnGround();
            }

            //Jump Calculations - starting on ground
            if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Space))
            {   ///<see cref="http://info.sonicretro.org/SPG:Jumping"/>; "Jump Velocity" section.
                OnJump(angleRad);
            }

            if (owner.collider.velocity.X != 0)
            {
                if (Math.Abs(gspeed) < 4)
                {
                    owner.renderer.texture.StartAnimation(0, true);
                    owner.renderer.texture.ForceFramerate((int)(8 - Math.Abs(gspeed)));
                }
                else if (Math.Abs(gspeed) >= 4 && Math.Abs(gspeed) < 8)
                {
                    owner.renderer.texture.StartAnimation(4, true);
                    owner.renderer.texture.ForceFramerate((int)(8 - Math.Abs(gspeed)));
                }
                else if (Math.Abs(gspeed) >= 8 && Math.Abs(gspeed) <= 10) 
                {
                    owner.renderer.texture.StartAnimation(5, true);
                    owner.renderer.texture.ForceFramerate((int)(8 - Math.Abs(gspeed)));
                }

                owner.renderer.texture.flip = facingDirection == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None ;
            }
            else owner.renderer.texture.StartAnimation(1, true);
        }

        private void InAir()
        {
            if (jump)
            {
                owner.renderer.offset = Vector2.Zero;
                owner.renderer.texture.StartAnimation(2, true);
                owner.renderer.texture.ForceFramerate((int)(8 - Math.Abs(gspeed)));

                if (GameMain.keyboard.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Space))
                {
                    if (canParry)
                    {
                        ScriptPlayerDamage ds = owner.GetComponent<ScriptPlayerDamage>();
                        parryTimer = 16;
                        ds.invulnTime = parryTimer;

                        canParry = false;
                    }
                }
            }
            else
            {
                owner.renderer.texture.StartAnimation(3, true);
                owner.renderer.texture.ForceFramerate((int)(8 - Math.Abs(gspeed)));
            }

            if (initialJumpHeld && owner.collider.velocity.Y > -4)  //if we start to slow down in the air, the "cutoff" jump release no longer works.
                initialJumpHeld = false;

            if (initialJumpHeld && !GameMain.keyboard.KeyHeld(Microsoft.Xna.Framework.Input.Keys.Space))
            {   //if we are jumping and release the spacebar, while still going upwards < -4, set yspeed to -4 so we can "cutoff" the jump.
                owner.collider.velocity.Y = -4;
            }

            //air drag stuff
            if (owner.collider.velocity.Y < 0 && owner.collider.velocity.Y > -4)
            {
                if (Math.Abs(owner.collider.velocity.X) >= 0.125f) owner.collider.velocity.X *= airDrag;
            }

            owner.transform.rotation = MathHelper.Lerp(owner.transform.rotation, 0, .25f);

            owner.collider.velocity = Vector2.Clamp(owner.collider.velocity, new Vector2(-atop), new Vector2(atop));

            if (owner.collider.velocity.Y > 0)  //moving down
            {
                yfalltime++;
            }
            else yfalltime = 0;
        }

        public void OnJump(float angleRad)
        {
            jump = true;
            ScriptDamagable sd = owner.GetComponent<ScriptDamagable>();
            sd.canDealDamage = true;
            sd.canTakeDamage = false;

            initialJumpHeld = true;

            Vector2 angM = new Vector2(-(float)Math.Sin(angleRad), (float)Math.Cos(angleRad));

            owner.collider.velocity -= 6.5f * angM;

            owner.collider.NotOnGround();

            owner.collider.quadrant = AngleQuadrant.Top;
        }

        public void OnReattachGround(float angle, float angleRad)
        {
            jump = false;
            ScriptDamagable sd = owner.GetComponent<ScriptDamagable>();
            sd.canDealDamage = false;
            sd.canTakeDamage = true;

            parryTimer = 0; //can't parry until we hit the ground again
            canParry = true;

            bool set = false;
            if ((angle >= 0 && angle < 22.5f) || (angle < 337.5f && angle >= 360))  //small angles, or flat ground.
            {
                gspeed = owner.collider.velocity.X;
                set = true;
            }

            if ((angle >= 22.5f && angle < 45) || (angle <= 337.5f && angle > 315)) //larger angles
            {
                if (Math.Abs(owner.collider.velocity.X) > owner.collider.velocity.Y)
                {
                    gspeed = owner.collider.velocity.X;
                }
                else gspeed = owner.collider.velocity.Y * .5f * Math.Sign(Math.Sin(angleRad));

                set = true;
            }

            if ((angle >= 45 && angle < 90) || (angle <= 315 && angle > 270))       //steep angles
            {
                if (Math.Abs(owner.collider.velocity.X) > owner.collider.velocity.Y)
                {
                    gspeed = owner.collider.velocity.X;
                }
                else gspeed = owner.collider.velocity.Y * Math.Sign(Math.Sin(angleRad));

                set = true;
            }

            if (!set || inputLockout)
                gspeed = 0; //falling straight down
        }

        public void OnShoot(GameWorld world)
        {
            GameObject go = GameMain.CloneFromPreset("internal_ent_bullet_player", owner.transform.position + new Vector2(0, -16), false, false);

            go.GetComponent<ScriptMoveDumb>().left = tryFacingDirection == 1 ? false : true;
            world.AddGameObject(go, false);

            if (owner.collider.onGround)
            {
                gspeed = -tryFacingDirection * 4;
            }
            else
            {
                if (owner.collider.velocity.Y < -2)
                    owner.collider.velocity.Y = 1;
                owner.collider.velocity.X = -tryFacingDirection * 6;
            }

            directionLockTimer = 20;

            shootTimer = 120;
        }

        public override object Clone()
        {
            return new ScriptPlayerMovement(owner);
        }

        public override int GetByteSize()
        {
            return 0;
        }

        public override void Serialize(BinaryWriter writer)
        {

        }

        public override void Deserialize(BinaryReader reader)
        {

        }

        public override void AddEditorValues()
        {
            base.AddEditorValues();
        }
    }
}
