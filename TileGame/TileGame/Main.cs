﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using TileGameEngine;
using System;
using TileGameEngine.Components;
using TileGameEngine.Components.Renderers;
using TileGameEngine.Components.Scripts;
using TileGame.Components.Scripts;
using TileGameEngine.Components.Colliders;
using System.Collections.Generic;
using BlueRavenUtility;
using TileGameEngine.Components.CollidersR;

namespace TileGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : GameMain
    {
        public Main() : base()
        {
            errorTextureKey = "error";
        }

        protected override void CreateWorld()
        {
            base.world = new World();
        }

        public override object CreateComponent(string className)
        {
            Type d = Type.GetType(className);
            return Activator.CreateInstance(d);
        }

        public override void RegisterPresets()
        {
            base.RegisterPresets();

            presets.Add("env_text", new Preset("env_text", delegate ()
            {
                GameObject envObj = new GameObject(world);
                envObj.transform = new Transform(envObj, new Vector2(128, 0));
                envObj.renderer = new RendererText(envObj, "bitfontMunro23BOLD", "default", Color.White, Enums.Alignment.Center, 128, RelativeTo.World);
                return envObj;
            }));
            //spawners
            presets.Add("ent_player", new Preset("ent_player", delegate ()
            {
                GameObject spawnerObj = new GameObject(world);
                spawnerObj.transform = new Transform(spawnerObj, new Vector2(128, 0));
                //no renderer, is a spawner
                spawnerObj.AddScript(new ScriptEntitySpawner(spawnerObj, "internal_ent_player"));
                ((World)world).playerObj = spawnerObj;
                return spawnerObj;
            }));

            presets.Add("ent_crab", new Preset("ent_crab", delegate ()
            {
                GameObject spawnerObj = new GameObject(world);
                spawnerObj.transform = new Transform(spawnerObj, new Vector2(128, 0));
                //no renderer, is a spawner
                spawnerObj.AddScript(new ScriptEntitySpawner(spawnerObj, "internal_ent_crab"));

                return spawnerObj;
            }));

            presets.Add("ent_energy", new Preset("ent_energy", delegate ()
            {
                GameObject spawnerObj = new GameObject(world);
                spawnerObj.transform = new Transform(spawnerObj, new Vector2(128, 0));
                //no renderer, is a spawner
                spawnerObj.AddScript(new ScriptEntitySpawner(spawnerObj, "internal_ent_energy"));

                return spawnerObj;
            }));

            presets.Add("ent_particle_parrypoof", new Preset("ent_particle_parrypoof", delegate ()
            {
                GameObject spawnerObj = new GameObject(world);
                spawnerObj.transform = new Transform(spawnerObj, new Vector2(128, 0));
                //no renderer, is a spawner
                spawnerObj.AddScript(new ScriptEntitySpawner(spawnerObj, "internal_ent_particle_parrypoof"));

                return spawnerObj;
            }));

            presets.Add("ent_playerwatertransport_3", new Preset("ent_playerwatertransport_3", delegate ()
            {
                GameObject entObj = new GameObject(world);
                entObj.transform = new Transform(entObj);
                entObj.renderer = new RendererNOOP(entObj, 0);
                entObj.collider = new ColliderNOOP(entObj);
                entObj.scripts.Add(new ScriptPlayerWaterTransport(entObj, .0125f, Vector2.Zero, new Vector2(0, -128), new Vector2(128, -128)));
                return entObj;
            }));

            presets.Add("env_intro", new Preset("env_intro", delegate ()
            {
                GameObject envObj = new GameObject(world);
                envObj.transform = new Transform(envObj, new Vector2(128, 0));
                envObj.renderer = new RendererNOOP(envObj, 1024);
                envObj.AddScript(new ScriptLevelOpen(envObj, "Test"));
                envObj.forceActive = true;
                return envObj;
            }));

            presets.Add("internal_ent_bullet_player", new Preset("internal_ent_bullet_player", delegate ()
            {
                GameObject entityObj = new GameObject(world);
                entityObj.transform = new Transform(entityObj, new Vector2(128, 0), scale: new Vector2(2));
                entityObj.renderer = new RendererPanel(entityObj, new TextureInfo("bulletp", new Rectangle(0, 0, 43, 31)), Enums.Alignment.Bottom);
                List<Animation> a = new List<Animation>();
                a.Add(new Animation(0, AnimationType.Loop, 0, Frame.CreateFrames(0, 0, 0, 0, 2, 16, 8, 6, 4)));
                entityObj.renderer.texture.AddAnimation(a);
                entityObj.collider = new ColliderRectangle(entityObj, 8, 2);
                entityObj.collider.SetUpdateConstants(0, 0);
                entityObj.AddScript(new ScriptAnimation(entityObj));
                entityObj.AddScript(new ScriptDamagable(entityObj, 8, 1, 1, "none"));
                entityObj.AddScript(new ScriptMoveDumb(entityObj, 12f, 12f, true));
                entityObj.AddScript(new ScriptDelayDeath(entityObj, 60));
                entityObj.AddScript(new ScriptDieOnStartTouch(entityObj));
                entityObj.serializable = false;
                entityObj.selectable = false;
                entityObj.destroyOnReset = true;
                entityObj.tags.Add("player");

                entityObj.forceActive = true;

                return entityObj;
            }));

            //internal
            presets.Add("internal_ent_player", new Preset("internal_ent_player", delegate ()
            {
                GameObject playerObj = new GameObject(world);
                playerObj.renderer = new RendererPanel(playerObj, new TextureInfo("sonicSpriteSheet", new Rectangle(0, 0, 8, 8)), Enums.Alignment.Center);    //800 -new Vector2(0, 20)

                List<Animation> a = new List<Animation>();
                a.Add(new Animation(0, AnimationType.Loop, 0,
                    new Frame(0, 0, new Rectangle(341, 76, 25, 40), 2),
                    new Frame(0, 1, new Rectangle(367, 76, 24, 40), 2),
                    new Frame(0, 2, new Rectangle(392, 76, 35, 40), 2),
                    new Frame(0, 3, new Rectangle(428, 76, 36, 40), 2),
                    new Frame(0, 4, new Rectangle(465, 76, 33, 40), 2),
                    new Frame(0, 5, new Rectangle(499, 76, 29, 40), 2),
                    new Frame(0, 6, new Rectangle(529, 76, 25, 40), 2),
                    new Frame(0, 7, new Rectangle(555, 76, 26, 40), 2),
                    new Frame(0, 8, new Rectangle(582, 76, 35, 40), 2),
                    new Frame(0, 9, new Rectangle(618, 76, 37, 40), 2),
                    new Frame(0, 10, new Rectangle(656, 76, 34, 40), 2),
                    new Frame(0, 11, new Rectangle(691, 76, 31, 40), 2)
                    )); //393, 182
                a.Add(new Animation(1, AnimationType.Loop, 0, new Frame(1, 0, new Rectangle(1, 23, 28, 40), 1)));   //idle
                a.Add(new Animation(2, AnimationType.Loop, 0, Frame.CreateFrames(2, 0, 393, 182, 1, 32, 32, 16, 2)));  //jump
                a.Add(new Animation(3, AnimationType.Loop, 0, Frame.CreateFrames(3, 0, 723, 76, 1, new int[] { 24, 35, 36, 33, 30, 25, 26, 36, 37, 34, 33, 25 }, 41, 12, 2))); //air walk
                a.Add(new Animation(4, AnimationType.Loop, 0, Frame.CreateFrames(4, 0, 1, 128, 1, new int[] { 34, 31, 28, 32, 33, 32, 28, 28, 31, 32 }, 37, 10, 2))); //jog
                a.Add(new Animation(5, AnimationType.Loop, 0, Frame.CreateFrames(5, 0, 320, 127, 1, new int[] { 31, 31, 31, 29, 30, 31, 31, 30 }, 38, 8, 2))); //run

                playerObj.renderer.texture.AddAnimation(a);
                //playerObj.renderer.texture.TryFindAnimFile();
                playerObj.transform = new Transform(playerObj, position: new Vector2(128, 0), scale: new Vector2(1.5625f));
                playerObj.collider = new ColliderRectangle(playerObj, 16, 24);
                playerObj.collider.SetUpdateConstants(ScriptPlayerMovement.gravity, 1);
                playerObj.AddScript(new ScriptPlayerMovement(playerObj));
                playerObj.AddScript(new ScriptAnimation(playerObj));
                playerObj.AddScript(new ScriptPlayerDamage(playerObj));
                playerObj.forceActive = true;
                playerObj.destroyOnReset = true;
                playerObj.tags.Add("player");

                playerObj.colliderDynamic = new ColliderDynamic(playerObj);

                return playerObj;
            }));

            presets.Add("internal_ent_crab", new Preset("internal_ent_crab", delegate ()
            {
                GameObject entityObj = new GameObject(world);
                entityObj.transform = new Transform(entityObj, position: new Vector2(128, 0), scale: new Vector2(1.25f));
                entityObj.collider = new ColliderRectangle(entityObj, 2, 8);
                entityObj.renderer = new RendererPanel(entityObj, new TextureInfo("crab", new Rectangle(0, 0, 43, 31)), Enums.Alignment.Bottom);
                List<Animation> a = new List<Animation>();
                a.Add(
                    new Animation(0, AnimationType.Loop, 0, 
                    new Frame(0, 0, new Rectangle(0, 0, 42, 31), 4),
                    new Frame(0, 1, new Rectangle(43, 0, 44, 31), 4),
                    new Frame(0, 2, new Rectangle(88, 0, 44, 31), 4),
                    new Frame(0, 3, new Rectangle(133, 0, 45, 31), 4),
                    new Frame(0, 4, new Rectangle(179, 0, 45, 31), 4),
                    new Frame(0, 5, new Rectangle(225, 0, 44, 31), 4),
                    new Frame(0, 6, new Rectangle(270, 0, 42, 31), 4)
                    ));
                entityObj.renderer.texture.AddAnimation(a);
                entityObj.AddScript(new ScriptAnimation(entityObj));
                entityObj.AddScript(new ScriptDamagable(entityObj, 32, 1, 1, "player"));
                entityObj.AddScript(new ScriptMoveDumb(entityObj, .5f, .5f, true));
                entityObj.collider.SetUpdateConstants(ScriptPlayerMovement.gravity, 1);
                entityObj.destroyOnReset = true;
                entityObj.tags.Add("enemy");

                return entityObj;
            }));

            presets.Add("internal_ent_energy", new Preset("internal_ent_energy", delegate ()
            {
                GameObject energyObj = new GameObject(world);
                energyObj.transform = new Transform(energyObj, position: new Vector2(128, 0), scale: new Vector2(2));
                energyObj.renderer = new RendererPanel(energyObj, new TextureInfo("energy", new Rectangle(0, 0, 16, 16)));
                List<Animation> a = new List<Animation>();
                a.Add(
                    new Animation(0, AnimationType.Loop, 0,
                    new Frame(0, 0, new Rectangle(0, 0, 16, 16), 16),
                    new Frame(0, 1, new Rectangle(16, 0, 16, 16), 4),
                    new Frame(0, 2, new Rectangle(32, 0, 16, 16), 4),
                    new Frame(0, 3, new Rectangle(48, 0, 16, 16), 4),
                    new Frame(0, 4, new Rectangle(64, 0, 16, 16), 4),
                    new Frame(0, 5, new Rectangle(80, 0, 16, 16), 4),
                    new Frame(0, 6, new Rectangle(96, 0, 16, 16), 4),
                    new Frame(0, 7, new Rectangle(112, 0, 16, 16), 4),
                    new Frame(0, 8, new Rectangle(128, 0, 16, 16), 4),
                    new Frame(0, 9, new Rectangle(144, 0, 16, 16), 4),
                    new Frame(0, 10, new Rectangle(160, 0, 16, 16), 4),
                    new Frame(0, 11, new Rectangle(176, 0, 16, 16), 4),
                    new Frame(0, 12, new Rectangle(192, 0, 16, 16), 4),
                    new Frame(0, 13, new Rectangle(208, 0, 16, 16), 4),
                    new Frame(0, 14, new Rectangle(224, 0, 16, 16), 4),
                    new Frame(0, 15, new Rectangle(240, 0, 16, 16), 4),
                    new Frame(0, 16, new Rectangle(256, 0, 16, 16), 4),
                    new Frame(0, 17, new Rectangle(272, 0, 16, 16), 4)
                    ));
                energyObj.renderer.texture.AddAnimation(a);
                energyObj.collider = new ColliderRectangle(energyObj, 4, 2);
                energyObj.scripts.Add(new ScriptAnimation(energyObj));
                energyObj.scripts.Add(new ScriptCollectableEnergy(energyObj));
                energyObj.destroyOnReset = true;
                return energyObj;
            }));

            presets.Add("internal_ent_particle_parrypoof", new Preset("internal_ent_particle_parrypoof", delegate ()
            {
                GameObject poofObj = new GameObject(world);
                poofObj.transform = new Transform(poofObj, position: new Vector2(128, 0), scale: new Vector2(2));
                poofObj.renderer = new RendererPanel(poofObj, new TextureInfo("parryPoof", new Rectangle(0, 0, 36, 36)));
                List<Animation> a = new List<Animation>();
                a.Add(new Animation(0, AnimationType.Pause, 0,
                    new Frame(0, 0, new Rectangle(0, 0, 36, 36), 2),
                    new Frame(0, 1, new Rectangle(0, 38, 36, 36), 2),
                    new Frame(0, 2, new Rectangle(0, 76, 36, 36), 2),
                    new Frame(0, 3, new Rectangle(0, 114, 36, 36), 2),
                    new Frame(0, 4, new Rectangle(0, 152, 36, 36), 2),
                    new Frame(0, 5, new Rectangle(0, 190, 36, 36), 2),
                    new Frame(0, 6, new Rectangle(0, 228, 36, 36), 2),
                    new Frame(0, 7, new Rectangle(0, 266, 36, 36), 2),
                    new Frame(0, 8, new Rectangle(0, 304, 36, 36), 2),
                    new Frame(0, 9, new Rectangle(0, 342, 36, 36), 2),
                    new Frame(0, 10, new Rectangle(0, 380, 36, 36), 2),
                    new Frame(0, 11, new Rectangle(0, 418, 36, 36), 2)
                    ));
                poofObj.renderer.texture.AddAnimation(a);
                poofObj.destroyOnReset = true;
                poofObj.scripts.Add(new ScriptAnimation(poofObj));
                poofObj.scripts.Add(new ScriptDelayDeath(poofObj, 24));

                return poofObj;
            }));
        }
    }
}
