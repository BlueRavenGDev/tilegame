﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

using TileGameEngine;
using TileGameEngine.Components.Colliders;
using TileGameEngine.Components.Renderers;
using TileGameEngine.Components;
using Microsoft.Xna.Framework.Input;
using System.IO;
using TileGameEngine.Components.Scripts;
using TileGame.Components.Scripts;
using BlueRavenUtility;
using TileGameEngine.Components.CollidersR;

namespace TileGame
{
    public class World : GameWorld
    {
        public GameObject playerObj;

        public World() : base()
        {
            addInstanceFunctions.Add("TileGame", str => { return Activator.CreateInstance(Type.GetType(str)); });
        }

        public override void LoadWorld()
        {
            TileSize = 32;
            base.LoadWorld();

            ColliderTile.pullHeight = 8;
            ColliderTile.pushDepth = 16;

            GameObject obj1 = new GameObject(this);
            obj1.forceActive = true;
            obj1.renderer = new RendererBackground(obj1, new TextureInfo("canyonBG2"), new Vector2(.25f, 0), new Vector2(0, 192));
            obj1.transform = new Transform(obj1);
            AddGameObject(obj1, false);

            obj1 = new GameObject(this);
            obj1.forceActive = true;
            obj1.renderer = new RendererBackground(obj1, new TextureInfo("canyonBG"), new Vector2(.5f, 0), new Vector2(0, 192));
            obj1.transform = new Transform(obj1);
            AddGameObject(obj1, false);

            for (int i = 0; i < 12; i++)
            {
                obj1 = new GameObject(this);
                obj1.colliderStatic = new ColliderStaticTile(obj1);
                obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(0, 0, 8, 8)));
                obj1.transform = new Transform(obj1, position: new Vector2(0 + i, 12), scale: new Vector2(4));
                obj1.tile = true;
                AddGameObject(obj1, true);
            }

            obj1 = new GameObject(this);
            obj1.colliderStatic = new ColliderStaticTile(obj1);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(0, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(6, 11), scale: new Vector2(4));
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.colliderStatic = new ColliderStaticTile(obj1);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(0, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(5, 9), scale: new Vector2(4));
            obj1.tile = true;
            AddGameObject(obj1, true);
            /*for (int i = 0; i < 8; i++)
            {
                GameObject obj = new GameObject(this);
                obj.renderer = new RendererTile(obj, new TextureInfo("tilesFull", new Rectangle(0, 0, 8, 8)));
                obj.transform = new Transform(obj, position: new Vector2(4 + i, 11), scale: new Vector2(4));
                obj.collider = new ColliderTileSlope(obj, 0, 16);
                obj.tile = true;

                AddGameObject(obj, true);
            }

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(0, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(4 + 4, 8), scale: new Vector2(4));
            obj1.collider = new ColliderTile(obj1, ColliderTile.TileCollideType.Left);
            obj1.tile = true;

            AddGameObject(obj1, true);
            

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(8, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(3, 10), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 26.57f, 8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(8, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(4, 10), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 26.57f, -8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(8, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(2, 9), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 45, 0);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(24, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(1, 9), scale: new Vector2(4));
            obj1.collider = new ColliderTileCheckNear(obj1, 0, -1);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(1, 8), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 63.43f, -8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(1, 7), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 63.43f, 8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            float a1 = VectorHelper.GetVectorAngle(new Vector2(TileSize, TileSize * 2));

            for (int i = 0; i < 2; i++)
            {
                obj1 = new GameObject(this);
                obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
                obj1.transform = new Transform(obj1, position: new Vector2(0, 6 - i), scale: new Vector2(4));
                obj1.collider = new ColliderTileSlope(obj1, 90, -16);
                obj1.tile = true;
                AddGameObject(obj1, true);
            }

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(24, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(1, 2), scale: new Vector2(4));
            obj1.collider = new ColliderTileCheckNear(obj1, 0, 1);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(1, 4), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 116.57f, 8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(1, 3), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 116.57f, -8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(2, 2), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 135, 0);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(3, 1), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 153.43f, 8);
            obj1.tile = true;
            AddGameObject(obj1, true);

            obj1 = new GameObject(this);
            obj1.renderer = new RendererTile(obj1, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
            obj1.transform = new Transform(obj1, position: new Vector2(4, 1), scale: new Vector2(4));
            obj1.collider = new ColliderTileSlope(obj1, 153.43f, -8);
            obj1.tile = true;
            AddGameObject(obj1, true);*/

        }

        public override void Update()
        {
            base.Update();

            if (noUpdate)
                return;

            if (GameMain.keyboard.KeyPressed(Keys.R))
                RecreatePlayer();

            if (GameMain.keyboard.KeyPressed(Keys.L))
            {
                Serialize("testmap");
            }

            if (GameMain.keyboard.KeyPressed(Keys.K))
            {
                Deserialize("testmap");
            }
        }

        public void RecreatePlayer()
        {
            if (playerObj == null || !playerObj.isAdded)
            {
                GameObject playerObj = new GameObject(this);
                playerObj.transform = new Transform(playerObj, new Vector2(128, 0), new Vector2(4));
                playerObj.renderer = new RendererPanel(playerObj, new TextureInfo("tilesFull", new Rectangle(16, 0, 8, 8)));
                playerObj.colliderDynamic = new ColliderDynamic(playerObj);
                //playerObj = GameMain.CloneFromPreset("ent_player", Vector2.Zero, false);
                AddGameObject(playerObj);
            }

            RespawnAll();
        }
    }
}
