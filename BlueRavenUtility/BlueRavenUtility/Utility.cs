﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueRavenUtility
{
    public static class Utility
    {
        internal static bool setup;

        public static void Setup(Camera camera, Texture2D whitePixel)
        {
            Camera.camera = camera;
            DrawHelper.whitePixel = whitePixel;

            setup = true;
        }

        internal static void CheckIsSetup()
        {
            if (!setup)
            {
                throw new Exception("BlueRavenUtility has not been correctly setup! Use Utility.Setup!");
            }
        }
    }
}
