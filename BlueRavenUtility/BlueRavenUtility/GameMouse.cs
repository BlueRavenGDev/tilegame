﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BlueRavenUtility
{
    public class GameMouse
    {
        private MouseState currentState, lastState;
        public Vector2 currentPosition, lastPosition;
        private float currentScrollWheelValue;
        public float deltaScrollWheelValue;

        public bool hasFocus;

        public GameMouse()
        {

        }

        public void PreUpdate()
        {
            currentState = Mouse.GetState();
            currentPosition = currentState.Position.ToVector2();

            deltaScrollWheelValue = currentState.ScrollWheelValue - currentScrollWheelValue;
            currentScrollWheelValue += deltaScrollWheelValue;
        }

        public void Update()
        {

        }

        public void PostUpdate()
        {
            lastState = currentState;
            lastPosition = currentPosition;
        }

        public Vector2 GetWorldPosition()
        {
            return VectorHelper.ScreenToWorldCoords(currentPosition);
        }

        public float ScrollWheelDirection()
        {
            return deltaScrollWheelValue;
        }

        public bool LeftButtonPressed()
        {
            return currentState.LeftButton == ButtonState.Pressed && lastState.LeftButton == ButtonState.Released && hasFocus;
        }

        public bool LeftButtonHeld()
        {
            return currentState.LeftButton == ButtonState.Pressed && hasFocus;
        }
    }
}
