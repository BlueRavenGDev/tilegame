﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueRavenUtility
{
    public class Logger
    {
        public enum LogLevel
        {
            Warn,
            Caution,
            Error,
            Fatal
        }

        public static Logger logger;

        public Logger()
        {
            logger = this;
        }

        public void Log(LogLevel level, string message)
        {
            string fmessage = level.ToString() + ": " + message;
            Console.WriteLine(fmessage);

            using (FileStream fs = File.OpenWrite("log_" + DateTime.Now))
            {
                fs.Write(Encoding.ASCII.GetBytes(fmessage), 0, fmessage.Length * sizeof(char));
            }
        }
    }
}
