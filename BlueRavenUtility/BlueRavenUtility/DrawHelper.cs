﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BlueRavenUtility
{
    public static class DrawHelper
    {
        internal static Texture2D _whitePixel;
        public static Texture2D whitePixel { get { Utility.CheckIsSetup(); return _whitePixel; } set { _whitePixel = value; } }

        public static void DrawRectangle(this SpriteBatch batch, Rectangle area, Color color, float depth = 0)
        {
            Texture2D whitePixel = DrawHelper.whitePixel;

            batch.Draw(whitePixel, area, null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
        }

        public static void DrawRectangle(this SpriteBatch batch, RectangleF area, Color color, float depth = 0)
        {
            Texture2D whitePixel = DrawHelper.whitePixel;

            batch.Draw(whitePixel, area.ToRectangle(), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
        }

        public static void DrawHollowRectangle(this SpriteBatch batch, Rectangle area, int width, Color color, float depth = 0)
        {
            Texture2D whitePixel = DrawHelper.whitePixel;

            batch.Draw(whitePixel, new Rectangle(area.X, area.Y, area.Width, width), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y, width, area.Height), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
            batch.Draw(whitePixel, new Rectangle(area.X + area.Width - width, area.Y, width, area.Height), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y + area.Height - width, area.Width, width), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
        }

        public static void DrawHollowRectangle(this SpriteBatch batch, RectangleF area, int width, Color color, float depth = 0)
        {
            Texture2D whitePixel = DrawHelper.whitePixel;

            batch.Draw(whitePixel, new RectangleF(area.x, area.y, area.width, width).ToRectangle(), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
            batch.Draw(whitePixel, new RectangleF(area.x, area.y, width, area.height).ToRectangle(), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
            batch.Draw(whitePixel, new RectangleF(area.x + area.width - width, area.y, width, area.height).ToRectangle(), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
            batch.Draw(whitePixel, new RectangleF(area.x, area.y + area.height - width, area.width, width).ToRectangle(), null, color, 0, Vector2.Zero, SpriteEffects.None, depth);
        }

        public static void DrawHollowCircle(this SpriteBatch batch, Vector2 center, float radius, Color color, int lineWidth = 2, int segments = 16)
        {
            Vector2[] vertex = new Vector2[segments];

            double increment = Math.PI * 2.0 / segments;
            double theta = 0.0;

            for (int i = 0; i < segments; i++)
            {
                vertex[i] = center + radius * new Vector2((float)Math.Cos(theta), (float)Math.Sin(theta));
                theta += increment;
            }

            DrawHollowPolygon(batch, vertex, segments, color, lineWidth);
        }
        public static void DrawHollowPolygon(this SpriteBatch batch, Vector2[] vertex, int count, Color color, int lineWidth)
        {
            Texture2D whitePixel = DrawHelper.whitePixel;

            if (count > 0)
            {
                for (int i = 0; i < count - 1; i++)
                {
                    DrawLine(batch, vertex[i], vertex[i + 1], color, lineWidth);
                }
                DrawLine(batch, vertex[count - 1], vertex[0], color, lineWidth);
            }
        }

        public static void DrawLine(this SpriteBatch batch, Vector2 begin, Vector2 end, Color color, int width = 1)
        {
            Texture2D whitePixel = DrawHelper.whitePixel;

            Rectangle r = new Rectangle((int)begin.X, (int)begin.Y, (int)(end - begin).Length() + width, width);
            Vector2 v = Vector2.Normalize(begin - end);
            float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
            if (begin.Y > end.Y) angle = MathHelper.TwoPi - angle;
            batch.Draw(whitePixel, r, null, color, angle, Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}
