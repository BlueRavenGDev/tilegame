﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

namespace BlueRavenUtility
{
    public struct RectangleF
    {
        public static RectangleF Empty { get { return new RectangleF { x = 0, y = 0, width = 0, height = 0 }; } set { } }

        public float x, y, width, height;

        public RectangleF(float x, float y, float width, float height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public RectangleF(Vector2 xy, Vector2 wh)
        {
            this.x = xy.X;
            this.y = xy.Y;
            this.width = wh.X;
            this.height = wh.Y;
        }

        public RectangleF(float x, float y, Vector2 wh)
        {
            this.x = x;
            this.y = y;
            this.width = wh.X;
            this.height = wh.Y;
        }

        public RectangleF(Vector2 xy, float width, float height)
        {
            this.x = xy.X;
            this.y = xy.Y;
            this.width = width;
            this.height = height;
        }

        public bool Contains(Vector2 point)
        {
            return point.X >= x && point.X <= x + width && point.Y >= y && point.Y <= y + height;
        }

        public RectangleF Expand(float byval)
        {
            x -= byval;
            y -= byval;
            width += byval * 2;
            height += byval * 2;

            return new RectangleF(x, y, width, height);
        }

        public RectangleF Expand(float byvalX, float byvalY)
        {
            x -= byvalX;
            y -= byvalY;
            width += byvalX * 2;
            height += byvalY * 2;

            return new RectangleF(x,y,width,height);
        }

        public static Rectangle ToRectangle(RectangleF rectf)
        {
            return new Rectangle((int)rectf.x, (int)rectf.y, (int)rectf.width, (int)rectf.height);
        }

        public Rectangle ToRectangle()
        {
            return new Rectangle((int)x, (int)y, (int)width, (int)height);
        }

        public override string ToString()
        {
            return "X:" + x + " Y:" + y + " W:" + width + " H:" + height;
        }
    }
}
